class MMenu
{
	constructor (textList, gameModeList, keyList, x, y, size, shift, foreground, background)
	{
		this.m_textList = textList;
		this.m_gameModeList = gameModeList;
		this.m_keyList = keyList;
		this.m_x = x;
		this.m_y = y;
		this.m_size = size;
		this.m_shift = shift;
		this.m_foreground = foreground;
		this.background = background;
	}

	displayText(context)
	{
		context.font = String(this.m_size) + "px Ubuntu";
		context.fillStyle = this.m_foreground;

		for(let i = 0; i<this.m_gameModeList.length; i++)
		{
			switch(this.m_gameModeList[i])
			{
				case PickUp.ONEPLAYER :
					this.m_textList[i] = "(1) Number of players : 1";
					break;
				case PickUp.TWOPLAYERS :
					this.m_textList[i] = "(1) Number of players : 2";
					break;
				case PickUp.BRICKMODE :
					this.m_textList[i] = "(2) Game Mode : Brick";
					break;
				case PickUp.DOUBLEMODE :
					this.m_textList[i] = "(2) Game Mode : Double";
					break;
				case PickUp.DOWNTEAM :
					this.m_textList[i] = "(3) Screen position : Down";
					break;
				case PickUp.UPTEAM :
					this.m_textList[i] = "(3) Screen position : Up";
					break;
				case PickUp.BACK :
					this.m_textList[i] = "(4) Double mode position : Back";
					break;
				case PickUp.FRONT :
					this.m_textList[i] = "(4) Double mode position : Front";
					break;
				case PickUp.VERSUS :
					this.m_textList[i] = "(5) Double mode two players game mode : Versus";
					break;
				case PickUp.TEAM :
					this.m_textList[i] = "(5) Double mode two players game mode : Team";
					break;
				case PickUp.BALLSPEEDSLOW :
					this.m_textList[i] = "(6) Ball Speed : Slow";
					break;
				case PickUp.BALLSPEEDMEZZO :
					this.m_textList[i] = "(6) Ball Speed : Medium";
					break;
				case PickUp.BALLSPEEDFAST :
					this.m_textList[i] = "(6) Ball Speed : Fast";
					break;
				case PickUp.COMPUTEREASY :
					this.m_textList[i] = "(7) Computer Level : Easy";
					break;
				case PickUp.COMPUTERMEDIUM :
					this.m_textList[i] = "(7) Computer Level : Medium";
					break;
				case PickUp.COMPUTERHARD :
					this.m_textList[i] = "(7) Computer Level : Hard";
					break;
				case PickUp.COMPUTERHARDEST :
					this.m_textList[i] = "(7) Computer Level : Hardest";
					break;
			}
		}

		for(let i = 0; i<this.m_textList.length; i++)
		{
			context.fillText(this.m_textList[i], this.m_x, this.m_y+(this.m_shift+this.m_size)*i);
		}
	}

	handleMenuInput(event, stateStack, pFunction)
	{
		if(event.type != "keydown"){
			return;
		}
		for(let i = 0; i<this.m_keyList.length; i++)
		{
			if(event.key == this.m_keyList[i])
			{
				switch(this.m_gameModeList[i])
				{
					case PickUp.ONEPLAYER :
						this.m_gameModeList[i] =  PickUp.TWOPLAYERS;
						break;
					case PickUp.TWOPLAYERS :
						this.m_gameModeList[i] = PickUp.ONEPLAYER;
						break;
					case PickUp.BRICKMODE :
						this.m_gameModeList[i] = PickUp.DOUBLEMODE;
						break;
					case PickUp.DOUBLEMODE :
						this.m_gameModeList[i] = PickUp.BRICKMODE;
						break;
					case PickUp.VERSUS :
						this.m_gameModeList[i] = PickUp.TEAM;
						break;
					case PickUp.TEAM :
						this.m_gameModeList[i] = PickUp.VERSUS;
						break;
					case PickUp.DOWNTEAM :
						this.m_gameModeList[i] = PickUp.UPTEAM;
						break;
					case PickUp.UPTEAM :
						this.m_gameModeList[i] = PickUp.DOWNTEAM;
						break;
					case PickUp.BACK :
						this.m_gameModeList[i] = PickUp.FRONT;
						break;
					case PickUp.FRONT :
						this.m_gameModeList[i] = PickUp.BACK;
						break;
					case PickUp.BALLSPEEDSLOW :
						this.m_gameModeList[i] = PickUp.BALLSPEEDMEZZO;
						break;
					case PickUp.BALLSPEEDMEZZO :
						this.m_gameModeList[i] = PickUp.BALLSPEEDFAST;
						break;
					case PickUp.BALLSPEEDFAST :
						this.m_gameModeList[i] = PickUp.BALLSPEEDSLOW;
						break;
					case PickUp.COMPUTEREASY :
						this.m_gameModeList[i] = PickUp.COMPUTERMEDIUM;
						break;
					case PickUp.COMPUTERMEDIUM :
						this.m_gameModeList[i] = PickUp.COMPUTERHARD;
						break;
					case PickUp.COMPUTERHARD :
						this.m_gameModeList[i] = PickUp.COMPUTERHARDEST;
						break;
					case PickUp.COMPUTERHARDEST :
						this.m_gameModeList[i] = PickUp.COMPUTEREASY;
						break;
				}
			}
		}

		if(event.key == "Escape"){
			stateStack.pop();
		}
		else if(event.key == "Enter"){
			stateStack.push(pFunction);
		}
	}

	get getGameModeList()
	{
		return this.m_gameModeList;
	}
};