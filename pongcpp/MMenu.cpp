/*!
*	\file MMenu.cpp
*	\brief MMenu class .cpp file
*	\author Boris Merminod
*	\version 1.0
*/

#include "MMenu.h"

MMenu::MMenu(std::vector<std::string> textList, std::vector<PickUp>gameModeList, std::vector<SDL_Keycode> keyList, int x, int y, int size, int shift, SDL_Color foreground, SDL_Color background)
{
	if(textList.size() != gameModeList.size() || textList.size() != keyList.size() || gameModeList.size() != keyList.size())
	{
		fprintf(stderr, "Menu::Menu : Error, Initialization's menu list not correct\n");
		exit(EXIT_FAILURE);
	}
	m_textList = textList;
	m_gameModeList = gameModeList;
	m_keyList = keyList;
	m_x = x;
	m_y = y;
	m_size = size;
	m_shift = shift;
	m_foreground = foreground;
	m_background = background;
}

bool MMenu::displayText(SDL_Surface * window)
{
	if(window == NULL)
	{
		fprintf(stderr, "MMenu::displayText : window parameter not correct\n");
		return false;
	}

	if(m_textList.size() != m_gameModeList.size())
	{
		fprintf(stderr, "MMenu::displayText : vectors m_textList and m_gameModeList have not the same size\n");
		return false;
	}

	TTF_Font *font = TTF_OpenFont(FONT_PATH, m_size);
	if(font == NULL)
	{
		fprintf(stderr, "MMenu::displayText : Failed to openned the font\n");
		return false;
	}

	for(int i = 0; i<m_gameModeList.size(); i++)
		switch(m_gameModeList[i])
		{
			case ONEPLAYER :
				m_textList[i] = "(1) Number of players : 1";
				break;
			case TWOPLAYERS :
				m_textList[i] = "(1) Number of players : 2";
				break;
			case BRICKMODE :
				m_textList[i] = "(2) Game Mode : Brick";
				break;
			case DOUBLEMODE :
				m_textList[i] = "(2) Game Mode : Double";
				break;
			case DOWNTEAM :
				m_textList[i] = "(3) Screen position : Down";
				break;
			case UPTEAM :
				m_textList[i] = "(3) Screen position : Up";
				break;
			case BACK :
				m_textList[i] = "(4) Double mode position : Back";
				break;
			case FRONT :
				m_textList[i] = "(4) Double mode position : Front";
				break;
			case VERSUS :
				m_textList[i] = "(5) Double mode two players game mode : Versus";
				break;
			case TEAM :
				m_textList[i] = "(5) Double mode two players game mode : Team";
				break;
			case BALLSPEEDSLOW :
				m_textList[i] = "(6) Ball Speed : Slow";
				break;
			case BALLSPEEDMEZZO :
				m_textList[i] = "(6) Ball Speed : Medium";
				break;
			case BALLSPEEDFAST :
				m_textList[i] = "(6) Ball Speed : Fast";
				break;
			case COMPUTEREASY :
				m_textList[i] = "(7) Computer Level : Easy";
				break;
			case COMPUTERMEDIUM :
				m_textList[i] = "(7) Computer Level : Medium";
				break;
			case COMPUTERHARD :
				m_textList[i] = "(7) Computer Level : Hard";
				break;
			case COMPUTERHARDEST :
				m_textList[i] = "(7) Computer Level : Hardest";
				break;	
		}

	for(int i = 0; i<m_textList.size(); i++)
	{
		SDL_Surface * temp = TTF_RenderText_Shaded(font, m_textList[i].c_str(), m_foreground, m_background);
		if(temp == NULL)
		{
			fprintf(stderr, "MMenu::displayText : Failed to convert text to SDL_Surface at index : %i\n", i);
			return false;
		}
		SDL_Rect destination = {m_x, m_y+(m_shift+m_size)*i, 0, 0};
		SDL_BlitSurface(temp, NULL, window, &destination);
		SDL_FreeSurface(temp);
	}
	TTF_CloseFont(font);

	return true;
}


std::stack<StateStruct>  MMenu::handleMenuInput(SDL_Event e, std::stack<StateStruct> stateStack, void (*pFunction)(void))
{
	if(pFunction == NULL)
	{
		fprintf(stderr, "MMenu::handleMenuInput : Error ! pFunction parameter not correct\n");
		return stateStack;
	}
	if(m_gameModeList.size() != m_keyList.size())
	{
		fprintf(stderr, "MMenu::handleMenuInput : Erreur attribut m_gameModeList et m_keyList de taille différente\n");
		return stateStack;
	}
	for(int i = 0; i<m_keyList.size(); i++)
	{
		if(e.key.keysym.sym == m_keyList[i])
			switch(m_gameModeList[i])
			{
				case ONEPLAYER :
					m_gameModeList[i] = TWOPLAYERS;
					break;
				case TWOPLAYERS :
					m_gameModeList[i] = ONEPLAYER;
					break;
				case BRICKMODE :
					m_gameModeList[i] = DOUBLEMODE;
					break;
				case DOUBLEMODE :
					m_gameModeList[i] = BRICKMODE;
					break;
				case VERSUS :
					m_gameModeList[i] = TEAM;
					break;
				case TEAM :
					m_gameModeList[i] = VERSUS;
					break;
				case DOWNTEAM :
					m_gameModeList[i] = UPTEAM;
					break;
				case UPTEAM :
					m_gameModeList[i] = DOWNTEAM;
					break;
				case BACK :
					m_gameModeList[i] = FRONT;
					break;
				case FRONT :
					m_gameModeList[i] = BACK ;
					break;
				case BALLSPEEDSLOW :
					m_gameModeList[i] = BALLSPEEDMEZZO ;
					break;
				case BALLSPEEDMEZZO :
					m_gameModeList[i] = BALLSPEEDFAST ;
					break;
				case BALLSPEEDFAST :
					m_gameModeList[i] = BALLSPEEDSLOW ;
					break;
				case COMPUTEREASY :
					m_gameModeList[i] = COMPUTERMEDIUM ;
					break;
				case COMPUTERMEDIUM :
					m_gameModeList[i] = COMPUTERHARD ;
					break;
				case COMPUTERHARD :
					m_gameModeList[i] = COMPUTERHARDEST ;
					break;
				case COMPUTERHARDEST :
					m_gameModeList[i] = COMPUTEREASY ;
					break;
			}
	}

	if(e.key.keysym.sym == SDLK_ESCAPE)
	{
		stateStack.pop();
	}
	else if ((e.key.keysym.sym == SDLK_RETURN) && (pFunction != NULL))
	{
		StateStruct temp;
		temp.StatePointer = pFunction;
		stateStack.push(temp);
	}

	return stateStack;
}

std::vector<PickUp> MMenu::getGameModeList()
{
	return m_gameModeList;
}

PickUp MMenu::getGameModeList(int index)
{
	if(index > m_gameModeList.size())
		index = m_gameModeList.size() - 1;
	else if(index < 0)
		index = 0;
	return m_gameModeList[index];
}

void MMenu::setGameModeList(std::vector<PickUp> gameModeList)
{
	m_gameModeList = gameModeList;
}

void MMenu::setGameModeList(PickUp gameMode, int index)
{
	if(index > m_gameModeList.size())
		index = m_gameModeList.size() - 1;
	else if(index < 0)
		index = 0;
	
	m_gameModeList[index] = gameMode;
}

std::vector<std::string> MMenu::getTextList()
{
	return m_textList;
}

std::string MMenu::getTextList(int index)
{
	if(index > m_textList.size())
		index = m_textList.size() - 1;
	else if(index < 0)
		index = 0;
	return m_textList[index];
}

void MMenu::setTextList(std::vector<std::string> textList)
{
	m_textList = textList;
}

void MMenu::setTextList(std::string text, int index)
{
	if(index > m_textList.size())
		index = m_textList.size() - 1;
	else if(index < 0)
		index = 0;
	
	m_textList[index] = text;
}
