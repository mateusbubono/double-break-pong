class MGameObject {

	constructor (quadParam, quadColor, isBrick = false)
	{
		this.m_quadParam = quadParam;
		this.m_quadColor = quadColor;
		this.m_isBrick = isBrick;
		this.m_lastBitmapY = Define.FEEDBACK_BITMAP_Y;
		this.m_firstBitmapY = this.m_quadParam.bitmap_location_y;
		this.m_feedbackFrames = 0;
		this.m_pixelGreenValue = 0;
		this.m_colorDownModulation = true;

	}

	checkBallCollision(ballEntity)
	{
		let cX, cY;
		if(ballEntity.screen_location_x + ballEntity.screen_location_w < this.m_quadParam.screen_location_x){

			cX = this.m_quadParam.screen_location_x;

		}else if(ballEntity.screen_location_x > this.m_quadParam.screen_location_x + this.m_quadParam.screen_location_w){
			
			cX = this.m_quadParam.screen_location_x + this.m_quadParam.screen_location_w;

		}else{

			cX = ballEntity.screen_location_x;

		}

		if(ballEntity.screen_location_y + ballEntity.screen_location_h < this.m_quadParam.screen_location_y){

			cY = this.m_quadParam.screen_location_y;

		}else if(ballEntity.screen_location_y > this.m_quadParam.screen_location_y + this.m_quadParam.screen_location_h){

			cY = this.m_quadParam.screen_location_y + this.m_quadParam.screen_location_h;

		}else{

			cY = ballEntity.screen_location_y;

		}

		let deltaX = cX - ballEntity.screen_location_x;
		let deltaY = cY - ballEntity.screen_location_y;

		let distanceSquared = deltaX * deltaX + deltaY * deltaY;
		let r = ballEntity.screen_location_w / 2;
		let rSquared = r*r;
		
		if(distanceSquared < rSquared){
			return true;
		}

		return false
	}

	checkWallCollision(dir)
	{
		let temp_x;
		switch(dir)
		{
			case Direction.LEFT :
				temp_x = this.m_quadParam.screen_location_x - this.m_quadParam.x_speed;
				break;
			case Direction.RIGHT :
				temp_x = this.m_quadParam.screen_location_x + this.m_quadParam.screen_location_w + this.m_quadParam.x_speed;
				break;
		}

		if( (temp_x <= 0) || (temp_x >= Define.WINDOW_WIDTH)){
			return true;
		}

		return false
	}

	bonusGameObject(randomBonus)
	{
		switch(randomBonus)
		{
			case  1 :
				if(this.m_quadParam.screen_location_w < (Define.PADDLE_WIDTH * 1.9))
				{
					this.m_quadParam.screen_location_w = this.m_quadParam.screen_location_w * 1.25;
					this.m_feedbackFrames = Define.FEEDBACKFRAMES;
				}
				break;
			case 2 :
				if(this.m_quadParam.x_speed < (Define.PLAYER_SPEED * 1.8))
				{
					this.m_quadParam.x_speed = this.m_quadParam.x_speed * 125 / 100;
					this.m_feedbackFrames = Define.FEEDBACKFRAMES;
				}
				break;
		}
	}

	malusGameObject(randomMalus)
	{
		switch(randomMalus)
		{
			case  1 :
				if(this.m_quadParam.screen_location_w > (Define.PADDLE_WIDTH * 0.6))
				{
					this.m_quadParam.screen_location_w = this.m_quadParam.screen_location_w * 0.75;
					this.m_feedbackFrames = Define.FEEDBACKFRAMES;
				}
				break;
			case 2 :
				if(this.m_quadParam.x_speed > (Define.PLAYER_SPEED * 0.6))
				{
					this.m_quadParam.x_speed = this.m_quadParam.x_speed * 0.75;
					this.m_feedbackFrames = Define.FEEDBACKFRAMES;
				}
				break;
		}
	}

	spriteFeedBack()
	{
		if(this.m_feedbackFrames <= 0)
		{
			this.m_quadParam.bitmap_location_y = this.m_firstBitmapY;
			this.m_lastBitmapY = Define.FEEDBACK_BITMAP_Y;
			return;
		}
		
		this.m_feedbackFrames--;

		let bitmapY = this.m_quadParam.bitmap_location_y;
		this.m_quadParam.bitmap_location_y = this.m_lastBitmapY;
		this.m_lastBitmapY = bitmapY;

	}

	get getQuadParam()
	{
		return this.m_quadParam;
	}

	set setQuadParam(quadParam)
	{
		this.m_quadParam = quadParam;
	}

	get getQuadColor()
	{
		return this.m_quadColor;
	}

	get getIsBrick()
	{
		return this.m_isBrick;
	}

	set setFeedbackFrames(feedbackFrames)
	{
		this.m_feedbackFrames = feedbackFrames;
	}

	get getPixelGreenValue()
	{
		return this.m_pixelGreenValue;
	}

	set setPixelGreenValue(pixelGreenValue)
	{
		this.m_pixelGreenValue = pixelGreenValue;
	}

	get isColorDownModulation()
	{
		return this.m_colorDownModulation;
	}

	set setIsColorDownModulation(colorDownModulation)
	{
		this.m_colorDownModulation = colorDownModulation;
	}
};

class MPlayer extends MGameObject
{
	constructor(quadParam, quadColor, leftButton, rightButton)
	{
		super(quadParam, quadColor);
		this.m_leftButton = leftButton;
		this.m_rightButton = rightButton;
		this.m_left_pressed = false;
		this.m_right_pressed = false;
	}

	playerEventHandler(event)
	{
		if(event.type == "keydown" && (this.m_left_pressed == false || this.m_right_pressed == false))
		{
			if(event.key == this.m_leftButton){
				this.m_left_pressed = true;
			} else if(event.key == this.m_rightButton){
				this.m_right_pressed = true;
			}
		}
		else if(event.type == "keyup" && (this.m_left_pressed == true || this.m_right_pressed == true))
		{
			if(event.key == this.m_leftButton){
				this.m_left_pressed = false;
			} else if(event.key == this.m_rightButton){
				this.m_right_pressed = false;
			}
		}
	}

	playerUpdateMovement()
	{
		if(this.m_left_pressed == true){
			if(this.checkWallCollision(Direction.LEFT) == false){
				this.m_quadParam.screen_location_x -= this.m_quadParam.x_speed;
			}
		}

		if(this.m_right_pressed == true){
			if(this.checkWallCollision(Direction.RIGHT) == false){
				this.m_quadParam.screen_location_x += this.m_quadParam.x_speed;
			}
		}
	}
}

class MComputer extends MGameObject
{
	constructor(quadParam, quadColor, computerLevel, decision)
	{
		super(quadParam, quadColor);
		this.m_decision = decision;

		switch(computerLevel)
		{
			case 0 :
				this.m_computerLevel = 75;
				break;
			case 1 :
				this. m_computerLevel = 50;
				break;
			case 2 :
				this.m_computerLevel = 25;
				break;
			case 3 :
				this.m_computerLevel = 1;
				break;
			default :
				this.m_computerLevel = 75;
				break;
		}
	}

	computerHandler(ballEntity, randomValue)
	{
		if(randomValue <= this.m_computerLevel){
			return;
		}

		let computer_x;
		let ball_center = ballEntity.screen_location_x + ballEntity.screen_location_w / 2;

		randomValue = Math.round(Math.random() * 60);
		if(randomValue == 1){
			this.m_decision = Math.round(Math.random() * 2) + 1;
		}

		switch(this.m_decision)
		{
			case 1 :
				computer_x = this.m_quadParam.screen_location_x;
				break;
			case 2 :
				computer_x = this.m_quadParam.screen_location_x + this.m_quadParam.screen_location_w;
				break;
			case 3 :
				computer_x = this.m_quadParam.screen_location_x + this.m_quadParam.screen_location_w / 2;
				break;
		}

		if(Math.abs(computer_x - ball_center) < 10){
			return;
		}

		if(computer_x > ball_center)
		{
			if(this.checkWallCollision(Direction.LEFT) == false){
				this.m_quadParam.screen_location_x -= this.m_quadParam.x_speed;
			}
		}
		else if(computer_x < ball_center)
		{
			if(this.checkWallCollision(Direction.RIGHT) == false){
				this.m_quadParam.screen_location_x += this.m_quadParam.x_speed;
			}
		}
	}
}

class  MBall extends MGameObject
{
	constructor(quadParam, quadColor, team)
	{
		super(quadParam, quadColor);
		this.m_team = team;
		this.m_score = [0, 0];
		this.m_lastTeam = PickUp.NOMODE;
		this.m_collisionLocked = false;
	}

	moveBall()
	{
		this.m_quadParam.screen_location_x += this.m_quadParam.x_speed;
		this.m_quadParam.screen_location_y += this.m_quadParam.y_speed;

		if(
			(
				(this.m_quadParam.x_speed < 0) &&
				this.checkWallCollision(Direction.LEFT)
			) ||
			(
				(this.m_quadParam.x_speed > 0) &&
				this.checkWallCollision(Direction.RIGHT)
			)
		)
		{
			this.m_quadParam.x_speed = -this.m_quadParam.x_speed;
		}

		if(this.m_quadParam.screen_location_y > Define.WINDOW_HEIGHT || this.m_quadParam.screen_location_y < 0)
		{

			if(this.m_quadParam.screen_location_y > Define.WINDOW_HEIGHT){
				this.m_score[0] += 1;
			}else if(this.m_quadParam.screen_location_y < 0){
				this.m_score[1] += 1;
			}

			let randomDir = Math.round(Math.random());
			if(randomDir == 0){
				randomDir = -1;
			}else{
				randomDir = 1;
			}

			this.m_quadParam.screen_location_y = Define.WINDOW_HEIGHT / 2;
			this.m_quadParam.screen_location_x = Define.WINDOW_WIDTH / 2;
			this.m_quadParam.x_speed = 0;
			this.m_quadParam.y_speed = randomDir * Define.BALL_SPEED_Y;
			this.m_lastTeam = PickUp.NOMODE;

			if(this.m_quadParam.y_speed < 0){
				this.m_team = Direction.DOWN;
			} else if(this.m_quadParam.y_speed > 0){
				this.m_team = Direction.UP;
			}
			return true;
		}

		return false;
	}

	handleBall(gameObjectList)
	{
		let returnList = [];
		let resetBallPosition = this.moveBall();
		let brickColor = "";
		let index_player = [-1, -1, -1, -1];
		let current_index = 0;

		for(let i = 0; i<gameObjectList.length; i++)
		{
			if(gameObjectList[i].checkBallCollision(this.m_quadParam) == true)
			{
				if(gameObjectList[i].getIsBrick == true)
				{
					this.pongBrickBall(gameObjectList[i].getQuadParam);
					brickColor = gameObjectList[i].getQuadColor;
				}
				else
				{
					if(index_player[current_index] == -1)
					{
						index_player[current_index] = i;
						current_index++;
						current_index %= 4;
					}

					if(this.m_collisionLocked == false){
						this.pongBall(gameObjectList[i].getQuadParam);
					}
					
					returnList.push(gameObjectList[i]);
				}
			}
			else 
			{
				if(this.m_collisionLocked == true){
					this.m_collisionLocked = false;
				}
				if(gameObjectList[i].getIsBrick == false)
				{
					if(index_player[current_index] == -1)
					{
						index_player[current_index] = i;
						current_index++;
						current_index %= 4;
					}
				}
				returnList.push(gameObjectList[i]);
			}
		}

		if((brickColor != "") && (index_player[0] != -1) && (index_player[1] != -1))
		{
			let randomValue = Math.round(Math.random())+ + 1;
			this.handleBrickBonus(brickColor, gameObjectList[index_player[0]], gameObjectList[index_player[1]], randomValue);
		}

		if(resetBallPosition == true)
		{
			for(let i = 0; i<4; i++)
			{
				if(index_player[i] == -1){
					break;
				}
				let resetEntity = gameObjectList[index_player[i]].getQuadParam;
				resetEntity.screen_location_w = Define.PADDLE_WIDTH;
				resetEntity.x_speed = Define.PLAYER_SPEED;
				gameObjectList[index_player[i]].setQuadParam = resetEntity; 
				gameObjectList[index_player[i]].setFeedbackFrames = 0;
			}
		}

		return returnList;
	}

	handleBrickBonus(brickColor, player1, player2, randomValue)
	{
		if(brickColor == "blanc")
		{
			if(this.m_quadParam.y_speed<0){
				this.m_quadParam.y_speed -= 1;
			}else{
				this.m_quadParam.y_speed += 1;
			}
			this.m_feedbackFrames = 30;
		}
		else if(this.m_lastTeam == PickUp.UPTEAM)
		{
			if(player1.getQuadParam.screen_location_y < Define.WINDOW_HEIGHT / 2)
			{
				if(player1.getQuadColor == brickColor){
					player1.bonusGameObject(randomValue);
				}else{
					player1.malusGameObject(randomValue);
				}
			}
			else 
			{
				if(player2.getQuadColor == brickColor){
					player2.bonusGameObject(randomValue);
				}else{
					player2.malusGameObject(randomValue);
				}
			}
		}
		else if(this.m_lastTeam == PickUp.DOWNTEAM)
		{
			if(player1.getQuadParam.screen_location_y > Define.WINDOW_HEIGHT / 2)
			{
				if(player1.getQuadColor == brickColor){
					player1.bonusGameObject(randomValue);
				}else{
					player1.malusGameObject(randomValue);
				}
			}
			else
			{
				if(player2.getQuadColor == brickColor){
					player2.bonusGameObject(randomValue);
				}else{
					player2.malusGameObject(randomValue);
				}
			}
		}
	}

	/*pongBall(padEntity)
	{
		if( ((padEntity.screen_location_y < Define.WINDOW_HEIGHT / 2) && (this.m_team == Direction.DOWN)) || ((padEntity.screen_location_y > Define.WINDOW_HEIGHT / 2) && (this.m_team == Direction.UP)) )
		{
			let paddle_center = padEntity.screen_location_x + padEntity.screen_location_w / 2;
			let ball_center = this.m_quadParam.screen_location_x + this.m_quadParam.screen_location_w / 2;

			let paddle_location = ball_center - paddle_center;

			this.m_quadParam.x_speed = paddle_location / Define.BALL_SPEED_MODIFIER;
			this.m_quadParam.y_speed = -this.m_quadParam.y_speed;

			if(this.m_quadParam.y_speed < 0)
			{
				this.m_team = Direction.DOWN;
				this.m_lastTeam = PickUp.DOWNTEAM;
			}
			else if(this.m_quadParam.y_speed > 0)
			{
				this.m_team = Direction.UP;
				this.m_lastTeam = PickUp.UPTEAM;
			}
		}
	}*/

	pongBall(padEntity)
	{
		if( ((padEntity.screen_location_y < Define.WINDOW_HEIGHT / 2) && (this.m_team == Direction.DOWN)) || ((padEntity.screen_location_y > Define.WINDOW_HEIGHT / 2) && (this.m_team == Direction.UP)) && this.m_collisionLocked == false)
		{
			let collisionDirection = this.checkForOverlap(padEntity);
			
			if((padEntity.screen_location_y < Define.WINDOW_HEIGHT / 2 && (collisionDirection == Direction.DOWN || collisionDirection == Direction.CORNERTOP) ) || (padEntity.screen_location_y > Define.WINDOW_HEIGHT / 2 && (collisionDirection == Direction.UP || collisionDirection == Direction.CORNERTOP) ))
			{
				let paddle_center = padEntity.screen_location_x + padEntity.screen_location_w / 2;
				let ball_center = this.m_quadParam.screen_location_x + this.m_quadParam.screen_location_w / 2;

				let paddle_location = ball_center - paddle_center;

				let ballSpeedModifier = Math.round(Define.BALL_SPEED_MODIFIER / (Math.abs(this.m_quadParam.y_speed) / 5));

				ballSpeedModifier =  Math.round(ballSpeedModifier * (padEntity.screen_location_w / Define.PADDLE_WIDTH))

				this.m_quadParam.x_speed = paddle_location / ballSpeedModifier;
				this.m_quadParam.y_speed = -this.m_quadParam.y_speed;
			}
			else if(collisionDirection == Direction.LEFT)
			{
				if(this.m_quadParam.x_speed > 0){
					this.m_quadParam.x_speed =  (this.checkWallCollision(Direction.LEFT) ? this.m_quadParam.x_speed : -this.m_quadParam.x_speed);
				}
				else{
					this.m_quadParam.x_speed -= (this.checkWallCollision(Direction.LEFT) ? -this.m_quadParam.x_speed : 1); 
				}
				 
				this.m_collisionLocked = true;
			}
			else if(collisionDirection == Direction.RIGHT)
			{
				if(this.m_quadParam.x_speed < 0){
					this.m_quadParam.x_speed = (this.checkWallCollision(Direction.RIGHT) ? this.m_quadParam.x_speed : -this.m_quadParam.x_speed);
				}
				else{
					this.m_quadParam.x_speed += (this.checkWallCollision(Direction.RIGHT) ? -this.m_quadParam.x_speed : 1);
				}
				 
				this.m_collisionLocked = true;
			}

			if(this.m_quadParam.y_speed < 0)
			{
				this.m_team = Direction.DOWN;
				this.m_lastTeam = PickUp.DOWNTEAM;
			}
			else if(this.m_quadParam.y_speed > 0)
			{
				this.m_team = Direction.UP;
				this.m_lastTeam = PickUp.UPTEAM;
			}
		}
	}

	checkForOverlap(entity)
	{
		let entityLeftX = entity.screen_location_x,
		entityRightX = entity.screen_location_x + entity.screen_location_w,
		entityUpY = entity.screen_location_y,
		entityDownY = entity.screen_location_y + entity.screen_location_h;

		let entityCenterX = entity.screen_location_x + entity.screen_location_w / 2,
		entityCenterY = entity.screen_location_y + entity.screen_location_h / 2;

		let ballLeftX = this.m_quadParam.screen_location_x,
		ballRightX = this.m_quadParam.screen_location_x + this.m_quadParam.screen_location_w,
		ballUpY = this.m_quadParam.screen_location_y,
		ballDownY = this.m_quadParam.screen_location_y + this.m_quadParam.screen_location_h;

		let ballCenterX = this.m_quadParam.screen_location_x + this.m_quadParam.screen_location_w / 2,
		ballCenterY = this.m_quadParam.screen_location_y + this.m_quadParam.screen_location_h / 2;

		let overlapLeftX = 0, overlapRightX = 0, overlapUpY = 0, overlapDownY = 0, overlapDistanceX = 0, overlapDistanceY = 0;

		if(entityLeftX >= ballLeftX){
			overlapLeftX = entityLeftX;
		}else{
			overlapLeftX = ballLeftX;
		}

		if(entityRightX <= ballRightX){
			overlapRightX = entityRightX;
		}else{
			overlapRightX = ballRightX;
		}

		if(entityUpY >= ballUpY){
			overlapUpY = entityUpY;
		}else{
			overlapUpY = ballUpY;
		}

		if(entityDownY <= ballDownY){
			overlapDownY = entityDownY;
		}else{
			overlapDownY = ballDownY;
		}

		overlapDistanceX = overlapRightX - overlapLeftX;
		overlapDistanceY = overlapDownY - overlapUpY;

		if(overlapDistanceX == overlapDistanceY)
		{
			if(ballCenterY > entityCenterY){
				return Direction.CORNERTOP;
			}else{
				return Direction.CORNERBOTTOM;
			};
		}
		else if(overlapDistanceX > overlapDistanceY)
		{
			if(ballCenterY > entityCenterY){
				return Direction.DOWN;
			}else{
				return Direction.UP;
			}
		}
		else if(overlapDistanceX < overlapDistanceY)
		{
			if(ballCenterX > entityCenterX){
				return Direction.RIGHT;
			}else{
				return Direction.LEFT;
			}
		}

		return Direction.NONE;
	}

	/*pongBrickBall(brickParam)
	{
		let brickLeftX = brickParam.screen_location_x,
		brickRightX = brickParam.screen_location_x + brickParam.screen_location_w,
		brickUpY = brickParam.screen_location_y,
		brickDownY = brickParam.screen_location_y + brickParam.screen_location_h;

		let brickCenterX = brickParam.screen_location_x + brickParam.screen_location_w / 2,
		brickCenterY = brickParam.screen_location_y + brickParam.screen_location_h / 2;

		let ballLeftX = this.m_quadParam.screen_location_x,
		ballRightX = this.m_quadParam.screen_location_x + this.m_quadParam.screen_location_w,
		ballUpY = this.m_quadParam.screen_location_y,
		ballDownY = this.m_quadParam.screen_location_y + this.m_quadParam.screen_location_h;

		let ballCenterX = this.m_quadParam.screen_location_x + this.m_quadParam.screen_location_w / 2,
		ballCenterY = this.m_quadParam.screen_location_y + this.m_quadParam.screen_location_h / 2;

		let overlapLeftX = 0, overlapRightX = 0, overlapUpY = 0, overlapDownY = 0, overlapDistanceX = 0, overlapDistanceY = 0;

		if(brickLeftX >= ballLeftX){
			overlapLeftX = brickLeftX;
		}else{
			overlapLeftX = ballLeftX;
		}

		if(brickRightX <= ballRightX){
			overlapRightX = brickRightX;
		}else{
			overlapRightX = ballRightX;
		}

		if(brickUpY >= ballUpY){
			overlapUpY = brickUpY;
		}else{
			overlapUpY = ballUpY;
		}

		if(brickDownY <= ballDownY){
			overlapDownY = brickDownY;
		}else{
			overlapDownY = ballDownY;
		}

		overlapDistanceX = overlapRightX - overlapLeftX;
		overlapDistanceY = overlapDownY - overlapUpY;

		if(overlapDistanceX == overlapDistanceY)
		{
			this.m_quadParam.x_speed = - this.m_quadParam.x_speed;
			this.m_quadParam.y_speed = -this.m_quadParam.y_speed;
		}
		else if(overlapDistanceX > overlapDistanceY)
		{
			if(ballCenterY > brickCenterY){
				this.ballBrickResponse(Direction.DOWN);
			}else{
				this.ballBrickResponse(Direction.UP);
			}
		}
		else if(overlapDistanceX < overlapDistanceY)
		{
			if(ballCenterX > brickCenterX){
				this.ballBrickResponse(Direction.RIGHT);
			}else{
				this.ballBrickResponse(Direction.LEFT);
			}
		}

		if(this.m_quadParam.y_speed < 0){
			this.m_team = Direction.DOWN;
		}else if(this.m_quadParam.y_speed > 0){
			this.m_team = Direction.UP;
		}
	}

	ballBrickResponse(brickSide)
	{
		let multx = 1, multy = 1;

		if(this.m_quadParam.x_speed > 0)
		{
			if(this.m_quadParam.y_speed > 0)
			{
				if(brickSide == Direction.LEFT || brickSide == Direction.DOWN){
					multx = -1;
				}else{
					multy = -1;
				}
			}
			else if(this.m_quadParam.y_speed < 0)
			{
				if(brickSide == Direction.LEFT || brickSide == Direction.UP){
					multx = -1;
				}else{
					multy = -1;
				}
			}
		}
		else if(this.m_quadParam.x_speed < 0)
		{
			if(this.m_quadParam.y_speed > 0)
			{
				if(brickSide == Direction.RIGHT || brickSide == Direction.DOWN){
					multx = -1;
				}else{
					multy = -1;
				}
			}
			else if(this.m_quadParam.y_speed < 0)
			{
				if(brickSide == Direction.UP || brickSide == Direction.RIGHT){
					multx = -1;
				}else{
					multy = -1;
				}
			}
		}
		else if(this.m_quadParam.x_speed == 0)
		{
			multy = -1;
		}

		this.m_quadParam.x_speed = multx * this.m_quadParam.x_speed;
		this.m_quadParam.y_speed = multy * this.m_quadParam.y_speed;
	}*/

	pongBrickBall(brickEntity)
	{
		let multx = 1, multy = 1;

		let brickSide = this.checkForOverlap(brickEntity);
		if(brickSide == Direction.CORNERTOP || brickSide == Direction.CORNERBOTTOM)
		{
			multx = -1;
			multy = -1;
		}
		else if(this.m_quadParam.x_speed > 0)
		{
			if(this.m_quadParam.y_speed > 0)
			{
				if(brickSide == Direction.LEFT || brickSide == Direction.DOWN){
					multx = -1;
				}else{
					multy = -1;
				}
			}
			else if(this.m_quadParam.y_speed < 0)
			{
				if(brickSide == Direction.LEFT || brickSide == Direction.UP){
					multx = -1;
				}else{
					multy = -1;
				}
			}
		}
		else if(this.m_quadParam.x_speed < 0)
		{
			if(this.m_quadParam.y_speed > 0)
			{
				if(brickSide == Direction.RIGHT || brickSide == Direction.DOWN){
					multx = -1;
				}else{
					multy = -1;
				}
			}
			else if(this.m_quadParam.y_speed < 0)
			{
				if(brickSide == Direction.UP || brickSide == Direction.RIGHT){
					multx = -1;
				}else{
					multy = -1;
				}
			}
		}
		else if(this.m_quadParam.x_speed == 0)
		{
			multy = -1;
		}

		this.m_quadParam.x_speed = multx * this.m_quadParam.x_speed;
		this.m_quadParam.y_speed = multy * this.m_quadParam.y_speed;

		if(this.m_quadParam.y_speed < 0){
			this.m_team = Direction.DOWN;
		}else if(this.m_quadParam.y_speed > 0){
			this.m_team = Direction.UP;
		}
	}

	setScore(index, value)
	{
		if(index > 1){
			index = 1;
		}else if(index < 0){
			index = 0;
		}
		this.m_score[index] = value;
	}

	getScore(index)
	{
		if(index > 1){
			index = 1;
		}else if(index < 0){
			index = 0;
		}
		return this.m_score[index];
	}

}