let Define = {
	//Windows parameters
	WINDOW_WIDTH : 800,
	WINDOW_HEIGHT : 600,
	WINDOW_CAPTION : "Pong",

	//Time parameters
	FRAME_PER_SECOND : 90,
	FRAME_RATE : 1000 / 75,

	//Location in bitmap file
	PADDLE_BITMAP_X : 0,
	PADDLE_BITMAP_RED_Y : 0,
	PADDLE_BITMAP_BLUE_Y : 20,
	BALL_BITMAP_X : 100,
	BALL_BITMAP_Y : 0,
	BRICK_WHITE_X : 200,
	BRICK_BLUE_X : 120,
	BRICK_RED_X : 160,
	BRICK_Y : 0,
	FEEDBACK_BITMAP_Y : -50,

	//Location paddles in-game
	COMPUTER_Y : 30,
	COMPUTER_FRONT_Y : 30 + 150,
	PLAYER_Y : 550,
	PLAYER_FRONT_Y : 550 - 150,

	//Paddle's dimensions
	PADDLE_WIDTH : 100,
	PADDLE_HEIGHT : 20,

	//Paddle's speed
	PLAYER_SPEED : 10,
	COMPUTER_SPEED : 10,

	//Ball's diameter
	BALL_DIAMETER : 20,
	BALL_WIDTH : 40,
	BALL_HEIGHT : 40, 

	//Ball's speed
	BALL_SPEED_MODIFIER : 5,
	BALL_SPEED_Y : 5,

	//Score's location
	COMPUTER_SCORE_X : 10,
	COMPUTER_SCORE_Y : 10,
	PLAYER_SCORE_X : 150,
	PLAYER_SCORE_Y : 10,

	BITMAP_PATH : "sprite/pong.bmp",
	FONT_PATH : "sprite/Ubuntu-B.ttf" ,

	FEEDBACKFRAMES : 20
}

let Direction = {
	LEFT : 0,
	RIGHT : 1,
	UP : 2,
	DOWN : 3,
	CORNERTOP : 4,
	CORNERBOTTOM : 5,
	NONE : 6,
};

let PickUp = {
	NOMODE : 0,
	ONEPLAYER : 1,
	TWOPLAYERS : 2,
	BRICKMODE : 3,
	DOUBLEMODE : 4,
	VERSUS : 5,
	TEAM : 6,
	DOWNTEAM : 7,
	UPTEAM : 8,
	BACK : 9,
	FRONT : 10,
	BALLSPEEDSLOW : 11,
	BALLSPEEDMEZZO : 12,
	BALLSPEEDFAST : 13,
	COMPUTEREASY : 14,
	COMPUTERMEDIUM : 15,
	COMPUTERHARD : 16,
	COMPUTERHARDEST : 17,
};

let Entity = {
	screen_location_x : 0,
	screen_location_y : 0,
	screen_location_w : 0,
	screen_location_h : 0,
	bitmap_location_x : 0,
	bitmap_location_y : 0,
	bitmap_location_w : 0,
	bitmap_location_h : 0,
	x_speed : 0,
	y_speed : 0,
};

function EntityConstructor(screen_location_x, screen_location_y, screen_location_w, screen_location_h, bitmap_location_x, bitmap_location_y, bitmap_location_w, bitmap_location_h, x_speed, y_speed)
{
	let entity = Object.create(Entity);
	entity.screen_location_x = screen_location_x;
	entity.screen_location_y = screen_location_y;
	entity.screen_location_w = screen_location_w;
	entity.screen_location_h = screen_location_h;
	entity.bitmap_location_x = bitmap_location_x;
	entity.bitmap_location_y = bitmap_location_y;
	entity.bitmap_location_w = bitmap_location_w;
	entity.bitmap_location_h = bitmap_location_h;
	entity.x_speed = x_speed;
	entity.y_speed = y_speed;

	return entity;
}

if (typeof module != "undefined" && module.exports && (typeof window == "undefined" || window.exports != exports))
  module.exports = Define;
if (typeof global != "undefined" && !global.Define)
  global.Define = Define;

if (typeof module != "undefined" && module.exports && (typeof window == "undefined" || window.exports != exports))
  module.exports = Entity;
if (typeof global != "undefined" && !global.Entity)
  global.Entity = Entity;

  if (typeof module != "undefined" && module.exports && (typeof window == "undefined" || window.exports != exports))
  module.exports = Direction;
if (typeof global != "undefined" && !global.Direction)
  global.Direction = Direction;

  if (typeof module != "undefined" && module.exports && (typeof window == "undefined" || window.exports != exports))
  module.exports = PickUp;
if (typeof global != "undefined" && !global.PickUp)
  global.PickUp = PickUp;

 