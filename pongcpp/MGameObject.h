/*!
*	\file MGameObject.h
*	\author Boris Merminod
*	\brief Header file of the following classes : MGamer, MPlayer and MComputer
*	\version 1.1.2
*
*	This file is a header that contains the 
*	declaration class of MGameObject and its two 
*	herited classes MPlayer and MComputer. The file
*	contains  two classes that allowed tests on MPlayer 
*	and MComputer.
*/

#ifndef MGAMEOBJECT_H
#define MGAMEOBJECT_H

#include "Defines.h"

/*!
*	\class MGameObject
*	\brief This class manage all game objects
*
*	This class manage all the common properties of a game
*	object
*/
class MGameObject
{
	public :
		/*!
		*	\brief Constructor of the MGameObject class
		*
		*	\param padParam (Entity) : Define the position
		*	and the dimensions of the game object
		*	player
		*	\param padColor (std::string) : Define the
		*	color of the pad which be used for bonuses and 
		*	maluses in brick mode
		*	\param isBrick (bool) : Flag that indicate if
		*	the GameObject is a brick or not	
		*/
		MGameObject(Entity quadParam, std::string quadColor, bool isBrick = false);
		
		/*!
		*	\brief function checkBallCollision -> check the collision between the gameObject and the ball
		*
		*	The function check the collision between a gameObject (pad or brick) and the ball game object.
		*	it return true if a collision is detected or false if it's not
		*
		*	\param ball (Entity) : The position and dimension of the ball game object
		*	
		*	\return the function return true if a collision is detected or false if it's not
		*/
		bool checkBallCollision(Entity ball);

		/*!
		*	\brief function checkWallCollision() -> Check the collision between the gameObject and the screen
		*
		*	The function check the collision between the gameObject (pad or brick or ball) and the screen. 
		*	It return true if collision is detected or false if it not.
		*
		*	\param dir (Direction) : Enumeration that indicate the side of collision : LEFT or RIGHT
		*/
		bool checkWallCollision(Direction dir);

		/*!
		*	\brief function bonusGameObject() -> increase the gameObject's speed or size in response to a bonus effect 
		*
		*	The function increase the game object's size or speed in response to a bonus effect. The modification is limited
		*	at two times the basic size or speed of the game object
		*
		*	\param randomBonus (int) : The value can be 1 (increase the game object's size) or 2 (increase the game object's speed)
		*/
		void bonusGameObject(int randomBonus);

		/*!
		*	brief function malusGameObject() -> decrease the game object's speed or size in response to a malus effect
		*
		*	The function decrease the game object's size or speed in response to a malus effect. The modification is limited at
		*	two times the basic size or speed of the game object
		*
		*	\param randomMalus (int) : The value can be 1 (decrease the game object's size) or 2 (decrease the game object's speed)
		*/
		void malusGameObject(int randomMalus);

		/*!
		*	\brief function spriteFeedback() -> After 
		*	a malus or a bonus, this function generate
		*	a feedback
		*
		*	When a player receive a bonus or a malus, 
		*	the spriteFeedback function will generate
		*	a feedback to warn the player that 
		*	something happening to his paddle or
		*	the opponent's paddle
		*/
		void spriteFeedback();

		/*!
		*	\brief function getQuadParam() -> Accessor of
		*	the m_quadParam member
		*
		*	\return m_quadParam (Entity) : the function
		*	return the m_quadParam member
		*/
		Entity getQuadParam();

		/*!
		*	\brief function setQuadParam() -> Mutator of the
		*	the m_quadParam member
		*
		*	\param quadParam (Entity) : The new value to
		*	assign at the m_quadParam member
		*/
		void setQuadParam(Entity quadParam);

		/*!
		*	\brief function getQuadColor() -> Accessor of
		*	the color member
		*
		*	\return m_quadColor (std::string) : the function
		*	return the m_quadColor member.
		*/
		std::string getQuadColor();

		/*!
		*	\brief getIsBrick() -> Accessor of the isBrick member
		*
		*	\return m_isBrick (bool) : the function return the isBrick member
		*/
		bool getIsBrick();

		/*!
		*	\brief getFeedbackFrames() -> Accessor of the
		*	feedbackFrames member
		*
		*	\return m_feedbackFrames (int) : the function
		*	return the feedback frames member
		*/
		int getFeedbackFrames();

		/*!
		*	\brief setFeedbackFrames() -> Mutator of the
		*	feedbackFrames member
		*
		*	\param feedbackFrames (int) : the new value to 
		*	assign at the feedback frames member
		*/
		void setFeedbackFrames(int feedbackFrames);

		int getPixelGreenValue();
		void setPixelGreenValue(int pixelGreenValue);
		bool isColorDownModulation();
		void setIsColorDownModulation(bool colorDownModulation);
	protected :
		Entity m_quadParam; /*!< Define the position and the dimensions of the game object*/
		std::string m_quadColor; /*!< Define the color of the game object*/
		bool m_isBrick; /*!<Define if the game object is a brick or not*/
		int m_lastBitmapY; /*!< For the sprite's feedback, define the location in the bitmap file of the last sprite to draw*/
		int m_firstBitmapY; /*!< For the sprite's feedback, define the location of the sprite without feedback effect in the bitmap file*/
		int m_feedbackFrames; /*!< Value that indicate how long the feedback will run*/
		int m_pixelGreenValue;
		bool m_colorDownModulation;
};

/*!
*	\class MPlayer
*	\brief MPlayer inherit from MGameObject class. It manage human player
*
*	MPlayer class inherit from MGameObject class, it manage human player with function
*	acting like an input manager
*/
class MPlayer: public MGameObject
{
	public :

		/*!
		*	\brief Constructor of MPlayer class
		*
		*	\param padParam (Entity) : Define the position and the dimensions of
		*	the pad used by the player
		*	\param padColor (std::string) : the color of the pad's player
		*	\param leftButton (SDL_Keycode) : Define the left button's code to move
		*	the player's pad on the left
		*	\param rightButton (SDL_Keycode) : Define the right button's code to move
		*	the player's pad on the right 
		*/
		MPlayer(Entity quadParam, std::string quadColor ,SDL_Keycode leftButton, SDL_Keycode rightButton);

		/*!
		*	\brief function playerEventHandler() -> Manage the player's inputs
		*
		*	The function manage the player's inputs to move the pad from the left
		*	or from the right
		*
		*	\param e (SDL_Event) : Data structure that contains the player's inputs
		*/
		void playerEventHandler(SDL_Event e);


	private :
		SDL_Keycode m_leftButton; /*!< SDL code that define the button allowing the player to move his pad on the left*/
		SDL_Keycode m_rightButton; /*!< SDL code that define the button allowing the player to move his pad on the right*/
		bool m_left_pressed; /*!< Indicate if the button to move on the left is pressed or not*/
		bool m_right_pressed; /*!< Indicate if the button to move on the right is pressed or not*/
};

/*!
*	\class MComputer
*	\brief MComputer inherit from MGameObject class. It manage the computer player
*
*	The MComputer inherit from MGameObject class. It manage computer player with a function
*	acting to manage artificial intelligence of the pad
*/
class MComputer : public MGameObject
{
	public :

		/*!
		*	\brief Constructor of the MComputer class
		*
		*	\param padParam (Entity) : Define the position and the dimensions of
		*	the pad used by the player
		*	\param quadColor (std::string) : the color of the pad's player
		*	\param computerLevel (int) : it is the computer difficulty, that adjust the AI Level 
		*	\param decision (int) : the computer can adjust its behavior into three
		*	decision to return the ball
		*/
		MComputer(Entity quadParam, std::string quadColor, int computerLevel, int decision);

		/*!
		*	\brief function computerHandler() -> Manage the behavior of the AI pad
		*
		*	The function adjust the computer behavior to move the pad according the 
		*	ball's position
		*
		*	\param ball (Entity) : The ball's parameters
		*	\param randomValue (int) : The randomValue is
		*	compared to the computerLevel to adjust the
		*	AI reaction's speed 
		*/
		void computerHandler(Entity ball, int randomValue);

		/*!
		*	\brief function getDecision() -> Accessor of the decision member
		*
		*	\return m_decision (int) : return the decision member
		*/
		int getDecision();

		/*!
		*	\brief function setDecision() -> Mutator of the decision member
		*
		*	\param decision (int) : set the new value of the m_decision member
		*/
		void setDecision(int decision);

		/*!
		*	\brief function getComputerLevel() -> Accessor of
		*	the computer level member
		*
		*	\return m_computerLevel (int) : return the computer
		* 	level member
		*/
		int getComputerLevel();

		
		/*!
		*	\brief function setComputerLevel() -> Mutator of
		*	the computer level member
		*
		*	\param computerLevel (int) : The new computer
		*	level value between 1 and 4
		*/
		void setComputerLevel(int computerLevel);
	
	private :
		int m_computerLevel; /*!< value that influence computer's responsiveness*/
		int m_decision; /*!< contains a random decision according the last speed member*/
};

/*!
*	\class MBall
*	\brief inherit from MGameObject's class, it manage the ball gameobject
*
*	The MBall class inherit from the MGameObject's class. It manage the ball gameobject : movement and collision effects
*/
class MBall : public MGameObject
{
	public :
		static int cBALL_SPEED_Y;

		/*!
		*	\brief Constructor of the MBall's class.
		*
		*	\param quadParam (Entity) : Define the dimensions and position of the game object
		*	\param quadColor (std::string) : Define the ball's color
		*	\param team (Direction) : Define the team who hit the ball last time (UP or DOWN), to manage the collision's in the 
		*	double mode 
		*/
		MBall(Entity quadParam, std::string quadColor, Direction team);

		/*!
		*	\brief moveBall() -> move the ball once by frame
		*
		*	moveBall() is called once by frame to update the ball's position. The function also check the ball-wall collision.
		*	if the bool moving out of the screen,
		*	the ball's location will be reset and the
		*	function will return true. Otherwise it will
		*	return false
		*
		*	\return (bool) : true if the ball position is
		*	reset, false if it's not
		*/
		bool moveBall();

		/*!
		*	\brief handleBall() -> Update the ball position, check the collisions, and run the bonuses or maluses
		*
		*	The function is called once by frame, it call the moveBall function to update the position of the ball,
		*	it check the ball-pad's collisions and the ball-brick's collisions and calculate a response of it and it run the
		*	bonuses or maluses effect when it have to.
		*
		*	\param gameObjectList (std::vector<MGameObject*>) : The function take a list of pointer on all the game objects in game, to manage
		*	the collision's effects.
		*
		*	\return returnList (std::vector<MGameObject*>) : The function update the gameObjectList and return it 
		*/
		std::vector<MGameObject*> handleBall(std::vector<MGameObject*> gameObjectList);

		/*!
		*	\brief handleBrickBonus() -> Manage the bonuses's or maluses's effects
		*
		*	the function manage who receive a bonus or a malus. When the player/computer is picked by the function it called
		*	theirselves bonus/malus methods to update their parameters
		*
		*	\param brickColor (std::string) : The brick's color that indicate if the player/computer receive a malus or a bonus
		*	\param player1 (MGameObject *) : A player one is going to be update if it is selected by the function
		*	\param player2 (MGameObject *) : A player two is going to be update if it is selected by the function
		*	\param randomValue (int) : A random value that indicate which bonus or malus will be selected 
		*/
		void handleBrickBonus(std::string brickColor, MGameObject *player1, MGameObject *player2, int randomValue);

		/*!
		*	\brief pongBall() -> Manage the collision's response between the ball and a pad
		*
		*	The function manage the collision's response between the ball and a pad
		*
		*	padParam (Entity) : The pad's position and dimensions to calculate collision's effect 
		*/
		void pongBall(Entity padParam);

		/*!
		*	\brief function pongBrickBall() -> Manage the
		*	collision's detection and response between
		* 	the ball and a pad
		*
		*	\param brickParam (Entity) : the brick's
		*	location and dimension
		*/
		void pongBrickBall(Entity brickParam);

		/*!
		*	getTeam() -> Accessor of the team member
		*
		*	\return m_team (Direction) : The function return the m_team member	
		*/
		Direction getTeam();

		/*!
		*	setTeam() -> Mutator of the team member
		*
		*	\param team (Direction) :Update the m_team member
		*/
		void setTeam(Direction team);

		/*!
		*	\brief function getScore() -> Accessor of one
		*	element of the m_score array member
		*
		*	\param index (int) : the index number of the 
		*	array element to return
		*
		*	\return m_score[index] (int) : return the score
		*	at the given index from the m_score array member
		*/
		int getScore(int index);

		/*!
		*	\brief function setScore() -> Mutator of one
		*	element of the m_score array member
		*
		*	\param index (int) : index of the element score
		*	array to replace
		*	\param value (int) : new score replace at
		*	the score array member
		*/
		void setScore(int index, int value);

		/*!
		*	\brief function getLastTeam() -> Accessor of
		*	lastTeam member
		*
		*	\return m_lastTeam (PickUp) : return the value
		*	from the lastTeam member;
		*/
		PickUp getLastTeam();

		/*!
		*	\brief funtion setLastTeam() -> Mutator of
		*	lastTeam member
		*
		*	\param lastTeam (PickUp) : New value to
		*	associate at m_lastTeam
		*/
		void setLastTeam(PickUp lastTeam);

	private :

		/*!
		*	\brief function checkForOverlap() -> If a collision between the ball and an other game object is raised, checkForOverlap is called and return which side of the game object received the collision
		*
		*	\param entity (Entity) : Game object location and dimensions
		*	
		*	\return (Direction) : The side of game object which received the collision is returned
		*/
		Direction checkForOverlap(Entity entity);

		Direction m_team; /*!< Contain the last team who hit the ball (UP or DOWN)*/
		int m_score[2]; /*!<Contain the score of the two different team*/
		PickUp m_lastTeam; /*!<Contain the last team who hit the ball*/
		bool m_collisionLocked; /*!< If a paddle failed to resend the ball by a hit from aside, the collision is then locked until the ball cannot raised an other collision with the same paddle (true) */
};

//~~~~~ Tests unitaires ~~~~~

//Test of the MGameObject's class
class Test_MGameObject
{
	public :
		void test_checkBallCollision();
		void test_checkWallCollision();
		void test_bonusGameObject();
		void test_malusGameObject();
};

// Test of the MPlayer's class
class Test_MPlayer
{
	public :
		void test_playerEventHandler();
};

// Test of the MComputer's class
class Test_MComputer
{
	public :
		void test_computerHandler();
};

//Test of the MBall's class
class Test_MBall
{
	public :
		void test_pongBall();
		void test_pongBrickBall();
		void test_handleBall();
		void test_moveBall();
		void test_handleBrickBonus();
};


#endif
