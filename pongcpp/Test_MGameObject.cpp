#include "MGameObject.h"
//~~~~~ TESTS UNITAIRES ~~~~~

void test_gameObject()
{
	Test_MGameObject test;
	test.test_checkWallCollision();
	test.test_checkBallCollision();
	test.test_bonusGameObject();
	test.test_malusGameObject();
}

void test_player()
{
	Test_MPlayer test;
	test.test_playerEventHandler();
}

void test_computer()
{
	Test_MComputer test;
	test.test_computerHandler();
}

void test_ball()
{
	Test_MBall test;
	test.test_moveBall();
	test.test_pongBall();
	test.test_pongBrickBall();
	test.test_handleBall();
	test.test_handleBrickBonus();
}
// ~~~~~ Tests de la classe MGameObject ~~~~~

void Test_MGameObject::test_checkWallCollision()
{
	printf("Test_MGameObject::test_checkWallCollision : Start testing\n");

	Entity gameObjectEntity;
	gameObjectEntity.screen_location.x = 50;
	gameObjectEntity.screen_location.y = 50;
	gameObjectEntity.screen_location.h = 50;
	gameObjectEntity.screen_location.w = 50;
	gameObjectEntity.x_speed = 10;
	gameObjectEntity.y_speed = 10;

	MGameObject gameObjectTest(gameObjectEntity, "bleu", true);

	//Absence de collision
	assert(gameObjectTest.checkWallCollision(LEFT) == false);
	assert(gameObjectTest.checkWallCollision(RIGHT) == false);

	//Collision avec le bord gauche de l'écran
	gameObjectEntity.screen_location.x = 0;
	gameObjectTest.setQuadParam(gameObjectEntity);
	assert(gameObjectTest.checkWallCollision(LEFT) == true);
	assert(gameObjectTest.checkWallCollision(RIGHT) == false);

	//Collision avec le bord droit de l'écran
	gameObjectEntity.screen_location.x = WINDOW_WIDTH - gameObjectEntity.screen_location.h;
	gameObjectTest.setQuadParam(gameObjectEntity);

	assert(gameObjectTest.checkWallCollision(LEFT) == false);
	assert(gameObjectTest.checkWallCollision(RIGHT) == true);

	printf("Test_MGameObject::test_checkWallCollision : OK\n\n");

}

void Test_MGameObject::test_checkBallCollision()
{
	printf("Test_MGameObject::test_checkBallCollision : Start testing\n");
	Entity ballTest;
	ballTest.screen_location.x = 50;
	ballTest.screen_location.y = 50;
	ballTest.screen_location.h = 50;
	ballTest.screen_location.w = 50;
	ballTest.x_speed = 10;
	ballTest.y_speed = 10;

	Entity gameObjectEntity;
	gameObjectEntity.screen_location.x = 150;
	gameObjectEntity.screen_location.y = 150;
	gameObjectEntity.screen_location.h = 50;
	gameObjectEntity.screen_location.w = 50;
	gameObjectEntity.x_speed = 10;
	gameObjectEntity.y_speed = 0;

	MGameObject gameObjectTest(gameObjectEntity, "bleu", true);
	assert(gameObjectTest.checkBallCollision(ballTest) == false);

	//Collision par le dessus
	ballTest.screen_location.x = 160;
	ballTest.screen_location.y = 160;
	assert(gameObjectTest.checkBallCollision(ballTest) == true);

	//Collision par le dessous
	ballTest.screen_location.x = 160;
	ballTest.screen_location.y = 180;
	assert(gameObjectTest.checkBallCollision(ballTest) == true);

	//Collision par la gauche
	ballTest.screen_location.x = 160;
	ballTest.screen_location.y = 170;
	assert(gameObjectTest.checkBallCollision(ballTest) == true);

	//Collision par la droite
	ballTest.screen_location.x = 180;
	ballTest.screen_location.y = 170;
	assert(gameObjectTest.checkBallCollision(ballTest) == true);
	
	printf("Test_MGameObject::test_checkBallCollision : OK\n\n");
}

void Test_MGameObject::test_bonusGameObject()
{
	printf("Test_MGameObject::test_bonusGameObject : Start testing\n");
	Entity testEntity;
	testEntity.screen_location.w = 100;
	testEntity.x_speed = 10;
	MGameObject testGameObject(testEntity, "bleu", false);
	
	testGameObject.bonusGameObject(1);
	assert(testGameObject.getQuadParam().screen_location.w == 125);

	testGameObject.bonusGameObject(1);
	assert(testGameObject.getQuadParam().screen_location.w == 156);

	testGameObject.bonusGameObject(1);
	assert(testGameObject.getQuadParam().screen_location.w == 195);

	testGameObject.bonusGameObject(1);
	assert(testGameObject.getQuadParam().screen_location.w == 195);

	testGameObject.bonusGameObject(2);
	assert(testGameObject.getQuadParam().x_speed == 12);

	testGameObject.bonusGameObject(2);
	assert(testGameObject.getQuadParam().x_speed == 15);

	testGameObject.bonusGameObject(2);
	assert(testGameObject.getQuadParam().x_speed == 18);

	testGameObject.bonusGameObject(2);
	assert(testGameObject.getQuadParam().x_speed == 18);

	printf("Test_MGameObject::test_bonusGameObject : OK\n\n");

}

void Test_MGameObject::test_malusGameObject()
{
	printf("Test_MGameObject::test_malusGameObject : Start testing \n");
	Entity testEntity;
	testEntity.screen_location.w = 100;
	testEntity.x_speed = 10;
	MGameObject testGameObject(testEntity, "bleu", false);
	
	testGameObject.malusGameObject(1);
	assert(testGameObject.getQuadParam().screen_location.w == 75);

	testGameObject.malusGameObject(1);
	assert(testGameObject.getQuadParam().screen_location.w == 56);

	testGameObject.malusGameObject(1);
	assert(testGameObject.getQuadParam().screen_location.w == 56);

	testGameObject.malusGameObject(2);
	assert(testGameObject.getQuadParam().x_speed == 7);

	testGameObject.malusGameObject(2);
	assert(testGameObject.getQuadParam().x_speed == 5);

	testGameObject.malusGameObject(2);
	assert(testGameObject.getQuadParam().x_speed == 5);

	printf("Test_MGameObject::test_malusGameObject : OK\n\n");

}


// ~~~~~ Tests de la classe MPlayer ~~~~~
void Test_MPlayer::test_playerEventHandler()
{
	/*
	La fonction teste les mouvements du pad lors de l'appuie d'une touche. Les mouvements sont testés sans et avec les collisions avec l'écran
	*/
	printf("Test_MPlayer::test_playerEventHandler : Start testing\n");
	Entity playerEntity, playerResults;
	playerEntity.screen_location.x = 50;	
	playerEntity.screen_location.y = 20;
	playerEntity.screen_location.h = 10;
	playerEntity.screen_location.w = 10;
	playerEntity.x_speed = 10;
	playerEntity.y_speed = 10;

	MPlayer playerTest(playerEntity, "bleu", SDLK_LEFT, SDLK_RIGHT);

	//I'm not pressing
	SDL_Event e;
	e.type = SDL_KEYUP;
	e.key.keysym.sym = SDLK_LEFT;
	playerTest.playerEventHandler(e);

	playerResults = playerTest.getQuadParam();
	assert(playerResults.screen_location.x == 50);

	//I'm pressing on the left
	e.type = SDL_KEYDOWN;
	playerTest.playerEventHandler(e);

	playerResults = playerTest.getQuadParam();
	assert(playerResults.screen_location.x == 40);	

	playerTest.playerEventHandler(e);

	playerResults = playerTest.getQuadParam();
	assert(playerResults.screen_location.x == 30);	

	//I stop pressing on the left
	e.type = SDL_KEYUP;
	playerTest.playerEventHandler(e);

	playerResults = playerTest.getQuadParam();
	assert(playerResults.screen_location.x == 30);

	//I'm pressing on a that is not the left or right arrow
	e.type = SDL_KEYDOWN;
	e.key.keysym.sym = SDLK_a;
	playerTest.playerEventHandler(e);

	playerResults = playerTest.getQuadParam();
	assert(playerResults.screen_location.x == 30);

	//I'm pressing on the right
	e.key.keysym.sym = SDLK_RIGHT;
	playerTest.playerEventHandler(e);

	playerResults = playerTest.getQuadParam();
	assert(playerResults.screen_location.x == 40);

	playerTest.playerEventHandler(e);

	playerResults = playerTest.getQuadParam();
	assert(playerResults.screen_location.x == 50);

	//I'm pressing but there is a collision on the right
	playerEntity.screen_location.x = 785;
	playerTest.setQuadParam(playerEntity);
	playerTest.playerEventHandler(e);
	playerResults = playerTest.getQuadParam();
	std::cout<<(playerResults.screen_location.x)<<std::endl;
	assert(playerResults.screen_location.x == 785);

	//I'm pressing o the left but I never released the right arrow button
	e.key.keysym.sym = SDLK_LEFT;
	playerEntity.screen_location.x = 5;
	playerTest.setQuadParam(playerEntity);
	playerTest.playerEventHandler(e);
	playerResults = playerTest.getQuadParam();
	assert(playerResults.screen_location.x == 15);

	printf("Test_MPlayer::test_playerEventHandler : OK\n\n");
}


// ~~~~~ Tests de la classe MComputer ~~~~~

void Test_MComputer::test_computerHandler()
{

	printf("Test_MComputer::test_computerHandler : Start testing\n");
	Entity ballTest;
	ballTest.screen_location.x = 100;
	ballTest.screen_location.y = 100;
	ballTest.screen_location.w = 10;
	ballTest.screen_location.h = 10;
	ballTest.x_speed = 5;
	ballTest.y_speed = 5;

	Entity computerEntity;
	computerEntity.screen_location.x = 250;
	computerEntity.screen_location.y = 10;
	computerEntity.screen_location.w = 50;
	computerEntity.screen_location.h = 10;
	computerEntity.x_speed = 10;
	computerEntity.y_speed = 0;

	MComputer computerTest(computerEntity, "rouge", 0, 1);

	//If the ball's location is on the left of the computer, so the computer goes to the left

	computerTest.computerHandler(ballTest, 100);
	assert(computerTest.getDecision() <= 3);
	assert(computerTest.getDecision() > 0);
	assert(computerTest.getQuadParam().screen_location.x < 250);

	//If the ball's location is on the right of the computer, so the computer goes to the right
	ballTest.screen_location.x = 500;

	computerTest.computerHandler(ballTest, 100);
	assert(computerTest.getDecision() <= 3);
	assert(computerTest.getDecision() > 0);
	assert(computerTest.getQuadParam().screen_location.x == 250);

	printf("Test_MComputer::test_computerHandler : OK\n\n");
}



//~~~~~ Tests de la classe MBall ~~~~~

void Test_MBall::test_moveBall()
{
	printf("Test_MBall::test_moveBall : Start testing\n");
	Entity ballEntity;
	ballEntity.screen_location.x = 100;
	ballEntity.screen_location.y = 100;
	ballEntity.screen_location.w = 50;
	ballEntity.screen_location.h = 50;
	ballEntity.x_speed = 10;
	ballEntity.y_speed = 10;

	MBall ballTest(ballEntity, "blanc", DOWN);

	//First Movement
	ballTest.moveBall();
	assert(ballTest.getQuadParam().screen_location.x == 110);
	assert(ballTest.getQuadParam().screen_location.y == 110);
	assert(ballTest.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Second mouvement consécutif
	ballTest.moveBall();
	assert(ballTest.getQuadParam().screen_location.x == 120);
	assert(ballTest.getQuadParam().screen_location.y == 120);
	assert(ballTest.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Collision with the right side of the screen
	ballEntity.screen_location.x = WINDOW_WIDTH - 50;
	ballTest.setQuadParam(ballEntity);
	ballTest.moveBall();
	assert(ballTest.getQuadParam().x_speed == -10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Collision with the left side of the screen
	ballEntity.screen_location.x = 0;
	ballEntity.x_speed = -10;
	ballTest.setQuadParam(ballEntity);
	ballTest.moveBall();
	assert(ballTest.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	printf("Test_MBall::test_moveBall : OK\n\n");
}

void Test_MBall::test_pongBall()
{
	printf("Test_MBall::test_pongBall : Start testing\n");
	Entity ballEntity, padEntity;
	ballEntity.screen_location.x = 100;
	ballEntity.screen_location.y = PLAYER_Y - 25;
	ballEntity.screen_location.h = 50;
	ballEntity.screen_location.w = 50;
	ballEntity.x_speed = 10;
	ballEntity.y_speed = 10;

	padEntity.screen_location.x = 100;
	padEntity.screen_location.y = PLAYER_Y;
	padEntity.screen_location.h = 50;
	padEntity.screen_location.w = 100;
	padEntity.x_speed = 10;
	padEntity.y_speed = 0;

	//Collision par le haut du paddle
	MBall ballTest(ballEntity, "blanc", UP);
	MPlayer playerTest(padEntity, "bleu", SDLK_LEFT, SDLK_RIGHT);
	MComputer computerTest(padEntity, "rouge", 10 , 1);

	ballTest.pongBall(playerTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getTeam() == DOWN);

	//Collision par le bas du paddle
	padEntity.screen_location.y = COMPUTER_Y;
	computerTest.setQuadParam(padEntity);

	ballTest.pongBall(computerTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getTeam() == UP);

	//PADDLE DU BAS
	//Collision par la gauche du paddle
	ballEntity.screen_location.y = PLAYER_Y + 20; 
	ballEntity.screen_location.x = 70;
	ballTest.setQuadParam(ballEntity);

	//La balle se dirige vers la droite
	ballTest.pongBall(playerTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getQuadParam().x_speed < 0);
	assert(ballTest.getTeam() == UP);

	//La balle se dirige vers la gauche
	ballTest.pongBall(playerTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getQuadParam().x_speed < 0);
	assert(ballTest.getTeam() == UP);

	//La balle a sa vitesse en x = 0
	ballEntity.x_speed = 0;
	ballTest.setQuadParam(ballEntity);
	assert(ballTest.getQuadParam().y_speed > 0);

	ballTest.pongBall(playerTest.getQuadParam());
	assert(ballTest.getQuadParam().x_speed < 0);
	assert(ballTest.getTeam() == UP);

	//Collision par la droite du paddle
	ballEntity.screen_location.y = PLAYER_Y + 20; 
	ballEntity.screen_location.x = 180;
	ballEntity.x_speed = -10;
	ballTest.setQuadParam(ballEntity);

	//La balle se dirige vers la droite
	ballTest.pongBall(playerTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getQuadParam().x_speed > 0);
	assert(ballTest.getTeam() == UP);

	//La balle se dirige vers la gauche
	ballTest.pongBall(playerTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getQuadParam().x_speed > 0);
	assert(ballTest.getTeam() == UP);

	//La balle a sa vitesse en x = 0
	ballEntity.x_speed = 0;
	ballTest.setQuadParam(ballEntity);
	assert(ballTest.getQuadParam().y_speed > 0);

	ballTest.pongBall(playerTest.getQuadParam());
	assert(ballTest.getQuadParam().x_speed > 0);
	assert(ballTest.getTeam() == UP);

	//PADDLE DU HAUT
	//Collision par la gauche du paddle
	ballEntity.screen_location.y = COMPUTER_Y - 20; 
	ballEntity.screen_location.x = 70;
	ballEntity.y_speed = -10;
	ballEntity.x_speed = 10;
	ballTest.setQuadParam(ballEntity);
	ballTest.setTeam(DOWN);

	//La balle se dirige vers la droite
	ballTest.pongBall(computerTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed < 0);
	assert(ballTest.getTeam() == DOWN);

	//La balle se dirige vers la gauche
	ballTest.pongBall(computerTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed < 0);
	assert(ballTest.getTeam() == DOWN);

	//La balle a sa vitesse en x = 0
	ballEntity.x_speed = 0;
	ballTest.setQuadParam(ballEntity);

	ballTest.pongBall(computerTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed < 0);
	assert(ballTest.getTeam() == DOWN);

	//Collision par la droite du paddle
	ballEntity.screen_location.y = COMPUTER_Y + 20; 
	ballEntity.screen_location.x = 180;
	ballEntity.x_speed = -10;
	ballTest.setQuadParam(ballEntity);

	//La balle se dirige vers la droite
	ballTest.pongBall(computerTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed > 0);
	assert(ballTest.getTeam() == DOWN);

	//La balle se dirige vers la gauche
	ballTest.pongBall(computerTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed > 0);
	assert(ballTest.getTeam() == DOWN);

	//La balle a sa vitesse en x = 0
	ballEntity.x_speed = 0;
	ballTest.setQuadParam(ballEntity);

	ballTest.pongBall(computerTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed > 0);
	assert(ballTest.getTeam() == DOWN);

	printf("Test_MBall::test_pongBall : OK\n\n");

}

void Test_MBall::test_pongBrickBall()
{
	printf("Test_MBall::test_pongBrickBall : Début du test\n");
	Entity ballEntity, brickEntity;
	brickEntity.screen_location.x = 200;
	brickEntity.screen_location.y = 200;
	brickEntity.screen_location.w = 200;
	brickEntity.screen_location.h = 200;

	ballEntity.screen_location.x = 250;
	ballEntity.screen_location.y = 160;
	ballEntity.screen_location.h = 50;
	ballEntity.screen_location.w = 50;
	ballEntity.x_speed = 10;
	ballEntity.y_speed = 10;

	MBall ballTest(ballEntity, "blanc", UP);
	MGameObject brickTest(brickEntity, "bleu", true);

	//Collision par le haut de la brique

	//vitesse x > 0
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed > 0);

	//vitesse x < 0
	ballEntity.x_speed = -10;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed < 0);

	//La vitesse x = 0
	ballEntity.x_speed = 0;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed == 0);


	//Collision par le bas de la brique

	//vitesse x < 0
	ballEntity.screen_location.y = 400;
	ballEntity.y_speed = -10;
	ballEntity.x_speed = -10;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getQuadParam().x_speed < 0);

	//vitesse x > 0
	ballEntity.x_speed = 10;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getQuadParam().x_speed > 0);

	//La vitesse x = 0
	ballEntity.x_speed = 0;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getQuadParam().x_speed == 0);
	

	//Collision par la gauche de la brique

	//vitesse y > 0
	ballEntity.screen_location.y = 250;
	ballEntity.screen_location.x = 160;
	ballEntity.y_speed = 10;
	ballEntity.x_speed = 10;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getQuadParam().x_speed < 0);

	//vitesse y < 0
	ballEntity.screen_location.y = 250;
	ballEntity.screen_location.x = 160;
	ballEntity.y_speed = -10;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed < 0);

	//Collision par la droite de la brique

	//La vitesse y > 0
	ballEntity.screen_location.y = 250;
	ballEntity.screen_location.x = 400;
	ballEntity.y_speed = 10;
	ballEntity.x_speed = -10;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getQuadParam().x_speed > 0);

	//La vitesse y < 0
	ballEntity.y_speed = -10;
	ballEntity.x_speed = -10;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed > 0);

	//Collision dans un coin d'une brique

	//Coin supérieur gauche
	ballEntity.screen_location.x = 155;
	ballEntity.screen_location.y = 155;
	ballEntity.x_speed = 10;
	ballEntity.y_speed = 10;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed < 0);

	//Coin supérieur droit
	ballEntity.screen_location.x = 195;
	ballEntity.x_speed = -10;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getQuadParam().x_speed > 0);

	//Coin inférieur gauche
	ballEntity.screen_location.x = 155;
	ballEntity.screen_location.y = 195;
	ballEntity.y_speed = -10;
	ballEntity.x_speed = 10;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getQuadParam().x_speed < 0);

	//Coin inférieur droit
	ballEntity.screen_location.x = 195;
	ballEntity.x_speed = -10;
	ballTest.setQuadParam(ballEntity);
	ballTest.pongBrickBall(brickTest.getQuadParam());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getQuadParam().x_speed > 0);

	printf("Test_MBall::test_pongBrickBall : OK\n\n");
}

void Test_MBall::test_handleBall()
{

	//0 : Joueur 1
	//1: Joueur 2
	//2: Computer 1
	//3: Computer 2

	printf("Test_MBall::test_handleBall : Début du test\n");
	Entity gameEntity;
	std::vector<MGameObject*>gameObjectList;

	//Paramétrage du joueur 1
	gameEntity.screen_location.x= WINDOW_WIDTH/2;
	gameEntity.screen_location.y = 50;
	gameEntity.screen_location.w = 100;
	gameEntity.screen_location.h = 50;
	gameEntity.x_speed = 10;
	gameEntity.y_speed = 0;
	MPlayer player1(gameEntity, "bleu", SDLK_LEFT, SDLK_RIGHT);
	gameObjectList.push_back(&player1);
	//Paramétrage du joueur 2
	gameEntity.screen_location.x= WINDOW_WIDTH/2;
	gameEntity.screen_location.y = WINDOW_HEIGHT - 50;
	gameEntity.screen_location.w = 100;
	gameEntity.screen_location.h = 50;
	gameEntity.x_speed = 10;
	gameEntity.y_speed = 0;
	MPlayer player2(gameEntity, "rouge", SDLK_LEFT, SDLK_RIGHT);
	gameObjectList.push_back(&player2);
	//Paramétrage du computer 1
	gameEntity.screen_location.x= WINDOW_WIDTH/2 + 150;
	gameEntity.screen_location.y = 50;
	gameEntity.screen_location.w = 100;
	gameEntity.screen_location.h = 50;
	gameEntity.x_speed = 10;
	gameEntity.y_speed = 0;
	MComputer computer1(gameEntity, "bleu", 1, 1);
	gameObjectList.push_back(&computer1);
	//Paramétrage du computer 2
	gameEntity.screen_location.x= WINDOW_WIDTH/2 + 150;
	gameEntity.screen_location.y = WINDOW_HEIGHT - 50;
	gameEntity.screen_location.w = 100;
	gameEntity.screen_location.h = 50;
	gameEntity.x_speed = 10;
	gameEntity.y_speed = 0;
	MComputer computer2(gameEntity, "rouge", 1, 1);
	gameObjectList.push_back(&computer2);
	//Paramétrage brique
	gameEntity.screen_location.x = WINDOW_WIDTH / 4;
	gameEntity.screen_location.y = WINDOW_HEIGHT / 4;
	gameEntity.screen_location.h = 100;
	gameEntity.screen_location.w = 100;
	gameEntity.x_speed = 0;
	gameEntity.y_speed = 0;
	MGameObject brickTest(gameEntity, "bleu", true);
	gameObjectList.push_back(&brickTest);

	//Paramétrage de la balle
	gameEntity.screen_location.x= WINDOW_WIDTH/2;
	gameEntity.screen_location.y = WINDOW_HEIGHT/2;
	gameEntity.screen_location.w = 50;
	gameEntity.screen_location.h = 50;
	gameEntity.x_speed = 10;
	gameEntity.y_speed = 10;
	MBall ballTest(gameEntity, "blanc", UP);
	
	//No Collision
	std::vector<MGameObject*>testList;
	testList = ballTest.handleBall(gameObjectList);
	assert(testList.size() == gameObjectList.size());
	assert(ballTest.getQuadParam().y_speed == 10);
	assert(ballTest.getQuadParam().x_speed == 10);
	assert(ballTest.getTeam() == UP);
	assert(ballTest.getLastTeam() == NOMODE);

	testList = ballTest.handleBall(gameObjectList);

	//Ball-Player1 Collision
	//	Ball was sent by opponents
	gameEntity.screen_location.y = WINDOW_HEIGHT - 60;
	ballTest.setQuadParam(gameEntity);
	testList = ballTest.handleBall(gameObjectList);
	
	assert(testList.size() == gameObjectList.size());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getTeam() == DOWN);
	assert(ballTest.getLastTeam() == DOWNTEAM);

	
	//	Ball is sent by my team
	gameEntity.screen_location.y = WINDOW_HEIGHT - 60;
	gameEntity.y_speed = -10;
	gameEntity.x_speed = 10;
	ballTest.setQuadParam(gameEntity);
	testList = ballTest.handleBall(gameObjectList);

	assert(testList.size() == gameObjectList.size());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getTeam() == DOWN);
	assert(ballTest.getLastTeam() == DOWNTEAM);


	//Ball - Player2 collision
	//	Ball was sent by opponents
	gameEntity.screen_location.y =  60;
	gameEntity.y_speed = -10;
	ballTest.setQuadParam(gameEntity);
	ballTest.setTeam(DOWN);
	testList = ballTest.handleBall(gameObjectList);
	
	assert(testList.size() == gameObjectList.size());
	assert((ballTest.getQuadParam().y_speed > 0));
	assert(ballTest.getTeam() == UP);
	assert(ballTest.getLastTeam() == UPTEAM);
	
	//	Ball was sent by same team
	gameEntity.screen_location.y =  60;
	gameEntity.y_speed = 10;
	ballTest.setQuadParam(gameEntity);
	ballTest.setTeam(UP);
	testList = ballTest.handleBall(gameObjectList);
	
	assert(testList.size() == gameObjectList.size());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getTeam() == UP);
	assert((ballTest.getLastTeam() == UPTEAM));

	//Collision ball-computer1
	//	Ball was sent by opponents
	gameEntity.screen_location.x = WINDOW_WIDTH/2 + 150;
	gameEntity.screen_location.y = 60;
	gameEntity.x_speed = 10;
	gameEntity.y_speed = -10;
	ballTest.setQuadParam(gameEntity);
	ballTest.setTeam(DOWN);

	testList = ballTest.handleBall(gameObjectList);

	assert(testList.size() == gameObjectList.size());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getTeam() == UP);
	assert(ballTest.getLastTeam() == UPTEAM);

	//	Ball was sent by my team
	gameEntity.screen_location.x = WINDOW_WIDTH/2 + 150;
	gameEntity.screen_location.y = 60;
	gameEntity.x_speed = 10;
	gameEntity.y_speed = 10;
	ballTest.setQuadParam(gameEntity);
	ballTest.setTeam(UP);

	assert(testList.size() == gameObjectList.size());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getTeam() == UP);
	assert(ballTest.getLastTeam() == UPTEAM);
	

	//Collision Ball-Computer2
	//	Ball was sent by opponents
	gameEntity.screen_location.x= WINDOW_WIDTH/2 + 150;
	gameEntity.screen_location.y = WINDOW_HEIGHT - 60;
	gameEntity.x_speed = 10;
	gameEntity.y_speed = 10;
	ballTest.setQuadParam(gameEntity);
	ballTest.setTeam(UP);
	testList = ballTest.handleBall(gameObjectList);

	assert(testList.size() == gameObjectList.size());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getTeam() == DOWN);
	assert(ballTest.getLastTeam() == DOWNTEAM);

	//	Ball was sent by same team
	gameEntity.screen_location.x= WINDOW_WIDTH/2 + 150;
	gameEntity.screen_location.y = WINDOW_HEIGHT - 60;
	gameEntity.x_speed = 10;
	gameEntity.y_speed = -10;
	ballTest.setQuadParam(gameEntity);
	ballTest.setTeam(DOWN);
	testList = ballTest.handleBall(gameObjectList);

	assert(testList.size() == gameObjectList.size());
	assert(ballTest.getQuadParam().y_speed < 0);
	assert(ballTest.getTeam() == DOWN);
	assert(ballTest.getLastTeam() == DOWNTEAM);

	//Collision Ball - Brick
	gameEntity.screen_location.x= WINDOW_WIDTH/4 + 50;
	gameEntity.screen_location.y = WINDOW_HEIGHT/4 + 110;
	gameEntity.x_speed = 10;
	gameEntity.y_speed = -10;
	ballTest.setQuadParam(gameEntity);
	ballTest.setTeam(DOWN);
	testList = ballTest.handleBall(gameObjectList);

	assert(testList.size() != gameObjectList.size());
	assert(ballTest.getQuadParam().y_speed > 0);
	assert(ballTest.getTeam() == UP);
	assert(ballTest.getLastTeam() == DOWNTEAM);

	printf("Test_MBall::test_handleBall : OK\n\n");

}


void Test_MBall::test_handleBrickBonus()
{
	printf("Test_MBall::test_handleBrickBonus : Début du test\n");
	Entity gameEntity;
	gameEntity.screen_location.x = WINDOW_WIDTH / 2;
	gameEntity.screen_location.y = WINDOW_HEIGHT + 50;
	gameEntity.screen_location.h = 50;
	gameEntity.screen_location.w = 100;
	gameEntity.x_speed = 10;
	gameEntity.y_speed = 0;
	MPlayer player1(gameEntity, "bleu", SDLK_LEFT, SDLK_RIGHT);
	MComputer computer1(gameEntity, "bleu", 10, 1);

	gameEntity.screen_location.y = WINDOW_HEIGHT - 150;
	MPlayer player2(gameEntity, "rouge", SDLK_LEFT, SDLK_RIGHT);
	MComputer computer2(gameEntity, "rouge", 10, 1);

	gameEntity.screen_location.x = WINDOW_WIDTH / 2;
	gameEntity.screen_location.y = WINDOW_HEIGHT / 2;
	gameEntity.screen_location.h = 50;
	gameEntity.screen_location.w = 50;
	gameEntity.x_speed = 10;
	gameEntity.y_speed = 10;
	MBall ballTest(gameEntity, "blanc", DOWN);

	//Increase ball's speed
	ballTest.handleBrickBonus("blanc", &player1, &player2, 1);

	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed > 10);

	//Bonus : Increase Player1's size
	gameEntity.x_speed = 10;
	ballTest.setQuadParam(gameEntity);
	ballTest.setTeam(DOWN);
	ballTest.setLastTeam(DOWNTEAM);
	ballTest.handleBrickBonus("bleu", &player1, &player2, 1);
	std::cout<<(player1.getQuadParam().screen_location.w)<<std::endl;
	assert(player1.getQuadParam().screen_location.w > 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Bonus : Increase Player1's speed
	gameEntity = player1.getQuadParam();
	gameEntity.screen_location.w = 100;
	player1.setQuadParam(gameEntity);

	ballTest.handleBrickBonus("bleu", &player1, &player2, 2);

	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed > 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Bonus : No bonus effects
	gameEntity = player1.getQuadParam();
	gameEntity.x_speed = 10;
	player1.setQuadParam(gameEntity);
	ballTest.handleBrickBonus("rouge", &player1, &player2, 3);

	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Malus : Decrease player1's size
	gameEntity.x_speed = 10;
	player1.setQuadParam(gameEntity);
	ballTest.setTeam(DOWN);
	ballTest.setLastTeam(DOWNTEAM);
	ballTest.handleBrickBonus("rouge", &player1, &player2, 1);
	
	assert(player1.getQuadParam().screen_location.w < 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Malus : Decrease player1's speed
	gameEntity = player1.getQuadParam();
	gameEntity.screen_location.w = 100;
	player1.setQuadParam(gameEntity);

	ballTest.handleBrickBonus("rouge", &player1, &player2, 2);

	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed < 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Malus : No maluses
	gameEntity = player1.getQuadParam();
	gameEntity.x_speed = 10;
	player1.setQuadParam(gameEntity);
	ballTest.handleBrickBonus("rouge", &player1, &player2, 3);

	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Bonus : Increase Player2's size
	ballTest.setTeam(UP);
	ballTest.setLastTeam(UPTEAM);
	ballTest.handleBrickBonus("rouge", &player1, &player2, 1);
	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w > 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Bonus : increase Player2's speed
	gameEntity = player1.getQuadParam();
	gameEntity.screen_location.w = 100;
	player2.setQuadParam(gameEntity);

	ballTest.handleBrickBonus("rouge", &player1, &player2, 2);

	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed > 10);
	assert(ballTest.getQuadParam().y_speed == 10);


	//Bonus : No bonuses
	gameEntity = player2.getQuadParam();
	gameEntity.x_speed = 10;
	player2.setQuadParam(gameEntity);

	ballTest.handleBrickBonus("rouge", &player1, &player2, 3);

	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Malus : Decrease Player2's size
	gameEntity.x_speed = 10;
	player2.setQuadParam(gameEntity);
	ballTest.setTeam(UP);
	ballTest.handleBrickBonus("bleu", &player1, &player2, 1);
	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w < 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);


	//Malus : Decrease Player2's speed
	gameEntity = player2.getQuadParam();
	gameEntity.screen_location.w = 100;
	player2.setQuadParam(gameEntity);

	ballTest.handleBrickBonus("bleu", &player1, &player2, 2);
	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed < 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Malus : No maluses
	gameEntity = player2.getQuadParam();
	gameEntity.x_speed = 10;
	player2.setQuadParam(gameEntity);
	ballTest.handleBrickBonus("bleu", &player1, &player2, 3);

	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Bonus : When lastTeam is set to NOMODE and we try to increase speed to player2

	ballTest.setLastTeam(NOMODE);
	ballTest.handleBrickBonus("rouge", &player1, &player2, 1);

	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Bonus : When lastTeam is set to NOMODE and we try to increase size to player2

	ballTest.setLastTeam(NOMODE);
	ballTest.handleBrickBonus("rouge", &player1, &player2, 2);

	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Malus : When lastTeam is set to NOMODE and we try to decrease speed to player2

	ballTest.setLastTeam(NOMODE);
	ballTest.handleBrickBonus("bleu", &player1, &player2, 1);

	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	//Bonus : When lastTeam is set to NOMODE and we try to decrease size to player2

	ballTest.setLastTeam(NOMODE);
	ballTest.handleBrickBonus("bleu", &player1, &player2, 2);

	assert(player1.getQuadParam().screen_location.w == 100);
	assert(player1.getQuadParam().x_speed == 10);
	assert(player2.getQuadParam().screen_location.w == 100);
	assert(player2.getQuadParam().x_speed == 10);
	assert(ballTest.getQuadParam().y_speed == 10);

	printf("Test_MBall::test_handleBrickBonus : OK\n\n");

}
