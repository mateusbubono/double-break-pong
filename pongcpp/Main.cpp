/*!
*	\file Main.cpp
*	\author Boris Merminod
*	\brief Execute main function
*	\version 1.0
*/

#include "MPong.h"

using namespace std;

extern std::stack<StateStruct> g_StateStack;

int main(int argc, char ** argv)
{
	if(argc == 2)
	{
		if(strcmp(argv[1], "-t") == 0)
		{
			test_pong();
			test_gameObject();
			test_player();
			test_computer();
			test_ball();
			test_menu();
		}

		return EXIT_SUCCESS;
	}

	MPong pongGame;
	while(!MPong::m_stateStack.empty())
		MPong::m_stateStack.top().StatePointer();

	return EXIT_SUCCESS;
}

