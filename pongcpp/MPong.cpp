/*!
*	\file MPong.cpp
*	\brief MPong class .cpp file
*	\author Boris Merminod
*	\version 1.0
*/

#include "MPong.h"

MMenu * MPong::m_menu;
SDL_Surface * MPong::m_screenSurface;
SDL_Window * MPong::m_window;
SDL_PixelFormat* MPong::m_mappingFormat;
SDL_Event MPong::m_event;
int MPong::m_timer;
bool MPong::m_gameError;
std::stack<StateStruct> MPong::m_stateStack;
MBall * MPong::m_ball;
MPlayer * MPong::m_player[NB_PLAYERS];
MComputer * MPong::m_computer[NB_COMPUTERS];
std::vector<MGameObject*>MPong::m_gameObjectList;
bool MPong::m_initGame;
bool MPong::m_brickMode;
bool MPong::m_endGame;
SDL_Surface * MPong::m_bitmap;

MPong::MPong()
{
	m_bitmap = NULL;
	m_screenSurface = NULL;
	m_window = NULL;
	m_ball = NULL;
	m_menu = NULL;
	m_initGame = false;
	m_brickMode = false;
	m_endGame = false;
	m_gameError = false;

	int i;
	for(i = 0; i<NB_PLAYERS; i++)
		m_player[i] = NULL;

	for(i = 0; i<NB_COMPUTERS; i++)
		m_computer[i] = NULL;

	if(init() == false)
	{
		fprintf(stderr, "MPong : Failed to instantiate the object\n");
		exit(EXIT_FAILURE);
	}
}

MPong::~MPong()
{
	shutdown();
}

bool MPong::init()
{
	//Init the SDL

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
	{
		fprintf(stderr, "MPong::init : Failed to initialized SDL\n");
		return false;
	}
	
	m_window = SDL_CreateWindow(WINDOW_CAPTION, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
	if(m_window == NULL)
	{
		fprintf(stderr, "MPong::init : Failed to initialized window\n");
		SDL_Quit();
		return false;
	}

	m_screenSurface = SDL_GetWindowSurface(m_window);
	if(m_screenSurface == NULL)
	{
		fprintf(stderr, "MPong::init : Failed to create the window's surface\n");
		SDL_DestroyWindow(m_window);
		SDL_Quit();
		return false;
	}

	//Init the timer
	m_timer = SDL_GetTicks();
	srand(time(0));

	//Set the sprites
	SDL_Surface* newSurface = SDL_LoadBMP(BITMAP_PATH);
	m_bitmap  = SDL_ConvertSurfaceFormat(newSurface, SDL_GetWindowPixelFormat(m_window), NULL);
	if(m_bitmap == NULL)
	{
		fprintf(stderr, "MPong::init : Failed to load the bitmap's picture to manage sprites\n");
		SDL_FreeSurface(m_screenSurface);
		SDL_DestroyWindow(m_window);
		SDL_Quit();
		return false;
	}

	Uint32 format = SDL_GetWindowPixelFormat(m_window);
	m_mappingFormat = SDL_AllocFormat(format);
	if(m_mappingFormat == NULL)
	{
		fprintf(stderr, "MPong::init : Failed to load pixels format to manage color modulation\n");
		SDL_FreeSurface(m_screenSurface);
		SDL_FreeSurface(m_bitmap);
		SDL_DestroyWindow(m_window);
		SDL_Quit();
		return false;
	}

	SDL_SetColorKey(m_bitmap, SDL_TRUE, SDL_MapRGB(m_bitmap->format, 255, 0, 255));


	//Set the menus
	std::vector<std::string>textList;
	textList.push_back("(1) Number of players : 1");
	textList.push_back("(2) Game Mode : Brick");
	textList.push_back("(3) Screen position : Down");
	textList.push_back("(4) Double mode position : Back");
	textList.push_back("(5) Double mode two players game mode : Versus");
	textList.push_back("(6) Ball Speed : Slow");
	textList.push_back("(7) Computer Level : Easy");
	textList.push_back("(Return) Start the Game");
	textList.push_back("(Escape) Quit the Game");

	std::vector<PickUp>gameModeList;
	gameModeList.push_back(ONEPLAYER);
	gameModeList.push_back(BRICKMODE);
	gameModeList.push_back(DOWNTEAM);
	gameModeList.push_back(BACK);
	gameModeList.push_back(VERSUS);
	gameModeList.push_back(BALLSPEEDSLOW);
	gameModeList.push_back(COMPUTEREASY);
	gameModeList.push_back(NOMODE);
	gameModeList.push_back(NOMODE);

	std::vector<SDL_Keycode>keyList;
	keyList.push_back(SDLK_1);
	keyList.push_back(SDLK_2);
	keyList.push_back(SDLK_3);
	keyList.push_back(SDLK_4);
	keyList.push_back(SDLK_5);
	keyList.push_back(SDLK_6);
	keyList.push_back(SDLK_7);
	keyList.push_back(SDLK_0);
	keyList.push_back(SDLK_0);

	int x = WINDOW_WIDTH / 4;
	int y = WINDOW_HEIGHT / 4;
	int size = 20;
	int shift = 20;

	SDL_Color foreground = {255, 255, 255};
	SDL_Color background = {11, 8, 39};

	m_menu = new MMenu(textList, gameModeList, keyList, x, y, size, shift, foreground, background);
	if(m_menu == NULL)
	{
		fprintf(stderr, "MPong::init : Failed to allocate memory for the menu\n");
		return false;
	}

	StateStruct state;
	state.StatePointer =  menu;
	m_stateStack.push(state);

	//Initialisation de la TTF
	if(TTF_Init() == -1)
	{
		fprintf(stderr, "MPong::init : Failed to initialize TTF\n");
		return false;
	}

	return true;

}

void MPong::shutdown()
{

	//Close SDL_TTF
	TTF_Quit();

	//Delete Surfaces
	if(m_bitmap != NULL)
	{
		SDL_FreeSurface(m_bitmap);
		m_bitmap = NULL;
	}

	if(m_mappingFormat != NULL)
	{
		SDL_FreeFormat(m_mappingFormat);
		m_mappingFormat = NULL;
	}

	if(m_screenSurface != NULL)
	{
		SDL_FreeSurface(m_screenSurface);
		m_screenSurface = NULL;
	}
	if(m_window != NULL)
	{
		SDL_DestroyWindow(m_window);
		m_window = NULL;
	}

	//Delete all the objects
	if(m_ball != NULL)
	{
		delete m_ball;
		m_ball = NULL;
	}
	int i;
	for(i = 0; i<NB_PLAYERS; i++)
		if(m_player[i] != NULL)
		{
			delete m_player[i];
			m_player[i] = NULL;
		}
	for(i = 0; i<NB_COMPUTERS; i++)
		if(m_computer[i] != NULL)
		{
			delete m_computer[i];
			m_computer[i] = NULL;
		}

	while(m_gameObjectList.empty() == false)
		m_gameObjectList.pop_back();

	//Delete the menu
	if(m_menu != NULL)
	{
		delete m_menu;
		m_menu = NULL;
	}

	//Close SDL
	SDL_Quit();
}

void  MPong::menu()
{
	if(m_menu == NULL || m_screenSurface == NULL || m_window == NULL)
	{
		fprintf(stderr, "MPong::menu : m_menu, m_screenSurface or m_window parameter(s) not correct\n");
		m_gameError = true;
		return;
	}
	if((SDL_GetTicks() - m_timer) < FRAME_RATE)
		return;
	
	eventManager();
	if(m_gameError == true)
	{
		fprintf(stderr, "MPong::menu : MPong::eventManager function failed\n");
		return;
	}
	
	if(clearScreen() == false)
	{
		fprintf(stderr, "MPong::menu : MPong::clearScreen function failed\n");
		m_gameError = true;
		return;
	}
	displayText("Double Break Pong", WINDOW_WIDTH / 4, 50, 40);
	m_menu->displayText(m_screenSurface);
	SDL_UpdateWindowSurface(m_window);
	m_timer = SDL_GetTicks();
}

void MPong::eventManager(bool wantGame)
{
	if(SDL_PollEvent(&m_event))
	{
		if(wantGame == true)
		{
			handleGameInput();
		}
		else 
		{
			if(handleMenuInput() == false)
			{
				fprintf(stderr, " MPong::eventManager : MPong::handleMenuInput function failed\n");
				m_gameError = true;
				return;
			}
		}
	}
	else if(wantGame == true)
	{
		for(int i = 0; i<NB_PLAYERS; i++)
			if(m_player[i] != NULL)
				m_player[i]->playerEventHandler(m_event);
	}
}

bool MPong::handleMenuInput()
{
	if(m_menu == NULL)
	{
		fprintf(stderr, "MPong::handleMenuInput : Error, m_menu parameter not correct \n");
		return false;
	}

	if(m_event.type == SDL_QUIT)
		while(!m_stateStack.empty())
			m_stateStack.pop();
	else if(m_event.type == SDL_KEYDOWN)
		m_stateStack = m_menu->handleMenuInput(m_event, m_stateStack, game);

	return true;
}

bool MPong::clearScreen()
{
	if(m_screenSurface == NULL)
	{
		fprintf(stderr, "MPong::clearScreen : Error, m_screenSurface parameter not correct\n");
		return false;
	}
	
	if(SDL_FillRect(m_screenSurface, 0, SDL_MapRGB(m_screenSurface->format, 11, 8, 39)) < 0)
	{
		fprintf(stderr, "MPong::clearScreen : SDL_FillRect function failed\n");
		return false;
	}

	return true;
}

void MPong::game()
{
	if(m_bitmap == NULL || m_screenSurface == NULL || m_window == NULL)
	{
		fprintf(stderr, "MPong::game : Error ! m_bitmap, m_screenSurface or m_window\n");
		m_gameError = true;
		return;
	}

	if(m_initGame == false)
	{
		m_initGame = initGame();
	}

	if(m_initGame == false)
	{
		fprintf(stderr, "MPong::game : Failed to init the game\n");
		m_gameError = true;
		return;
	}
	if(m_endGame == true)
	{
		eventManager(true);
		return;
	}

	if((SDL_GetTicks() - m_timer) < FRAME_RATE)
		return;
	
	eventManager(true);

	if(m_brickMode == true)
	{
		if(brickSpawner() == false)
		{
			fprintf(stderr, "MPong::game : Failed to manage brick's spawn \n");
			m_gameError = true;
			return;
		}
	}
	else
	{
		if(bonusLauncher() == false)
		{
			fprintf(stderr, "MPong::game : Failed to manage bonus launcher \n");
			m_gameError = true;
			return;
		}
	}
	
	if(m_ball == NULL)
	{
		fprintf(stderr, "MPong::game : Error ! m_ball parameter not correct\n");
		m_gameError = true;
		return;
	}
	
	//Events Management
	m_gameObjectList = m_ball->handleBall(m_gameObjectList);
	for(int i = 0; i<NB_COMPUTERS; i++)
		if(m_computer[i] != NULL)
			m_computer[i]->computerHandler(m_ball->getQuadParam(), rand() %100);
	

	//Draw Sprites
	if(clearScreen() == false)
	{
		fprintf(stderr, "MPong::game : clearScreen function failed\n");
		m_gameError = true;
		return;
	}

	if(bonusDisplay() == false)
	{
		fprintf(stderr, "MPong::game : bonusDisplay function failed\n");
		m_gameError = true;
		return;
	}

	if(m_endGame == false)
		m_endGame = manageScore();


	for(int i = 0; i<m_gameObjectList.size(); i++)
	{
		if(m_gameObjectList[i] == NULL)
			continue;
		m_gameObjectList[i]->spriteFeedback();

		Entity pad = m_gameObjectList[i]->getQuadParam();
		
		SDL_BlitScaled(m_bitmap, &pad.bitmap_location, m_screenSurface, &pad.screen_location);

		colorFeedback(m_gameObjectList[i]);
	}

	m_ball->spriteFeedback();
	Entity ball = m_ball->getQuadParam();
	SDL_BlitSurface(m_bitmap, &ball.bitmap_location, m_screenSurface, &ball.screen_location);

	//Update Screen
	SDL_UpdateWindowSurface(m_window);

	m_timer = SDL_GetTicks();

}

void MPong::colorFeedback(MGameObject * gameObject)
{
	if(gameObject->getQuadColor() == "blanc")
		return;
	
	Entity quadParam = gameObject->getQuadParam();
	int x = quadParam.screen_location.x,
	y = quadParam.screen_location.y,
	w = quadParam.screen_location.w,
	h = quadParam.screen_location.h,
	positionIndex = y *  m_screenSurface->pitch / 4 + x,
	widthSize = positionIndex + w,
	heightSize = y+h,
	colorMinTreshold = 10, 
	colorMaxTreshold = 250, 
	colorSpeedModulation = 5;

	Uint8 r=0, g=0, b=0, a=0;
	SDL_LockSurface(m_screenSurface);

	Uint32* pixels = (Uint32*) m_screenSurface->pixels;
	int pixelCount = m_screenSurface->pitch / 4 * m_screenSurface->h;
	
	while(y < heightSize)
	{
		for(int i = positionIndex; i<widthSize; i++)
		{
			SDL_GetRGBA(pixels[i], m_mappingFormat, &r, &g, &b, &a);
			if(g == 8)
				continue;

			Uint32 pixelColor = SDL_MapRGBA(m_mappingFormat, r, gameObject->getPixelGreenValue(),b, a);
			pixels[i] = pixelColor;
		}
		y++;
		positionIndex = y *  m_screenSurface->pitch / 4 + x;
		widthSize = positionIndex + w;
	}

	SDL_UnlockSurface(m_screenSurface);

	int pixelGreenValue = gameObject->getPixelGreenValue();
	if(gameObject->isColorDownModulation() == true)
	{
		gameObject->setPixelGreenValue(pixelGreenValue - colorSpeedModulation);
	}	
	else
	{
		gameObject->setPixelGreenValue(pixelGreenValue + colorSpeedModulation);
	}

	if(pixelGreenValue >= colorMaxTreshold)
	{
		gameObject->setIsColorDownModulation(true);
		gameObject->setPixelGreenValue(pixelGreenValue - colorSpeedModulation);
	}
	else if(pixelGreenValue <= colorMinTreshold)
	{
		gameObject->setIsColorDownModulation(false);
		gameObject->setPixelGreenValue(pixelGreenValue + colorSpeedModulation);
	}
}

bool MPong::initGame()
{
	if(m_menu == NULL)
	{
		fprintf(stderr, "Error m_menu parameter not correct\n");
		return false;
	}
	Entity pad;
	std::vector<PickUp> gameModeList = m_menu->getGameModeList();
	bool twoPlayersFlag = false, doubleModeFlag = false, teamFlag = false, downFlag = false, frontFlag = false;
	int computerLevel;

	Entity tabEntity[4];
	std::string tabColorString[4];

	if (initTransformAndColor(tabEntity, tabColorString, 4) == false)
	{
		fprintf(stderr, "MPong::initGame : Error ! initTransformAndColor function failed\n");
		return false;
	}

	if (initGameMode(gameModeList, &twoPlayersFlag,&doubleModeFlag, &teamFlag, &downFlag, &frontFlag, &computerLevel) == false)
	{
		fprintf(stderr, "MPong::initGame : Error ! initGameMode function failed\n");
		return false;
	}
	
	//Initialize Ball's GameObject
	Entity ballEntity;
	ballEntity.screen_location.x = (WINDOW_WIDTH / 2) - (BALL_DIAMETER / 2);
	ballEntity.screen_location.y = (WINDOW_HEIGHT / 2) - 100;
	ballEntity.screen_location.w = BALL_DIAMETER;
	ballEntity.screen_location.h = BALL_DIAMETER;

	ballEntity.bitmap_location.x = BALL_BITMAP_X;
	ballEntity.bitmap_location.y = BALL_BITMAP_Y;
	ballEntity.bitmap_location.w = BALL_DIAMETER;
	ballEntity.bitmap_location.h = BALL_DIAMETER;

	ballEntity.x_speed = 0;
	ballEntity.y_speed = MBall::cBALL_SPEED_Y;

	m_ball = new MBall(ballEntity, "blanc", UP);
	if(m_ball == NULL)
	{
		fprintf(stderr, "MPong::initGame : Failed to allocate memory for the ball\n");
		return false;
	}

	m_ball->setScore(0, 0);
	m_ball->setScore(1, 0);
	
	if(initAllPlayers(twoPlayersFlag, doubleModeFlag, teamFlag, downFlag, frontFlag, computerLevel, tabEntity, tabColorString) == false)
	{
		fprintf(stderr, "MPong::initGame : Error ! initAllPlayers function failed\n");
		return false;
	}

	if(m_gameObjectList.empty() == false)
	{
		fprintf(stderr, "MPong::initGame : Error ! m_gameObjectList parameter is not empty\n");
		return false;
	}
	
	for(int i = 0; i<NB_PLAYERS; i++)
		if(m_player[i] != NULL)
			m_gameObjectList.push_back(m_player[i]);
	
	for(int i = 0; i<NB_COMPUTERS; i++)
		if(m_computer[i] != NULL)
			m_gameObjectList.push_back(m_computer[i]);
	
	
	return true;			
}

bool MPong::initTransformAndColor(Entity tabEntity[], std::string tabColorString[], const int sizeTab)
{
	if(tabEntity == NULL || tabColorString == NULL)
	{
		fprintf(stderr, "MPong::initTransformAndColor :  tabEntity or tabColorString not correct\n");
		return false;
	}
	for(int j=0; j<sizeTab; j++)
	{
		tabEntity[j].screen_location.x =  WINDOW_WIDTH / 2 - PADDLE_WIDTH / 2;
		tabEntity[j].screen_location.w = PADDLE_WIDTH;
		tabEntity[j].screen_location.h = PADDLE_HEIGHT;

		tabEntity[j].bitmap_location.x = PADDLE_BITMAP_X;
		tabEntity[j].bitmap_location.w = PADDLE_WIDTH;
		tabEntity[j].bitmap_location.h = PADDLE_HEIGHT;

		tabEntity[j].x_speed = PLAYER_SPEED;
		tabEntity[j].y_speed = 0;

		switch(j)
		{
			case 0 :
				tabColorString[j] = "bleu";
				tabEntity[j].screen_location.y = PLAYER_Y;
				tabEntity[j].bitmap_location.y = PADDLE_BITMAP_BLUE_Y;
				break;
			case 1 :
				tabColorString[j] = "bleu";
				tabEntity[j].screen_location.y = PLAYER_Y - 150;
				tabEntity[j].bitmap_location.y = PADDLE_BITMAP_BLUE_Y;
				break;
			case 2 :
				tabColorString[j] = "rouge";
				tabEntity[j].screen_location.y = COMPUTER_Y;
				tabEntity[j].bitmap_location.y = PADDLE_BITMAP_RED_Y;
				break;
			case 3 :
				tabColorString[j] = "rouge";
				tabEntity[j].screen_location.y = COMPUTER_Y + 150;
				tabEntity[j].bitmap_location.y = PADDLE_BITMAP_RED_Y;
				break;
		}
	}

	return true;
}

bool MPong::initGameMode(std::vector<PickUp> gameModeList, bool * twoPlayersFlag, bool * doubleModeFlag, bool * teamFlag, bool * downFlag, bool * frontFlag, int * computerLevel)
{
	if(twoPlayersFlag == NULL || doubleModeFlag == NULL || teamFlag == NULL || downFlag == NULL || frontFlag == NULL || computerLevel == NULL)
	{
		fprintf(stderr, "MPong::initGameMode : Error parameter(s) not correct\n");
		return false;
	}
	for(int i = 0; i<gameModeList.size(); i++)
	{
		switch(gameModeList[i])
		{
			case ONEPLAYER :
				*twoPlayersFlag = false;
				break;
			case TWOPLAYERS :
				*twoPlayersFlag = true;
				break;
			case BRICKMODE :
				*doubleModeFlag = false;
				break;
			case DOUBLEMODE :
				*doubleModeFlag = true;
				break;
			case VERSUS :
				*teamFlag = false;
				break;
			case TEAM :
				*teamFlag = true;
				break;
			case DOWNTEAM :
				*downFlag = true;
				break;
			case UPTEAM :
				*downFlag = false;
				break;
			case BACK :
				*frontFlag = false;
				break;
			case FRONT :
				*frontFlag = true;
				break;
			case BALLSPEEDSLOW :
				MBall::cBALL_SPEED_Y = 3;
				break;
			case BALLSPEEDMEZZO :
				MBall::cBALL_SPEED_Y = 5;
				break;
			case BALLSPEEDFAST :
				MBall::cBALL_SPEED_Y = 7;
				break;
			case COMPUTEREASY :
				*computerLevel = 0;
				break;
			case COMPUTERMEDIUM :
				*computerLevel = 1;
				break;
			case COMPUTERHARD : 
				*computerLevel = 2;
				break;
			case COMPUTERHARDEST : 
				*computerLevel = 3;
				break;
		}
	}

	return true;
}

bool MPong::initAllPlayers(bool twoPlayersFlag, bool doubleModeFlag, bool teamFlag, bool downFlag, bool frontFlag, int computerLevel, Entity tabEntity[], std::string tabColorString[])
{
	if(twoPlayersFlag == true)
	{
		if(doubleModeFlag == true)
		{
			m_brickMode = false;
			if(teamFlag == true)
			{
				if(downFlag == true)
				{
					if(frontFlag == true)
					{
						m_player[0] = new MPlayer(tabEntity[1], tabColorString[1], SDLK_LEFT, SDLK_RIGHT);
						m_player[1] = new MPlayer(tabEntity[0], tabColorString[0], SDLK_q, SDLK_d);
					}
					else
					{
						m_player[0] = new MPlayer(tabEntity[0], tabColorString[0], SDLK_LEFT, SDLK_RIGHT);
						m_player[1] = new MPlayer(tabEntity[1], tabColorString[1], SDLK_q, SDLK_d);
					}
					m_computer[0] = new MComputer(tabEntity[2], tabColorString[2], computerLevel, 1);
					m_computer[1] = new MComputer(tabEntity[3], tabColorString[3], computerLevel, 1);
				}
				else //Players team up
				{
					if(frontFlag == true)
					{
						m_player[0] = new MPlayer(tabEntity[3], tabColorString[3], SDLK_LEFT, SDLK_RIGHT);
						m_player[1] = new MPlayer(tabEntity[2], tabColorString[2], SDLK_q, SDLK_d);
					}
					else
					{
						m_player[0] = new MPlayer(tabEntity[2], tabColorString[2], SDLK_LEFT, SDLK_RIGHT);
						m_player[1] = new MPlayer(tabEntity[3], tabColorString[3], SDLK_q, SDLK_d);
					}
					m_computer[0] = new MComputer(tabEntity[0], tabColorString[0], computerLevel, 1);
					m_computer[1] = new MComputer(tabEntity[1], tabColorString[1], computerLevel, 1);
				}
			}
			else //Versus double mode two players
			{
				if(downFlag == true)
				{
					if(frontFlag == true)
					{
						m_player[0] = new MPlayer(tabEntity[1], tabColorString[1], SDLK_LEFT, SDLK_RIGHT);
						m_player[1] = new MPlayer(tabEntity[3], tabColorString[3], SDLK_q, SDLK_d);
						m_computer[0] = new MComputer(tabEntity[0], tabColorString[0], computerLevel, 1);
						m_computer[1] = new MComputer(tabEntity[2], tabColorString[2], computerLevel, 1);
					}
					else
					{
						m_player[0] = new MPlayer(tabEntity[0], tabColorString[0], SDLK_LEFT, SDLK_RIGHT);
						m_player[1] = new MPlayer(tabEntity[2], tabColorString[2], SDLK_q, SDLK_d);
						m_computer[0] = new MComputer(tabEntity[1], tabColorString[1], computerLevel, 1);
						m_computer[1] = new MComputer(tabEntity[3], tabColorString[3], computerLevel, 1);
					}
				}
				else
				{
					if(frontFlag == true)
					{
						m_player[0] = new MPlayer(tabEntity[3], tabColorString[3], SDLK_LEFT, SDLK_RIGHT);
						m_player[1] = new MPlayer(tabEntity[1], tabColorString[1], SDLK_q, SDLK_d);
						m_computer[0] = new MComputer(tabEntity[0], tabColorString[0], computerLevel, 1);
						m_computer[1] = new MComputer(tabEntity[2], tabColorString[2], computerLevel, 1);
					}
					else
					{
						m_player[0] = new MPlayer(tabEntity[2], tabColorString[2], SDLK_LEFT, SDLK_RIGHT);
						m_player[1] = new MPlayer(tabEntity[0], tabColorString[0], SDLK_q, SDLK_d);
						m_computer[0] = new MComputer(tabEntity[1], tabColorString[1], computerLevel, 1);
						m_computer[1] = new MComputer(tabEntity[3], tabColorString[3], computerLevel, 1);
					}
				}
			}

			for(int i = 0; i<NB_PLAYERS; i++)
			{
				if(m_player[i] == NULL)
				{
					fprintf(stderr, "MPong::initAllPlayers : Failed to allocate memory for players\n");
					return false;
				}
				
				if(m_computer[i] == NULL)
				{
					fprintf(stderr, "MPong::initAllPlayers : Failed to allocate memory for computers\n");
					return false;
				}
			}

		}
		else //Brick Mode two players
		{
			m_brickMode = true;
			if(downFlag == true)
			{
				m_player[0] = new MPlayer(tabEntity[0], tabColorString[0], SDLK_LEFT, SDLK_RIGHT);
				m_player[1] = new MPlayer(tabEntity[2], tabColorString[2], SDLK_q, SDLK_d);
			}
			else //Up position
			{
				m_player[0] = new MPlayer(tabEntity[2], tabColorString[2], SDLK_LEFT, SDLK_RIGHT);
				m_player[1] = new MPlayer(tabEntity[0], tabColorString[0], SDLK_q, SDLK_d);
			}

			for(int i = 0; i<NB_PLAYERS; i++)
			{
				if(m_player[i] == NULL)
				{
					fprintf(stderr, "MPong::initAllPlayers : Failed to allocate memory for players\n");
					return false;
				}
			}
		}
	}
	else //One player mode
	{
		if(doubleModeFlag == true)
		{
			m_brickMode = false;
			if(downFlag == true)
			{
				if(frontFlag == true)
				{
					m_player[0] = new MPlayer(tabEntity[1], tabColorString[1], SDLK_LEFT, SDLK_RIGHT);
					m_computer[0] = new MComputer(tabEntity[0], tabColorString[0], computerLevel, 1);
				}
				else //Player position back
				{
					m_player[0] = new MPlayer(tabEntity[0], tabColorString[0], SDLK_LEFT, SDLK_RIGHT);
					m_computer[0] = new MComputer(tabEntity[1], tabColorString[1], computerLevel, 1);
				}
				m_computer[1] = new MComputer(tabEntity[2], tabColorString[2], computerLevel, 1);
				m_computer[2] = new MComputer(tabEntity[3], tabColorString[3], computerLevel, 1);
			}
			else //Player position up 
			{
				if(frontFlag == true)
				{
					m_player[0] = new MPlayer(tabEntity[3], tabColorString[3], SDLK_LEFT, SDLK_RIGHT);
					m_computer[0] = new MComputer(tabEntity[2], tabColorString[2], computerLevel, 1);
				}
				else // Player position back
				{
					m_player[0] = new MPlayer(tabEntity[2], tabColorString[2], SDLK_LEFT, SDLK_RIGHT);
					m_computer[0] = new MComputer(tabEntity[3], tabColorString[3], computerLevel, 1);
				}
				m_computer[1] = new MComputer(tabEntity[0], tabColorString[0], computerLevel, 1);
				m_computer[2] = new MComputer(tabEntity[1], tabColorString[1], computerLevel, 1);
			}


			if(m_player[0] == NULL)
			{
				fprintf(stderr, "MPong::initAllPlayers : Failed to allocate memory for player\n");
				return false;
			}

			for(int i = 0; i<NB_COMPUTERS; i++)
			{
				if(m_computer[i] == NULL)
				{
					fprintf(stderr, "MPong::initAllPlayers : Failed to allocate memory for computers\n");
					return false;
				}
			}
		}
		else //BrickMode One Player
		{
			m_brickMode = true;
			if(downFlag == true)
			{
				m_player[0] = new MPlayer(tabEntity[0], tabColorString[0], SDLK_LEFT, SDLK_RIGHT);
				m_computer[0] = new MComputer(tabEntity[2], tabColorString[2], computerLevel, 1);
			}
			else // Player position up
			{
				m_player[0] = new MPlayer(tabEntity[2], tabColorString[2], SDLK_LEFT, SDLK_RIGHT);
				m_computer[0] = new MComputer(tabEntity[0], tabColorString[0], computerLevel, 1);
			}

			if(m_player[0] == NULL)
			{
				fprintf(stderr, "MPong::initAllPlayers : Failed to allocate memory for player\n");
				return false;
			}
			if(m_computer[0] == NULL)
			{
				fprintf(stderr, "MPong::initAllPlayers : Failed to allocate memory for computer\n");
				return false;
			}
		}
	}
	return true;
}


void MPong::handleGameInput()
{
	switch(m_event.type)
	{
		case SDL_QUIT :
			while(!m_stateStack.empty())
				m_stateStack.pop();
		case SDL_KEYDOWN :
			for(int i = 0; i<NB_PLAYERS; i++)
				if(m_player[i] != NULL)
					m_player[i]->playerEventHandler(m_event);
			break;
		case SDL_KEYUP :
			if(m_endGame == true)
			{
				m_initGame = false;
				m_endGame = false;
				endGame();
				if(m_stateStack.empty() == false)
					m_stateStack.pop();
			}
			else if(m_event.key.keysym.sym == SDLK_ESCAPE)
			{
				m_endGame = true;
			}
			else
			{
				for(int i = 0; i<NB_PLAYERS; i++)
					if(m_player[i] != NULL)
						m_player[i]->playerEventHandler(m_event);
			}
			break;		
	}
}

void MPong::endGame()
{
	for(int i = 0; i<2; i++)
		if(m_player[i] != NULL)
		{
			delete m_player[i];
			m_player[i] = NULL;
		}
	
	for(int i = 0; i<3; i++)
		if(m_computer[i] != NULL)
		{
			delete m_computer[i];
			m_computer[i] = NULL;
		}
	
	if(m_ball != NULL)
	{
		delete m_ball;
		m_ball = NULL;
	}
	
	while(m_gameObjectList.empty() == false)
		m_gameObjectList.pop_back();
}


bool MPong::brickSpawner()
{
	std::string brickColor = "blanc";
	int randomValue = rand() % 100 + 1;
	if(randomValue == 1)
	{
		Entity brickEntity;
		brickEntity.screen_location.y = rand() % (WINDOW_HEIGHT - 300) + 100;
		brickEntity.screen_location.x = rand() % (WINDOW_WIDTH - 200) + 100;
		brickEntity.screen_location.h = 40;
		brickEntity.screen_location.w = 40;
		randomValue = rand() % 3;
		switch(randomValue)
		{
			case 0 :
				brickEntity.bitmap_location.x = BRICK_RED_X;
				brickColor = "rouge";
				break;
			case 1 :
				brickEntity.bitmap_location.x = BRICK_BLUE_X;
				brickColor = "bleu";
				break;
			case 2 :
				brickEntity.bitmap_location.x = BRICK_WHITE_X;
				brickColor = "blanc";
				break;
		}
		
		brickEntity.bitmap_location.y = BRICK_Y;
		brickEntity.bitmap_location.w = 40;
		brickEntity.bitmap_location.h = 40;
		brickEntity.x_speed = 0;
		brickEntity.y_speed = 0;

		for(int i = 0; i< m_gameObjectList.size(); i++)
		{
			if(m_gameObjectList[i]->checkBallCollision(brickEntity) == true)
				return true;
		}

		MGameObject * brick = NULL;
		brick = new MGameObject(brickEntity, brickColor, true);
		if(brick == NULL)
		{
			fprintf(stderr, "MPong:: brickSpawner : Memory's allocation for the brick failed\n");
			return false;
		}
		m_gameObjectList.push_back(brick);
	}

	return true;
}


bool  MPong::bonusLauncher()
{
	if(m_ball == NULL)
	{
		fprintf(stderr, " MPong::bonusLauncher : Error ! the ball isn't initialized\n");
		return false;
	}
	int randomValue = rand() % 50;
	if(randomValue != 1)
		return true;
	
	for(int i = 0; i<NB_PLAYERS; i++)
	{
		if(m_player[i] == NULL)
			continue;
		randomValue = rand() % 4;
		if(randomValue != 0)
			continue;
		
		randomValue = rand() % 2;
		if(randomValue == 0)
			m_player[i]->bonusGameObject(rand() % 2 + 1);
		else if(randomValue == 1)
			m_player[i]->malusGameObject(rand() % 2 + 1);

		return true;
	}

	for(int i = 0; i<NB_COMPUTERS; i++)
	{
		if(m_computer[i] == NULL)
			continue;
		randomValue = rand() % 4;
		if(randomValue != 0)
			continue;
		
		randomValue = rand() % 2;
		if(randomValue == 0)
			m_computer[i]->bonusGameObject(rand() % 2 + 1);
		else if(randomValue == 1)
			m_computer[i]->malusGameObject(rand() % 2 + 1);
		return true;
	}

	Entity ballEntity = m_ball->getQuadParam();
	if(ballEntity.y_speed > 0)
	{
		ballEntity.y_speed += 1;
		m_ball->setFeedbackFrames(30);
	}
	else
	{
		ballEntity.y_speed -= 1;
		m_ball->setFeedbackFrames(30);
	}
	m_ball->setQuadParam(ballEntity);

	return true;
}

bool MPong::bonusDisplay()
{
	if(m_ball == NULL)
	{
		fprintf(stderr, "MPong::bonusDisplay : Error m_ball paramater not initialized\n");
		return false;
	}

	int displayPosY = 0;
	for(int i = 0; i<NB_PLAYERS ;i++)
	{
		if(m_player[i] == NULL)
			continue;
		
		int padPosY = m_player[i]->getQuadParam().screen_location.y;
		if(padPosY == COMPUTER_Y)
			displayPosY = COMPUTER_Y + 20;
		else if(padPosY == PLAYER_Y)
			displayPosY = PLAYER_Y - 20;
		else if(padPosY == COMPUTER_Y + 150)
			displayPosY = COMPUTER_Y + 150;
		else if(padPosY == PLAYER_Y - 150)
			displayPosY = PLAYER_Y - 150;
		
		if(displayText("Speed : "+std::to_string(m_player[i]->getQuadParam().x_speed),10, displayPosY, 12) == false)
		{
			fprintf(stderr, "MPong::bonusDisplay : displayText failed for players\n");
			return false;
		} 
		
		if(displayText("Size : "+std::to_string(m_player[i]->getQuadParam().screen_location.w),10, displayPosY+13, 12) == false)
		{
			fprintf(stderr, "MPong::bonusDisplay : displayText failed for players\n");
			return false;
		} 

	}

	for(int i = 0; i<NB_COMPUTERS ;i++)
	{
		if(m_computer[i] == NULL)
			continue;
		
		int padPosY = m_computer[i]->getQuadParam().screen_location.y;
		if(padPosY == COMPUTER_Y)
			displayPosY = COMPUTER_Y + 20;
		else if(padPosY == PLAYER_Y)
			displayPosY = PLAYER_Y - 20;
		else if(padPosY == COMPUTER_Y + 150)
			displayPosY = COMPUTER_Y + 150;
		else if(padPosY == PLAYER_Y - 150)
			displayPosY = PLAYER_Y - 150;
		
		if(displayText("Speed : "+std::to_string(m_computer[i]->getQuadParam().x_speed),10, displayPosY, 12) == false)
		{
			fprintf(stderr, "MPong::bonusDisplay : displayText failed for computers\n");
			return false;
		}

		if(displayText("Size : "+std::to_string(m_computer[i]->getQuadParam().screen_location.w),10, displayPosY+13, 12) == false)
		{
			fprintf(stderr, "MPong::bonusDisplay : displayText failed for computers\n");
			return false;
		}
	}

	if(displayText("Ball Speed : "+std::to_string(m_ball->getQuadParam().y_speed),10, WINDOW_HEIGHT / 2, 12) == false)
	{
			fprintf(stderr, "MPong::bonusDisplay : displayText failed for the ball\n");
			return false;
	} 

	return true;
}

bool MPong::displayText(std::string text, int x, int y, int size)
{
	if(m_screenSurface == NULL)
	{
		fprintf(stderr, "MPong::displayText : m_screenSurface parameter not correct\n");
		return false;
	}
	
	TTF_Font * font = NULL;
	font = TTF_OpenFont(FONT_PATH, size);
	if(font == NULL)
	{
		fprintf(stderr, "MPong::displayText : Error failed to load TTF font\n");
		return false;
	}

	SDL_Color foreground = {255, 255, 255};
	SDL_Color background = {11, 8, 39};
	
	SDL_Surface* temp = NULL;
	temp = TTF_RenderText_Shaded(font, text.c_str(), foreground, background);
	if(temp == NULL)
	{
		fprintf(stderr, "MPong::displayText : Error, failed to load a temporarly SDL_Surface");
		TTF_CloseFont(font);
		return false;
	}

	SDL_Rect destination = {x, y, 0, 0};
	SDL_BlitSurface(temp, NULL, m_screenSurface, &destination);

	SDL_FreeSurface(temp);
	TTF_CloseFont(font);

	return true;
}

bool MPong::manageScore()
{
	if(m_ball == NULL)
	{
		fprintf(stderr, "MPong::manageScore : Error m_ball parameter(s) not correct\n");
		return true;
	}
	int scoreDown = m_ball->getScore(1);
	int scoreUp = m_ball->getScore(0);
	
	if(displayText("Score : "+std::to_string(scoreDown),10, PLAYER_Y + 10, 12) == false)
	{
		fprintf(stderr, "MPong::manageScore : Error displayText function failed\n");
		return true;
	}
	
	if(displayText("Score : "+std::to_string(scoreUp),10, COMPUTER_Y - 10, 12) == false)
	{
		fprintf(stderr, "MPong::manageScore : Error displayText function failed\n");
		return true;
	}

	if(scoreDown >= 10)
	{
		if(displayText("Down team win !!!",WINDOW_WIDTH/2,WINDOW_HEIGHT/2, 24) == false)
			fprintf(stderr, "MPong::managaScore : Error displayText function failed to show Down team win !\n");
		return true;
	}
	else if (scoreUp >= 10)
	{
		if(displayText("Up team win !!!",WINDOW_WIDTH/2,WINDOW_HEIGHT/2, 24) == false)
			fprintf(stderr, "MPong::managaScore : Error displayText function failed to show Up team win !\n");
		return true;
	}
	return false;
}


SDL_Surface * MPong::getScreenSurface()
{
	return m_screenSurface;
}

void MPong::setScreenSurface(SDL_Surface * screenSurface)
{
	m_screenSurface = screenSurface;
}

SDL_Window * MPong::getWindow()
{
	return m_window;
}

void MPong::setWindow(SDL_Window * window)
{
	m_window = window;
}

MMenu * MPong::getMenu()
{
	return m_menu;
}

void MPong::setMenu(MMenu * menu)
{
	m_menu = menu;
}

int MPong::getTimer()
{
	return m_timer;
}

void MPong::setTimer(int timer)
{
	m_timer = timer;
}

SDL_Event MPong::getEvent()
{
	return m_event;
}

void MPong::setEvent(SDL_Event event)
{
	m_event = event;
}

bool MPong::getGameError()
{
	return m_gameError;
}

void MPong::setGameError(bool gameError)
{
	m_gameError = gameError;
}

SDL_Surface * MPong::getBitmap()
{
	return m_bitmap;
}

void MPong::setBitmap(SDL_Surface * bitmap)
{
	m_bitmap = bitmap;
}

MBall * MPong::getBall()
{
	return m_ball;
}

void MPong::setBall(MBall * ball)
{
	m_ball = ball;
}

MPlayer * MPong::getPlayer(int index)
{
	if(index < 0)
		index = 0;
	else if(index >= NB_PLAYERS)
		index = NB_PLAYERS - 1;
	return m_player[index];
}

void MPong::setPlayer(MPlayer * player, int index)
{
	if(index < 0)
		index = 0;
	else if(index >= NB_PLAYERS)
		index = NB_PLAYERS - 1;
	m_player[index] = player;
}

MComputer * MPong::getComputer(int index)
{
	if(index < 0)
		index = 0;
	else if(index >= NB_COMPUTERS)
		index = NB_COMPUTERS - 1;
	
	return m_computer[index];
}

void MPong::setComputer(MComputer * computer, int index)
{
	if(index < 0)
		index = 0;
	else if(index >= NB_COMPUTERS)
		index = NB_COMPUTERS - 1;
	
	m_computer[index] = computer;
}

bool MPong::getInitGame()
{
	return m_initGame;
}

void MPong::setInitGame(bool initGame)
{
	m_initGame = initGame;
}

bool MPong::getBrickMode()
{
	return m_brickMode;
}

void MPong::setBrickMode(bool brickMode)
{
	m_brickMode = brickMode;
}

bool MPong::getEndGame()
{
	return m_endGame;
}

void MPong::setEndGame(bool endGame)
{
	m_endGame = endGame;
}
