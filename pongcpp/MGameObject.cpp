/*!
*	\file MGameObject.cpp
*	\author Boris Merminod
*	\brief Cpp file of the following classes : MGameObject, MPlayer and MComputer
*	\version 1.1.5
*
*	This file is a .cpp that contains the 
*	functions of MGameObject and its two 
*	herited classes MPlayer and MComputer.
*/

#include "MGameObject.h"

//~~~~~ Méthodes de la classe MGameObject ~~~~~ 

MGameObject::MGameObject(Entity quadParam, std::string quadColor, bool isBrick)
{
	m_quadParam = quadParam;
	m_quadColor = quadColor;
	m_isBrick = isBrick;
	m_feedbackFrames = 0;
	m_lastBitmapY = FEEDBACK_BITMAP_Y; 
	m_firstBitmapY = m_quadParam.bitmap_location.y;
	m_pixelGreenValue = 0;
	m_colorDownModulation = true;
}


bool MGameObject::checkBallCollision(Entity ball)
{
	//The nearest point from the collision's box
	int cX, cY;

	if(ball.screen_location.x + ball.screen_location.w < m_quadParam.screen_location.x)
		cX = m_quadParam.screen_location.x;
	else if (ball.screen_location.x > m_quadParam.screen_location.x + m_quadParam.screen_location.w)
		cX = m_quadParam.screen_location.x + m_quadParam.screen_location.w;
	else
		cX = ball.screen_location.x;
	
	if(ball.screen_location.y + ball.screen_location.h < m_quadParam.screen_location.y)
		cY = m_quadParam.screen_location.y;
	else if (ball.screen_location.y > m_quadParam.screen_location.y + m_quadParam.screen_location.h)
		cY = m_quadParam.screen_location.y + m_quadParam.screen_location.h;
	else
		cY = ball.screen_location.y;
	
	//If the nearest point is in the circle (the ball) here is a collision
	int deltaX = cX - ball.screen_location.x;
	int deltaY = cY - ball.screen_location.y;
	int distanceSquared = deltaX*deltaX + deltaY*deltaY;
	int r = ball.screen_location.w / 2;
	int rSquared = r * r;
	if(distanceSquared <= rSquared)
		return true;
	
	return false;
}

bool MGameObject::checkWallCollision(Direction dir)
{
	int temp_x;

	switch(dir)
	{
		case LEFT :
			temp_x = m_quadParam.screen_location.x - m_quadParam.x_speed;
			break;
		case RIGHT :
			temp_x = m_quadParam.screen_location.x + m_quadParam.screen_location.w + m_quadParam.x_speed;
			break;
	}

	if((temp_x <= 0) || (temp_x >= WINDOW_WIDTH))
		return true;
	
	return false;
}

void MGameObject::bonusGameObject(int randomBonus)
{
	switch(randomBonus)
	{
		case 1 :
			if(m_quadParam.screen_location.w < (PADDLE_WIDTH * 1.9))
			{
				m_quadParam.screen_location.w = m_quadParam.screen_location.w * 1.25;
				m_feedbackFrames = FEEDBACKFRAMES;
			}
			break;
		case 2 :
			if(m_quadParam.x_speed < (PLAYER_SPEED * 1.8))
			{
				m_quadParam.x_speed = m_quadParam.x_speed * 1.25;
				m_feedbackFrames = FEEDBACKFRAMES;
			}
			break;
	}
}

void MGameObject::malusGameObject(int randomMalus)
{
	switch(randomMalus)
	{
		case 1 :
			if(m_quadParam.screen_location.w > (PADDLE_WIDTH * 0.6))
			{
				m_quadParam.screen_location.w = m_quadParam.screen_location.w * 0.75;
				m_feedbackFrames = FEEDBACKFRAMES;
			}
			break;
		case 2 :
			if(m_quadParam.x_speed > (PLAYER_SPEED * 0.6))
			{
				m_quadParam.x_speed = m_quadParam.x_speed * 0.75;
				m_feedbackFrames = FEEDBACKFRAMES;
			}
			break;
	}
}

void MGameObject::spriteFeedback()
{
	if(m_feedbackFrames <= 0)
	{
		m_quadParam.bitmap_location.y = m_firstBitmapY;
		m_lastBitmapY = FEEDBACK_BITMAP_Y;
		return;
	}

	m_feedbackFrames--;

	if(m_feedbackFrames % (FRAMES_PER_SECOND/16) != 0)
		return;

	int bitmapY = m_quadParam.bitmap_location.y;
	m_quadParam.bitmap_location.y = m_lastBitmapY;
	m_lastBitmapY = bitmapY;
}


Entity MGameObject::getQuadParam()
{
	return m_quadParam;
}

void MGameObject::setQuadParam(Entity quadParam)
{
	m_quadParam = quadParam;
}

std::string MGameObject::getQuadColor()
{
	return m_quadColor;
}

bool MGameObject::getIsBrick()
{
	return m_isBrick;
}

int MGameObject::getFeedbackFrames()
{
	return m_feedbackFrames;
}

void MGameObject::setFeedbackFrames(int feedbackFrames)
{
	m_feedbackFrames = feedbackFrames;
}

int MGameObject::getPixelGreenValue()
{
	return m_pixelGreenValue;
}

void MGameObject::setPixelGreenValue(int pixelGreenValue)
{
	m_pixelGreenValue = pixelGreenValue; 
}

bool MGameObject::isColorDownModulation()
{
	return m_colorDownModulation;
}

void MGameObject::setIsColorDownModulation(bool colorDownModulation)
{
	m_colorDownModulation = colorDownModulation;
}

//~~~~~ Méthodes de la classe MPlayer

MPlayer::MPlayer(Entity quadParam, std::string quadColor, SDL_Keycode leftButton, SDL_Keycode rightButton) : MGameObject(quadParam, quadColor, false)
{
	m_leftButton = leftButton;
	m_rightButton = rightButton;
	m_left_pressed = false;
	m_right_pressed = false;
}

void MPlayer::playerEventHandler(SDL_Event e)
{

	if(e.type == SDL_KEYDOWN && (m_left_pressed == false || m_right_pressed == false))
	{
			if(e.key.keysym.sym == m_leftButton)
				m_left_pressed = true;
			else if(e.key.keysym.sym == m_rightButton)
				m_right_pressed = true;
	}
	else if(e.type == SDL_KEYUP && (m_left_pressed == true || m_right_pressed == true))
	{
		if(e.key.keysym.sym == m_leftButton)
			m_left_pressed = false;
		else if(e.key.keysym.sym == m_rightButton)
			m_right_pressed = false;
	}
	
	if(m_left_pressed == true)
	{
		if(checkWallCollision(LEFT) == false)
			m_quadParam.screen_location.x -= m_quadParam.x_speed;
	}
	
	if(m_right_pressed == true)
	{
		if(checkWallCollision(RIGHT) == false)
			m_quadParam.screen_location.x += m_quadParam.x_speed;
	}
}

//~~~~~ Méthodes de la classe MComputer ~~~~~
MComputer::MComputer(Entity quadParam, std::string quadColor, int computerLevel, int decision ) : MGameObject(quadParam, quadColor, false)
{
	switch(computerLevel)
	{
		case 0 :
			m_computerLevel = 75;
			break;
		case 1 :
			m_computerLevel = 50;
			break;
		case 2 :
			m_computerLevel = 25;
			break;
		case 3 : 
			m_computerLevel = 1;
			break;
		default :
			m_computerLevel = 75;
			break;
	}
	m_decision = decision;
}

void MComputer::computerHandler(Entity ball, int randomValue)
{
	if(randomValue <= m_computerLevel)
		return;
	int computer_x;
	int ball_center = ball.screen_location.x + ball.screen_location.w / 2;

	randomValue = rand() % 60;
	if(randomValue == 1)
		m_decision  = rand() % 3 + 1;

	//the Pad's area that will be used to hit the ball, in function of m_decision
	switch(m_decision)
	{
		case 1 :
			computer_x = m_quadParam.screen_location.x;
			break;
		case 2 :
			computer_x = m_quadParam.screen_location.x + m_quadParam.screen_location.w;
			break;
		case 3 :
			computer_x = m_quadParam.screen_location.x + m_quadParam.screen_location.w / 2;
	}

	//Prevent imbalanced computer movement
	if(abs(computer_x - ball_center) < 10)
		return ;

	//It moving on the left
	if(computer_x > ball_center)
	{
		if(!checkWallCollision(LEFT))
			m_quadParam.screen_location.x -= m_quadParam.x_speed;
	}
	//It moving on the right
	else if(computer_x < ball_center)
	{
		if(!checkWallCollision(RIGHT))
			m_quadParam.screen_location.x += m_quadParam.x_speed;
	}

}

 int MComputer::getDecision()
 {
	 return m_decision;
 }

 void MComputer::setDecision(int decision)
 {
	 m_decision = decision;
 }

 int MComputer::getComputerLevel()
 {
	 switch(m_computerLevel)
	 {
		case 75 : 
		 	return 1;
		case 50 : 
			return 2;
		case 25 : 
			return 3;
		case 1 : 
			return 4; 
		default : 
			fprintf(stderr, "MComputer::getComputerLevel : Error ! ComputerLevel unrecognized\n");
			return 0;
	 }
 }

 void MComputer::setComputerLevel(int computerLevel)
 {
	switch(computerLevel)
	{
		case 1 : 
		 	m_computerLevel = 75;
			break;
		case 2 : 
			m_computerLevel = 50;
			break;
		case 3 : 
			m_computerLevel = 25;
			break;
		case 4 : 
			m_computerLevel = 1;
			break; 
		default : 
			fprintf(stderr, "MComputer::setComputerLevel : Error ! computerLevel unrecognized m_computerLevel will be set to 1 (Easy)\n");
			m_computerLevel = 75;
	}
 }

 //~~~~~ Méthodes de la classe MBall ~~~~~

 int MBall::cBALL_SPEED_Y;

 MBall::MBall(Entity quadParam, std::string quadColor, Direction team) : MGameObject(quadParam, quadColor, false)
 {
	 m_team = team;
	 m_score[0] = 0;
	 m_score[1] = 0;
	 m_lastTeam = NOMODE;
	 m_collisionLocked = false;
 }
 
 bool MBall::moveBall()
{
	m_quadParam.screen_location.x += m_quadParam.x_speed;
	m_quadParam.screen_location.y += m_quadParam.y_speed;

	//Handle collision between Ball and screen
	if( 
		(
			(m_quadParam.x_speed < 0) &&
			checkWallCollision(LEFT) 
		) ||
		(
			(m_quadParam.x_speed > 0) &&
			checkWallCollision(RIGHT)
		)
	)
		m_quadParam.x_speed = -m_quadParam.x_speed;
	
	//Reset Ball position when it goes out of screen by up or down side
	if(m_quadParam.screen_location.y > WINDOW_HEIGHT || m_quadParam.screen_location.y < 0)
	{

		if(m_quadParam.screen_location.y > WINDOW_HEIGHT)
			m_score[0] += 1;
		else if(m_quadParam.screen_location.y < 0)
			m_score[1] += 1;

		int randomDir = rand () % 2;
		if(randomDir == 0)
			randomDir = -1;
		else
			randomDir = 1;

		m_quadParam.screen_location.y = WINDOW_HEIGHT / 2;
		m_quadParam.screen_location.x = WINDOW_WIDTH / 2;
		m_quadParam.x_speed = 0;
		m_quadParam.y_speed = randomDir* cBALL_SPEED_Y;
		
		m_lastTeam = NOMODE;

		if(m_quadParam.y_speed < 0)
			m_team = DOWN;
		else if(m_quadParam.y_speed > 0)
			m_team = UP;
		return true;
	}

	return false;
}		


std::vector<MGameObject*> MBall::handleBall(std::vector<MGameObject*> gameObjectList)
{
	std::vector<MGameObject*> returnList;
	bool resetBallPosition = moveBall();
	std::string brickColor = "";
	int index_player[4] = {-1, -1, -1, -1};
	int current_index = 0;

	for(int i=0 ; i<gameObjectList.size(); i++)
	{
		if(gameObjectList[i]->checkBallCollision(m_quadParam) == true)
		{
			if(gameObjectList[i]->getIsBrick() == true)
			{
				pongBrickBall(gameObjectList[i]->getQuadParam());
				brickColor = gameObjectList[i]->getQuadColor();
			}
			else
			{
				if(index_player[current_index] == -1)
				{
					index_player[current_index] = i;
					current_index++;
					current_index %= 4;
				}

				if(m_collisionLocked == false)
					pongBall(gameObjectList[i]->getQuadParam());
				returnList.push_back(gameObjectList[i]);
			}
		}
		else
		{
			if(m_collisionLocked == true)
				m_collisionLocked = false;
			if(gameObjectList[i]->getIsBrick() == false)
			{
				if(index_player[current_index] == -1)
				{
					index_player[current_index] = i;
					current_index++;
					current_index %= 4;
				}
			}
			returnList.push_back(gameObjectList[i]);
		}
	}
	//Brick Mode only
	if((brickColor != "") && (index_player[0] != -1) && (index_player[1] != -1))
	{
		int randomValue = rand() % 2 + 1; 
		handleBrickBonus(brickColor, gameObjectList[index_player[0]], gameObjectList[index_player[1]], randomValue);
	}

	if(resetBallPosition == true)
	{// reset == cancel bonuses/maluses	
		for(int i = 0; i<4; i++)
		{
			if(index_player[i] == -1)
				break;
			Entity resetEntity = gameObjectList[index_player[i]]->getQuadParam();
			resetEntity.screen_location.w = PADDLE_WIDTH;
			resetEntity.x_speed = PLAYER_SPEED;
			gameObjectList[index_player[i]]->setQuadParam(resetEntity);
			gameObjectList[index_player[i]]->setFeedbackFrames(0);
		}
	}
	return returnList;	
}



void MBall::handleBrickBonus(std::string brickColor, MGameObject * player1, MGameObject * player2, int randomValue)
{
	if(brickColor == "blanc")
	{
		if(m_quadParam.y_speed < 0)
			m_quadParam.y_speed -= 1;
		else
			m_quadParam.y_speed += 1;

		m_feedbackFrames = FEEDBACKFRAMES;
	}
	else if(m_lastTeam == UPTEAM)
	{
		if(player1->getQuadParam().screen_location.y < WINDOW_HEIGHT / 2)
		{
			if(player1->getQuadColor() == brickColor)
				player1->bonusGameObject(randomValue);
			else
				player1->malusGameObject(randomValue);
		}
		else
		{
			if(player2->getQuadColor() == brickColor)
				player2->bonusGameObject(randomValue);
			else
				player2->malusGameObject(randomValue);
		}
	}
	else if(m_lastTeam == DOWNTEAM)
	{
		if(player1->getQuadParam().screen_location.y > WINDOW_HEIGHT / 2)
		{
			if(player1->getQuadColor() == brickColor)
				player1->bonusGameObject(randomValue);
			else
				player1->malusGameObject(randomValue);
		}
		else
		{
			if(player2->getQuadColor() == brickColor)
				player2->bonusGameObject(randomValue);
			else
				player2->malusGameObject(randomValue);
		}
	}

}

Direction MBall::checkForOverlap(Entity entity)
{
	int entityLeftX = entity.screen_location.x,
	entityRightX = entity.screen_location.x + entity.screen_location.w,
	entityUpY = entity.screen_location.y,
	entityDownY = entity.screen_location.y + entity.screen_location.h;

	int entityCenterX = entity.screen_location.x + entity.screen_location.w / 2,
	entityCenterY = entity.screen_location.y + entity.screen_location.h / 2;

	int ballLeftX = m_quadParam.screen_location.x,
	ballRightX = m_quadParam.screen_location.x + m_quadParam.screen_location.w,
	ballUpY = m_quadParam.screen_location.y,
	ballDownY = m_quadParam.screen_location.y + m_quadParam.screen_location.h;

	int ballCenterX = m_quadParam.screen_location.x + m_quadParam.screen_location.w / 2,
	ballCenterY = m_quadParam.screen_location.y + m_quadParam.screen_location.h / 2;

	int overlapLeftX = 0, overlapRightX = 0, overlapUpY = 0, overlapDownY = 0, overlapDistanceX = 0, overlapDistanceY = 0;

	if(entityLeftX >= ballLeftX)
		overlapLeftX = entityLeftX;
	else
		overlapLeftX = ballLeftX;

	if(entityRightX <= ballRightX)
		overlapRightX = entityRightX;
	else
		overlapRightX = ballRightX;
	

	if(entityUpY >= ballUpY)
		overlapUpY = entityUpY;
	else
		overlapUpY = ballUpY;
	

	if(entityDownY <= ballDownY)
		overlapDownY = entityDownY;
	else
		overlapDownY = ballDownY;

	overlapDistanceX = overlapRightX - overlapLeftX;
	overlapDistanceY = overlapDownY - overlapUpY;

	if(overlapDistanceX == overlapDistanceY)
	{
		if(ballCenterY > entityCenterY)
				return CORNERTOP;
		else
			return CORNERBOTTOM;

	}
	else if(overlapDistanceX > overlapDistanceY)
	{
		if(ballCenterY > entityCenterY)
			return DOWN;
		else
			return UP;
	}		
	else if(overlapDistanceX < overlapDistanceY)
	{
		if(ballCenterX > entityCenterX)
			return RIGHT;
		else
			return LEFT;
	}

	return NONE;
}


void MBall::pongBall(Entity padEntity)
{
	if(((padEntity.screen_location.y < WINDOW_HEIGHT / 2) && (m_team == DOWN)) || ((padEntity.screen_location.y > WINDOW_HEIGHT / 2) && (m_team == UP)) )
	{

		Direction collisionDirection = checkForOverlap(padEntity);

		if((padEntity.screen_location.y < WINDOW_HEIGHT / 2 && (collisionDirection == DOWN || collisionDirection == CORNERTOP) ) || (padEntity.screen_location.y > WINDOW_HEIGHT / 2 && (collisionDirection == UP || collisionDirection == CORNERTOP) ))
		{

			int paddle_center = padEntity.screen_location.x + padEntity.screen_location.w / 2;
			int ball_center = m_quadParam.screen_location.x + m_quadParam.screen_location.w / 2;

			int paddle_location = ball_center - paddle_center;

			int ballSpeedModifier = round((float)BALL_SPEED_MODIFIER / abs((float)m_quadParam.y_speed / 5.0));

			ballSpeedModifier =  round((float)ballSpeedModifier * ((float)padEntity.screen_location.w / (float)PADDLE_WIDTH));
			
			m_quadParam.x_speed = paddle_location / ballSpeedModifier;
			m_quadParam.y_speed = -m_quadParam.y_speed;
		}
		else if(collisionDirection == LEFT)
		{
			if(m_quadParam.x_speed > 0)
				m_quadParam.x_speed = (checkWallCollision(LEFT) ? m_quadParam.x_speed : -m_quadParam.x_speed);
			else
				m_quadParam.x_speed -= (checkWallCollision(LEFT) ? -m_quadParam.x_speed : 1); 
			
			m_collisionLocked = true;
		}
		else if(collisionDirection == RIGHT)
		{
			if(m_quadParam.x_speed < 0)
				m_quadParam.x_speed = (checkWallCollision(RIGHT) ? m_quadParam.x_speed : -m_quadParam.x_speed);
			else
				m_quadParam.x_speed += (checkWallCollision(RIGHT) ? -m_quadParam.x_speed : 1); 
			
			m_collisionLocked = true;
		}

		if(m_quadParam.y_speed < 0)
		{
			m_team = DOWN;
			m_lastTeam = DOWNTEAM;
		}
		else if(m_quadParam.y_speed > 0)
		{
			m_team = UP;
			m_lastTeam = UPTEAM;
		}
	}
}

void MBall::pongBrickBall(Entity brickEntity)
{
	int multx = 1, multy = 1;
	Direction brickSide = checkForOverlap(brickEntity);	
	
	if(brickSide == CORNERTOP || brickSide == CORNERBOTTOM)
	{
		multx = -1;
		multy = -1;
	}
	else if(m_quadParam.x_speed > 0)
	{
		if(m_quadParam.y_speed > 0)
		{
			if(brickSide == LEFT || brickSide == DOWN)
				multx = -1;
			else
				multy = -1;
		}
		else if(m_quadParam.y_speed < 0)
		{
			if(brickSide == LEFT || brickSide == UP)
				multx = -1;
			else 
				multy = -1;
		}
	}
	else if(m_quadParam.x_speed < 0)
	{
		if(m_quadParam.y_speed > 0)
		{
			if(brickSide == RIGHT || brickSide == DOWN)
				multx = -1;
			else 
				multy = -1;
		}
		else if(m_quadParam.y_speed < 0)
		{
			if(brickSide == UP || brickSide == RIGHT)
				multx = -1;
			else 
				multy = -1;
		}
	}
	else if(m_quadParam.x_speed == 0)
	{
		multy = -1;
	}
	
	m_quadParam.x_speed = multx * m_quadParam.x_speed;
	m_quadParam.y_speed = multy * m_quadParam.y_speed;

	if(m_quadParam.y_speed < 0)
		m_team = DOWN;
	else if(m_quadParam.y_speed > 0)
		m_team = UP;
}

Direction MBall::getTeam()
{
	return m_team;
}

void MBall::setTeam( Direction team)
{
	m_team = team;
}

int MBall::getScore(int index) 
{
	if(index > 1)
		index = 1;
	else if(index < 0)
		index = 0;
	return m_score[index];
}

void MBall::setScore(int index, int value)
{
	if(index > 1)
		index = 1;
	else if(index < 0)
		index = 0;
	
	m_score[index] = value;
}

PickUp MBall::getLastTeam()
{
	return m_lastTeam;
}

void MBall::setLastTeam(PickUp lastTeam)
{
	m_lastTeam = lastTeam;
}

