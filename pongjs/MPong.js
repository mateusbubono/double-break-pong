
let MPong = {

	m_canvas : null,
	m_context : null,
	m_bitmap : null,
	m_menu : null,
	m_timer : null,
	m_initGame : false,
	m_brickMode : false,
	m_endGame : false,
	m_gameObjectList : [],
	m_player : [],
	m_computer : [],
	m_stateStack : [],

	menu()
	{
		if(Date.now() - this.m_timer < 1000 / 60){
			return;
		}
		this.m_context.fillStyle = "#0b0827";
		this.m_context.fillRect(0, 0, 800, 600);
		this.displayText("Double Break Pong", Define.WINDOW_WIDTH /4, 50, 40);
		this.m_menu.displayText(this.m_context);
		this.m_timer = Date.now();
	},

	game()
	{
		if(this.m_initGame == false)
		{
			this.m_initGame = this.initGame()
		}

		if(Date.now() - this.m_timer < Define.FRAME_RATE){
			return;
		}

		if(this.m_endGame == true)
		{
			this.m_timer = Date.now();
			return;
		}

		if(this.m_brickMode == true)
		{
			this.brickSpawner();
		}
		else 
		{
			this.bonusLauncher();
		}

		this.m_gameObjectList = this.m_ball.handleBall(this.m_gameObjectList);

		for(let computer of this.m_computer)
		{
			if(computer != null)
			{
				computer.computerHandler(this.m_ball.getQuadParam, Math.random() * 100);
			}
		}

		for(let player of this.m_player)
		{
			if(player != null)
			{
				player.playerUpdateMovement();
			}
		}

		//Draw Sprites
		this.m_context.fillStyle = "#0b0827";
		this.m_context.fillRect(0, 0, 800, 600);

		this.bonusDisplay();

		if(this.m_endGame == false)
		{
			this.m_endGame = this.manageScore();
		}

		if(this.m_endGame == true)
		{
			return;
		}
		//setTimeout(this.colorFeedback(), 500);
		for(let gameObject of this.m_gameObjectList)
		{
			if(gameObject == null)
			{
				continue;
			}
			gameObject.spriteFeedBack();
			let pad = gameObject.getQuadParam;
			this.m_context.drawImage(this.m_bitmap,pad.bitmap_location_x,pad.bitmap_location_y,pad.bitmap_location_w,pad.bitmap_location_h,pad.screen_location_x, pad.screen_location_y, pad.screen_location_w, pad.screen_location_h);
			this.colorFeedback(gameObject);

		}
		//this.colorFeedback();

		this.m_ball.spriteFeedBack();
		let ballEntity = this.m_ball.getQuadParam;

		this.m_context.drawImage(this.m_bitmap,ballEntity.bitmap_location_x,ballEntity.bitmap_location_y,ballEntity.bitmap_location_w,ballEntity.bitmap_location_h,ballEntity.screen_location_x, ballEntity.screen_location_y, ballEntity.screen_location_w, ballEntity.screen_location_h);

		//Update Screen
		this.m_timer = Date.now();
	},

	colorFeedback(gameObject)
	{
		if(gameObject == null){
			return;
		}
		if(gameObject.getQuadColor == "blanc"){
			return;
		}

		let colorMinTreshold = 10, colorMaxTreshold = 250, colorSpeedModulation = 5;

		let pixels = this.m_context.getImageData(gameObject.getQuadParam.screen_location_x, gameObject.getQuadParam.screen_location_y, gameObject.getQuadParam.screen_location_w, gameObject.getQuadParam.screen_location_h );
		
		let data = pixels.data;

		for (let i = 0; i < data.length; i += 4) {
			//if the green != background color
			if(data[i+1] != 8){
				//Green color modulation
				data[i+1] = gameObject.getPixelGreenValue;
			}
		}

		if(gameObject.isColorDownModulation == true){
			gameObject.m_pixelGreenValue -=colorSpeedModulation;
		}else{
			gameObject.m_pixelGreenValue+=colorSpeedModulation;
		}

		if(gameObject.getPixelGreenValue >= colorMaxTreshold)
		{
			gameObject.setIsColorDownModulation = true;
			gameObject.m_pixelGreenValue-=colorSpeedModulation;
		}
		else if(gameObject.getPixelGreenValue <= colorMinTreshold)
		{
			gameObject.setIsColorDownModulation = false;
			gameObject.m_pixelGreenValue+=colorSpeedModulation;
		}
		
		this.m_context.putImageData(pixels,gameObject.getQuadParam.screen_location_x,  gameObject.getQuadParam.screen_location_y);
	},

	initGame()
	{
		let gameModeList = this.m_menu.getGameModeList;
		let tabEntity = [null, null, null, null];
		let flag = {
			twoPlayerFlag : false, doubleModeFlag : false, teamFlag : false, downFlag : false, frontFlag : false, computerLevel : 0
		};  
		let tabColorString = ["", "", "", ""];
		
		this.initTransformAndColor(tabEntity, tabColorString);
		this.initGameMode(flag, gameModeList);

		let ballEntity = EntityConstructor(
			(Define.WINDOW_WIDTH/2) - (Define.BALL_DIAMETER/2),
			(Define.WINDOW_HEIGHT/2) - 100,
			Define.BALL_DIAMETER,
			Define.BALL_DIAMETER,
			Define.BALL_BITMAP_X,
			Define.BALL_BITMAP_Y,
			Define.BALL_DIAMETER,
			Define.BALL_DIAMETER,
			0,
			Define.BALL_SPEED_Y 
		);

		this.m_ball = new MBall(ballEntity, "blanc", Direction.UP);

		this.m_ball.setScore(0, 0);
		this.m_ball.setScore(1, 0);

		this.initAllPlayers(flag.twoPlayerFlag, flag.doubleModeFlag, flag.teamFlag, flag.downFlag, flag.frontFlag, flag.computerLevel, tabEntity, tabColorString);

		for(player of this.m_player){
			if(player != null){
				this.m_gameObjectList.push(player);
			}
		}

		for(computer of this.m_computer){
			if(computer != null){
				this.m_gameObjectList.push(computer);
			}
		}

		//setTimeout(this.colorFeedback(this.m_canvas, 100, true), 500);

		return true;
	
	},

	initTransformAndColor(tabEntity, tabColorString)
	{
		for(let i = 0; i< tabEntity.length; i++)
		{
			tabEntity[i] = EntityConstructor(
				Define.WINDOW_WIDTH / 2 - Define.PADDLE_WIDTH / 2,
				0,
				Define.PADDLE_WIDTH,
				Define.PADDLE_HEIGHT,
				Define.PADDLE_BITMAP_X,
				0,
				Define.PADDLE_WIDTH,
				Define.PADDLE_HEIGHT,
				Define.PLAYER_SPEED,
				0
			);
			
			switch(i)
			{
				case 0 :
					tabColorString[i] = "bleu";
					tabEntity[i].screen_location_y = Define.PLAYER_Y;
					tabEntity[i].bitmap_location_y = Define.PADDLE_BITMAP_BLUE_Y;
					break;
				case 1 :
					tabColorString[i] = "bleu";
					tabEntity[i].screen_location_y = 	Define.PLAYER_FRONT_Y;
					console.log(Define.PLAYER_FRONT_Y);
					tabEntity[i].bitmap_location_y = 	Define.PADDLE_BITMAP_BLUE_Y;
					break;
				case 2 :
					tabColorString[i] = "rouge";
					tabEntity[i].screen_location_y = 	Define.COMPUTER_Y;
					tabEntity[i].bitmap_location_y = 	Define.PADDLE_BITMAP_RED_Y;
					break;
				case 3 :
					tabColorString[i] = "rouge";
					tabEntity[i].screen_location_y = 	Define.COMPUTER_FRONT_Y;
					tabEntity[i].bitmap_location_y = 	Define.PADDLE_BITMAP_RED_Y;
					break;
			}
		}
	},

	initGameMode(flag, gameModeList)
	{
		for(let gameMode of gameModeList)
		{
			switch(gameMode)
			{
				case PickUp.ONEPLAYER :
					flag.twoPlayerFlag = false;
					break;
				case PickUp.TWOPLAYERS :
					flag.twoPlayerFlag = true;
					break;
				case PickUp.BRICKMODE :
					flag.doubleModeFlag = false;
					break;
				case PickUp.DOUBLEMODE :
					flag.doubleModeFlag = true;
					break;
				case PickUp.VERSUS :
					flag.teamFlag = false;
					break;
				case PickUp.TEAM :
					flag.teamFlag = true;
					break;
				case PickUp.DOWNTEAM :
					flag.downFlag = true;
					break;
				case PickUp.UPTEAM :
					flag.downFlag = false;
					break;
				case PickUp.BACK :
					flag.frontFlag = false;
					break;
				case PickUp.FRONT :
					flag.frontFlag = true;
					break;
				case PickUp.BALLSPEEDSLOW :
					Define.BALL_SPEED_Y = 3;
					break;
				case PickUp.BALLSPEEDMEZZO :
					Define.BALL_SPEED_Y = 5;
					break;
				case PickUp.BALLSPEEDFAST :
					Define.BALL_SPEED_Y = 7;
					break;
				case PickUp.COMPUTEREASY :
					flag.computerLevel = 0;
					break;
				case PickUp.COMPUTERMEDIUM :
					flag.computerLevel = 1;
					break;
				case PickUp.COMPUTERHARD :
					flag.computerLevel = 2;
					break;
				case PickUp.COMPUTERHARDEST :
					flag.computerLevel = 3;
					break;
			}

		}
	},

	initAllPlayers(twoPlayerFlag, doubleModeFlag, teamFlag, downFlag, frontFlag, computerLevel, tabEntity, tabColorString)
	{
		if(twoPlayerFlag == true)
		{
			if(doubleModeFlag == true)
			{
				this.m_brickMode = false;
				if(teamFlag == true)
				{
					if(downFlag == true)
					{
						if(frontFlag == true)
						{
							this.m_player[0] = new MPlayer(tabEntity[1], tabColorString[1], "ArrowLeft", "ArrowRight");
							this.m_player[1] = new MPlayer(tabEntity[0], tabColorString[0], "q", "d");
						}
						else 
						{
							this.m_player[0] = new MPlayer(tabEntity[0], tabColorString[0], "ArrowLeft", "ArrowRight");
							this.m_player[1] = new MPlayer(tabEntity[1], tabColorString[1], "q", "d");
						}
						this.m_computer[0] = new MComputer(tabEntity[2], tabColorString[2], computerLevel, 1);
						this.m_computer[1] = new MComputer(tabEntity[3], tabColorString[3], computerLevel, 1);
					}
					else //Players team up
					{
						if(frontFlag == true)
						{
							this.m_player[0] = new MPlayer(tabEntity[3], tabColorString[3],"ArrowLeft", "ArrowRight");
							this.m_player[1] = new MPlayer(tabEntity[2], tabColorString[2], "q", "d");
						}
						else
						{
							this.m_player[0] = new MPlayer(tabEntity[2], tabColorString[2], "ArrowLeft", "ArrowRight");
							this.m_player[1] = new MPlayer(tabEntity[3], tabColorString[3], "q", "d");
						}
						this.m_computer[0] = new MComputer(tabEntity[0], tabColorString[0], computerLevel, 1);
						this.m_computer[1] = new MComputer(tabEntity[1], tabColorString[1], computerLevel, 1);
					}
				}
				else //Versus double mode two players
				{
					if(downFlag == true)
					{
						if(frontFlag == true)
						{
							this.m_player[0] = new MPlayer(tabEntity[1], tabColorString[1], "ArrowLeft", "ArrowRight");
							this.m_player[1] = new MPlayer(tabEntity[3], tabColorString[3], "q", "d");
							this.m_computer[0] = new MComputer(tabEntity[0], tabColorString[0], computerLevel, 1);
							this.m_computer[1] = new MComputer(tabEntity[2], tabColorString[2], computerLevel, 1);
						}
						else
						{
							this.m_player[0] = new MPlayer(tabEntity[0], tabColorString[0], "ArrowLeft", "ArrowRight");
							this.m_player[1] = new MPlayer(tabEntity[2], tabColorString[2], "q", "d");
							this.m_computer[0] = new MComputer(tabEntity[1], tabColorString[1], computerLevel, 1);
							this.m_computer[1] = new MComputer(tabEntity[3], tabColorString[3], computerLevel, 1);
						}
					}
					else
					{
						if(frontFlag == true)
						{
							this.m_player[0] = new MPlayer(tabEntity[3], tabColorString[3],"ArrowLeft", "ArrowRight");
							this.m_player[1] = new MPlayer(tabEntity[1], tabColorString[1], "q", "d");
							this.m_computer[0] = new MComputer(tabEntity[0], tabColorString[0], computerLevel, 1);
							this.m_computer[1] = new MComputer(tabEntity[2], tabColorString[2], computerLevel, 1);
						}
						else
						{
							this.m_player[0] = new MPlayer(tabEntity[2], tabColorString[2], "ArrowLeft", "ArrowRight");
							this.m_player[1] = new MPlayer(tabEntity[0], tabColorString[0], "q", "d");
							this.m_computer[0] = new MComputer(tabEntity[1], tabColorString[1], computerLevel, 1);
							this.m_computer[1] = new MComputer(tabEntity[3], tabColorString[3], computerLevel, 1);
						}
					}
				}
			}
			else //BrickMode two players
			{
				this.m_brickMode = true;
				if(downFlag == true)
				{
					this.m_player[0] = new MPlayer(tabEntity[0], tabColorString[0], "ArrowLeft", "ArrowRight");
					this.m_player[1] = new MPlayer(tabEntity[2], tabColorString[2], "q", "d");
				}
				else 
				{
					this.m_player[0] = new MPlayer(tabEntity[2], tabColorString[2], "ArrowLeft", "ArrowRight");
					this.m_player[1] = new MPlayer(tabEntity[0], tabColorString[0], "q", "d");
				}
			}		
		}
		else //One player mode
		{
			if(doubleModeFlag == true)
			{
				this.m_brickMode = false;
				if(downFlag == true)
				{
					if(frontFlag == true)
					{
						this.m_player[0] = new MPlayer(tabEntity[1], tabColorString[1], "ArrowLeft", "ArrowRight");
						this.m_computer[0] = new MComputer(tabEntity[0], tabColorString[0], computerLevel, 1);
					}
					else //Player position back
					{
						this.m_player[0] = new MPlayer(tabEntity[0], tabColorString[0], "ArrowLeft", "ArrowRight");
						this.m_computer[0] = new MComputer(tabEntity[1], tabColorString[1], computerLevel, 1);
					}
					this.m_computer[1] = new MComputer(tabEntity[2], tabColorString[2], computerLevel, 1);
					this.m_computer[2] = new MComputer(tabEntity[3], tabColorString[3], computerLevel, 1);
				}
				else //Player position up
				{
					if(frontFlag == true)
					{
						this.m_player[0] = new MPlayer(tabEntity[3], tabColorString[3], "ArrowLeft", "ArrowRight");
						this.m_computer[0] = new MComputer(tabEntity[2], tabColorString[2], computerLevel, 1);
					}
					else // Player position back
					{
						this.m_player[0] = new MPlayer(tabEntity[2], tabColorString[2], "ArrowLeft", "ArrowRight");
						this.m_computer[0] = new MComputer(tabEntity[3], tabColorString[3], computerLevel, 1);
					}

					this.m_computer[1] = new MComputer(tabEntity[0], tabColorString[0], computerLevel, 1);
					this.m_computer[2] = new MComputer(tabEntity[1], tabColorString[1], computerLevel, 1);
				}
			}
			else //BrickMode One Player
			{
				this.m_brickMode = true;
				if(downFlag == true)
				{
					this.m_player[0] = new MPlayer(tabEntity[0], tabColorString[0], "ArrowLeft", "ArrowRight");
					this.m_computer[0] = new MComputer(tabEntity[2], tabColorString[2], computerLevel, 1);
				}
				else //Player position up
				{
					this.m_player[0] = new MPlayer(tabEntity[2], tabColorString[2], "ArrowLeft", "ArrowRight");
					this.m_computer[0] = new MComputer(tabEntity[0], tabColorString[0], computerLevel, 1);
				}
			}
		}
	},

	handleGameInput(event)
	{
		switch(event.type)
		{
			case "keydown" :
				for(let player of this.m_player){
					if(player != null){
						player.playerEventHandler(event);
					}
				}
				break;
			case "keyup" :
				if(this.m_endGame == true)
				{
					this.m_initGame = false;
					this.m_endGame = false;
					this.endGame();
					this.m_stateStack.pop();
				}
				else if(event.key == "Escape")
				{
					this.m_endGame = true;
				}
				else
				{
					for(let player of this.m_player){
						if(player != null){
							player.playerEventHandler(event);
						}
					}
				}
				break;
		}
	},

	endGame()
	{
		for(let i = 0; i<this.m_player.length; i++)
		{
			if(this.m_player[i] != null)
			{
				// ? delete m_player[i]
				this.m_player[i] = null;
			}
		}

		for(let i = 0; i< this.m_computer.length; i++)
		{
			if(this.m_computer[i] != null)
			{
				// ? delete this.m_computer[i]
				this.m_computer[i] = null;
			}
		}

		if(this.m_ball != null)
		{
			//delete this.m_ball;
			this.m_ball = null;
		}

		this.m_gameObjectList = [];
	},

	brickSpawner()
	{
		let brickColor = "blanc";
		let randomValue = Math.round(Math.random() * 50) + 1;
		if(randomValue == 1)
		{
			let brickEntity = EntityConstructor(
				Math.round(Math.random() * (Define.WINDOW_WIDTH - 200)) + 100,
				Math.round(Math.random() * (Define.WINDOW_HEIGHT - 300)) + 100,
				40,
				40, 
				0,
				Define.BRICK_Y,
				40,
				40,
				0,
				0
			);

			randomValue = Math.round(Math.random() * 2);
			switch(randomValue)
			{
				case 0 :
					brickEntity.bitmap_location_x = Define.BRICK_RED_X;
					brickColor = "rouge";
					break;
				case 1 :
					brickEntity.bitmap_location_x = Define.BRICK_BLUE_X;
					brickColor = "bleu";
					break;
				case 2 :
					brickEntity.bitmap_location_x = Define.BRICK_WHITE_X;
					brickColor = "blanc";
					break;
			}

			for(gameObject of this.m_gameObjectList)
			{
				if(gameObject.checkBallCollision(brickEntity) == true){
					return true;
				}
			}

			var brick = new MGameObject(brickEntity, brickColor, true);
			this.m_gameObjectList.push(brick);
		}
	},

	bonusLauncher()
	{
		let randomValue = Math.round(Math.random() * 50);
		if(randomValue != 1){
			return;
		}

		for(player of this.m_player)
		{
			if(player == null){
				continue;
			}

			randomValue = Math.round(Math.random()*4);
			if(randomValue != 0){
				continue;
			}

			randomValue = Math.round(Math.random());
			if(randomValue == 0){
				player.bonusGameObject(Math.round(Math.random() + 1));
			}else if(randomValue == 1){
				player.malusGameObject(Math.round(Math.random() + 1));
			}

			console.log(randomValue);

			return;
		}

		for(computer of this.m_computer)
		{
			if(computer == null){
				continue;
			}
			
			randomValue = Math.round(Math.random() * 4);
			if(randomValue != 0){
				continue;
			}

			randomValue = Math.round(Math.random());
			if(randomValue == 0){
				computer.bonusGameObject(Math.round(Math.random()) + 1);
			}else if(randomValue == 1){
				computer.malusGameObject(Math.round(Math.random()) + 1);
			}
			return;
		}
		let ballEntity = this.m_ball.getQuadParam;
		if(ballEntity.y_speed > 0)
		{
			ballEntity.y_speed += 1;
			this.m_ball.setFeedbackFrames = 30;
		}
		else
		{
			ballEntity.y_speed -= 1;
			this.m_ball.setFeedbackFrames = 30;
		}
		this.m_ball.setQuadParam = ballEntity;
	},

	displayText(text, x, y, size)
	{
		this.m_context.font = String(size) + "px Ubuntu";
		this.m_context.fillStyle = "white";
		this.m_context.fillText(text, x, y);
	},

	bonusDisplay()
	{
		let displayPosY = 0;
		for(player of this.m_player)
		{
			if(player == null){
				continue;
			}

			let padPosY = player.getQuadParam.screen_location_y;
	
			if(padPosY == Define.COMPUTER_Y){
				displayPosY = Define.COMPUTER_Y + 20;
			}else if(padPosY == Define.PLAYER_Y){
				displayPosY = Define.PLAYER_Y - 20;
			}else if(padPosY == Define.COMPUTER_FRONT_Y){
				displayPosY = Define.COMPUTER_FRONT_Y;
			}else if(padPosY == Define.PLAYER_FRONT_Y){
				displayPosY = Define.PLAYER_FRONT_Y;
			}

			this.displayText("Speed : "+player.getQuadParam.x_speed, 10, displayPosY, 12);

			this.displayText("Size : "+player.getQuadParam.screen_location_w, 10, displayPosY+13, 12);
		}

		for(computer of this.m_computer)
		{
			if(computer == null){
				continue;
			}

			let padPosY = computer.getQuadParam.screen_location_y;

			if(padPosY == Define.COMPUTER_Y){
				displayPosY = Define.COMPUTER_Y + 20;
			}else if(padPosY == Define.PLAYER_Y){
				displayPosY = Define.PLAYER_Y - 20;
			}else if(padPosY == Define.COMPUTER_FRONT_Y){
				displayPosY = Define.COMPUTER_FRONT_Y;
			}else if(padPosY == Define.PLAYER_FRONT_Y){
				displayPosY = Define.PLAYER_FRONT_Y;
			}

			this.displayText("Speed : "+computer.getQuadParam.x_speed, 10, displayPosY, 12);

			this.displayText("Size : "+computer.getQuadParam.screen_location_w, 10, displayPosY+13, 12);
		}

		this.displayText("Ball Speed : "+this.m_ball.getQuadParam.y_speed, 10, Define.WINDOW_HEIGHT / 2, 12);
	},

	manageScore()
	{
		let scoreDown = this.m_ball.getScore(1);
		let scoreUp = this.m_ball.getScore(0);
		
		this.displayText("Score : "+String(scoreDown), 10, Define.PLAYER_Y +10, 12);

		this.displayText("Score : "+String(scoreUp), 10, Define.COMPUTER_Y -10, 12);

		if(scoreDown >= 10)
		{
			this.displayText("Down team win !!!", Define.WINDOW_WIDTH/3, Define.WINDOW_HEIGHT/2, 30);
			return true;
		}
		else if(scoreUp >= 10)
		{
			this.displayText("Up team win !!!", Define.WINDOW_WIDTH/3, Define.WINDOW_HEIGHT/2, 30);
			return true;
		}
		return false;
	}

};

function MPongConstructor() 
{
	let mPong = Object.create(MPong);
	mPong.m_canvas = document.querySelector("canvas");	
	mPong.m_context = mPong.m_canvas.getContext("2d");
	mPong.m_context.fillStyle = "#0b0827";
	mPong.m_context.fillRect(0, 0, 800, 600);
	mPong.m_bitmap = document.createElement("img");
	mPong.m_bitmap.src = "sprite/pong.bmp";
	mPong.m_timer = Date.now();
	let textList = [
		"(1) Number of players : 1",
		"(2) Game Mode : Brick",
		"(3) Screen position : Down",
		"(4) Double mode position : Back",
		"(5) Double mode two players game mode : Versus",
		"(6) Ball Speed : Slow",
		"(7) Computer Level : Easy",
		"(Return) Start the Game"	
	];

	let gameModeList = [
		PickUp.ONEPLAYER,
		PickUp.BRICKMODE,
		PickUp.DOWNTEAM,
		PickUp.BACK,
		PickUp.VERSUS,
		PickUp.BALLSPEEDSLOW,
		PickUp.COMPUTEREASY,
		PickUp.NOMODE
	];

	let keyList = ["1", "2", "3", "4", "5", "6", "7", "0"];

	mPong.m_computer = [null, null, null, null];
	mPong.m_player = [null, null, null, null];

	let x = Define.WINDOW_WIDTH/4, y = Define.WINDOW_HEIGHT/2 - 50, size = 20, shift = 20;
	let foreground = "white", background = "black";

	mPong.m_menu = new MMenu(textList, gameModeList, keyList, x, y, size, shift, foreground, background);

	mPong.m_stateStack.push(mPong.menu);

	requestAnimationFrame(animate);
	window.addEventListener("keydown",eventManager);
	window.addEventListener("keyup",eventManager);

	return mPong
}


let test = MPongConstructor();
let iter = 0

//setInterval("test.colorFeedback()", 1000);


function animate()
{
	if(test.m_stateStack[test.m_stateStack.length - 1] == test.menu){
		test.menu();
	}
	else if(test.m_stateStack[test.m_stateStack.length - 1] == test.game)
	{
		test.game();
	}
	
	requestAnimationFrame(animate);
}

function eventManager(event)
{
	if(test.m_stateStack[test.m_stateStack.length - 1] == test.menu){
		test.m_menu.handleMenuInput(event, test.m_stateStack, test.game);
	}
	else if(test.m_stateStack[test.m_stateStack.length - 1] == test.game){
		test.handleGameInput(event);
	}
	
}

/*test.menu();
test.game();*/