#include "MPong.h"

//~~~~~~~~~~ Tests unitaires ~~~~~~~~~~

void test_pong()
{
	Test_MPong test;
	test.test_init_shutdown();
	test.test_menu();
	test.test_clearScreen();
	test.test_handleMenuInput();
	test.test_game();
	test.test_initGame();
	test.test_handleGameInput();
	test.test_bonusLauncher();
	test.test_bonusDisplay();
	test.test_displayText();
	test.test_manageScore();
}

void Test_MPong::test_init_shutdown()
{
	printf("Test_MPong::test_init_shutdown() : Start testing\n");
	MPong pongTest;
	assert(pongTest.init() == true);

	pongTest.shutdown();
	pongTest.shutdown();

	assert(pongTest.init() == true);
	printf("Test_MPong::test_init_shutdown() : OK\n\n");
}

void Test_MPong::test_menu()
{
	printf("Test_MPong::test_menu : Start testing \n");

	//Normal Tests
	MPong pongTest;
	pongTest.menu();
	assert(pongTest.getGameError() == false);

	//Error Tests
	SDL_Surface * screenSurface = pongTest.getScreenSurface();
	pongTest.setScreenSurface(NULL);
	pongTest.menu();
	assert(pongTest.getGameError() == true);
	pongTest.setScreenSurface(screenSurface);
	pongTest.setGameError(false);

	SDL_Window * window = pongTest.getWindow();
	pongTest.setWindow(NULL);
	pongTest.menu();
	assert(pongTest.getGameError() == true);
	pongTest.setWindow(window);
	pongTest.setGameError(false);

	MMenu * menu = pongTest.getMenu();
	pongTest.setMenu(NULL);
	pongTest.menu();
	assert(pongTest.getGameError() == true);
	pongTest.setMenu(menu);
	pongTest.setGameError(false);

	printf("Test_MPong::test_menu : OK \n\n");
}

void Test_MPong::test_clearScreen()
{
	printf("Test_MPong::test_clearScreen : Start testing\n");
	MPong pongTest;
	assert(pongTest.clearScreen() == true );

	SDL_Surface * screenSurface = pongTest.getScreenSurface();
	pongTest.setScreenSurface(NULL);
	assert(pongTest.clearScreen() == false);
	pongTest.setScreenSurface(screenSurface);

	printf("Test_MPong::test_clearScreen : OK\n\n");
}

void Test_MPong::test_handleMenuInput()
{
	printf("Test_MPong::test_handleMenuInput : Start testing\n");
	MPong pongTest;
	assert(pongTest.handleMenuInput() == true);
	
	SDL_Event event;
	event.type = SDL_QUIT;
	pongTest.setEvent(event);
	assert(pongTest.handleMenuInput() == true);

	event.type = SDL_KEYDOWN;
	pongTest.setEvent(event);
	assert(pongTest.handleMenuInput() == true);

	event.type = SDL_KEYUP;
	pongTest.setEvent(event);
	assert(pongTest.handleMenuInput() == true);

	MMenu * menu = pongTest.getMenu();
	pongTest.setMenu(NULL);
	assert(pongTest.handleMenuInput() == false);
	pongTest.setMenu(menu);

	printf("Test_MPong::test_handleMenuInput : OK\n\n");
}

void Test_MPong::test_initGame()
{
	printf("Test_MPong::test_initGame : Start testing\n");
	MPong pongTest;
	MMenu * menu = pongTest.getMenu();
	assert(pongTest.initGame() == true);

	//m_gameObjectList isn't empty
	assert(pongTest.initGame() == false);
	//m_gameObjectList is empty
	pongTest.endGame();
	assert(pongTest.initGame() == true);
	pongTest.endGame();

	menu = pongTest.getMenu();
	pongTest.setMenu(NULL);
	assert(pongTest.initGame() == false);
	pongTest.setMenu(menu);
	pongTest.endGame();

	std::vector<PickUp>gameModeList;
	gameModeList.push_back(ONEPLAYER);
	gameModeList.push_back(BRICKMODE);
	gameModeList.push_back(DOWNTEAM);
	gameModeList.push_back(BACK);
	gameModeList.push_back(VERSUS);
	gameModeList.push_back(BALLSPEEDSLOW);
	gameModeList.push_back(COMPUTEREASY);
	gameModeList.push_back(NOMODE);
	gameModeList.push_back(NOMODE);

	//Testing all the initGame's parameters
	initGame_goThroughGameMode(gameModeList, &pongTest);

	printf("Test_MPong::test_initGame : OK\n\n");
}

void Test_MPong::initGame_goThroughGameMode(std::vector<PickUp>gameModeList, MPong * pongTest)
{
	int i = 0, nbTest=1;
	for(int a = (int)ONEPLAYER; a<BRICKMODE; a++)
	{
		gameModeList[i] = (PickUp)a;
		for(int b = (int)BRICKMODE; b<VERSUS; b++)
		{
			gameModeList[i+1] = (PickUp)b;
			for(int c =(int) VERSUS; c<DOWNTEAM; c++)
			{
				gameModeList[i+2] = (PickUp)c;
				for(int d = (int)DOWNTEAM; d<BACK; d++)
				{
					gameModeList[i+3] = (PickUp)d;
					for(int e= (int)BACK; e<BALLSPEEDSLOW; e++)
					{
						gameModeList[i+4] = (PickUp)e;
						for(int f = (int)BALLSPEEDSLOW; f<COMPUTEREASY; f++)
						{
							gameModeList[i+5] = (PickUp)f;
							for(int g = (int)COMPUTEREASY;g<=COMPUTERHARDEST; g++)
							{
								std::cout<<"test "<<a<<"-"<<b<<"-"<<c<<"-"<<d<<"-"<<e<<"-"<<f<<"-"<<g<<std::endl;
								gameModeList[i+6] = (PickUp)g;
								pongTest->getMenu()->setGameModeList(gameModeList);
								assert(pongTest->initGame() == true);
								initGame_gameModeTest(gameModeList, pongTest);
								pongTest->endGame();

							}
						}
					}
				}
			}
		}
	}
}

void Test_MPong::initGame_gameModeTest(std::vector<PickUp>gameModeList, MPong * pongTest)
{
	for(int i = 0; i<gameModeList.size(); i++)
	{
		//std::cout<<i<<std::endl;
		switch(gameModeList[i])
		{
			case ONEPLAYER :
				assert(pongTest->getPlayer(0) != NULL);
				assert(pongTest->getPlayer(1) == NULL);
				assert(pongTest->getComputer(0) != NULL);
				break;
			case TWOPLAYERS :
				assert(pongTest->getPlayer(0) != NULL);
				assert(pongTest->getPlayer(1) != NULL);
				break;
			case BRICKMODE :
				 assert(pongTest->getComputer(1) == NULL);
				 assert(pongTest->getComputer(2) == NULL);
				 break;
			case DOUBLEMODE :
				if(pongTest->getPlayer(1) == NULL)
				{
					assert(pongTest->getComputer(1) != NULL);
					assert(pongTest->getComputer(2) != NULL);
				}
				else if(pongTest->getPlayer(1) != NULL)
				{
					assert(pongTest->getComputer(1) != NULL);
					assert(pongTest->getComputer(2) == NULL);
				}
				break;
			case DOWNTEAM :
				if(pongTest->getComputer(1) != NULL)
				{
					assert(pongTest->getPlayer(0)->getQuadParam().screen_location.y > WINDOW_HEIGHT / 2 );
				}
				else 
				{
					assert(pongTest->getPlayer(0)->getQuadParam().screen_location.y == PLAYER_Y);
				}
				break;
			case UPTEAM :
				if(pongTest->getComputer(1) != NULL)
				{
					assert(pongTest->getPlayer(0)->getQuadParam().screen_location.y < WINDOW_HEIGHT / 2 );
				}
				else 
				{
					assert(pongTest->getPlayer(0)->getQuadParam().screen_location.y == COMPUTER_Y);
				}
				break; 
			case VERSUS :
				if(pongTest->getComputer(1) != NULL)
				{
					if(pongTest->getPlayer(1) != NULL)
					{
						if(pongTest->getPlayer(0)->getQuadParam().screen_location.y < WINDOW_HEIGHT / 2 )
						{
							assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y > WINDOW_HEIGHT / 2);
						}
						else 
						{
							assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y < WINDOW_HEIGHT / 2);
						}
					}
				}
				else 
				{
					if(pongTest->getPlayer(1) != NULL)
					{
						if(pongTest->getPlayer(0)->getQuadParam().screen_location.y < WINDOW_HEIGHT / 2 )
						{
							assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == PLAYER_Y);
						}
						else 
						{
							assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == COMPUTER_Y);
						}
					}
				}
				break;
			case TEAM :
				if(pongTest->getComputer(1) != NULL)
				{
					if(pongTest->getPlayer(0)->getQuadParam().screen_location.y < WINDOW_HEIGHT / 2 )
					{
						if(pongTest->getPlayer(1) != NULL)
							assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y < WINDOW_HEIGHT / 2);
					}
					else 
					{
						if(pongTest->getPlayer(1) != NULL)
							assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y > WINDOW_HEIGHT / 2);
					}
				}
				else 
				{
					if(pongTest->getPlayer(0)->getQuadParam().screen_location.y < WINDOW_HEIGHT / 2 )
					{
						if(pongTest->getPlayer(1) != NULL)
							assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == PLAYER_Y);
					}
					else 
					{
						if(pongTest->getPlayer(1) != NULL)
							assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == COMPUTER_Y);
					}
				}
				break;
			case BACK :
				if(pongTest->getComputer(1) != NULL)
				{
					if(pongTest->getPlayer(0)->getQuadParam().screen_location.y < WINDOW_HEIGHT / 2 )
					{
						assert(pongTest->getPlayer(0)->getQuadParam().screen_location.y == COMPUTER_Y);
						if(pongTest->getComputer(2) == NULL)
						{
							if(pongTest->getPlayer(1)->getQuadParam().screen_location.y < WINDOW_HEIGHT/2)
								assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == COMPUTER_FRONT_Y);
							else 
								assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == PLAYER_Y);
						}
						else
						{
							assert(pongTest->getComputer(0)->getQuadParam().screen_location.y == COMPUTER_FRONT_Y);
						}
					}
					else 
					{
						assert(pongTest->getPlayer(0)->getQuadParam().screen_location.y == PLAYER_Y);
						if(pongTest->getComputer(2) == NULL)
						{
								if(pongTest->getPlayer(1)->getQuadParam().screen_location.y < WINDOW_HEIGHT/2)
									assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == COMPUTER_Y);
								else
									assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == PLAYER_FRONT_Y);
						}
						else
						{
							assert(pongTest->getComputer(0)->getQuadParam().screen_location.y == PLAYER_FRONT_Y);
						}
					}
				}
				else 
				{
					if(pongTest->getPlayer(0)->getQuadParam().screen_location.y < WINDOW_HEIGHT / 2 )
					{
						if(pongTest->getPlayer(1) != NULL)
						{
							assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == PLAYER_Y);
						}
						else
						{
							assert(pongTest->getComputer(0)->getQuadParam().screen_location.y == PLAYER_Y);
						}
					}
					else 
					{
						if(pongTest->getPlayer(1) != NULL)
						{
							assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == COMPUTER_Y);
						}
						else
						{
							assert(pongTest->getComputer(0)->getQuadParam().screen_location.y == COMPUTER_Y);
						}
					}
				}
				break;
			case FRONT :
				if(pongTest->getComputer(1) != NULL)
				{
					if(pongTest->getPlayer(0)->getQuadParam().screen_location.y < WINDOW_HEIGHT / 2 )
					{
						assert(pongTest->getPlayer(0)->getQuadParam().screen_location.y == COMPUTER_FRONT_Y);
						if(pongTest->getComputer(2) == NULL)
						{
							if(pongTest->getPlayer(1)->getQuadParam().screen_location.y < WINDOW_HEIGHT/2)
								assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == COMPUTER_Y);
							else 
								assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == PLAYER_FRONT_Y);
						}
						else
						{
							assert(pongTest->getComputer(0)->getQuadParam().screen_location.y == COMPUTER_Y);
						}
					}
					else 
					{
						assert(pongTest->getPlayer(0)->getQuadParam().screen_location.y == PLAYER_FRONT_Y);
						if(pongTest->getComputer(2) == NULL)
						{
							if(pongTest->getPlayer(1)->getQuadParam().screen_location.y > WINDOW_HEIGHT/2)
								assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == PLAYER_Y);
							else 
								assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == COMPUTER_FRONT_Y);
						}
						else 
						{
							assert(pongTest->getComputer(0)->getQuadParam().screen_location.y == PLAYER_Y);
						}
					}
				}
				else 
				{
					if(pongTest->getPlayer(0)->getQuadParam().screen_location.y < WINDOW_HEIGHT / 2 )
					{
						if(pongTest->getPlayer(1) != NULL)
							assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == PLAYER_Y);
					}
					else 
					{
						if(pongTest->getPlayer(1) != NULL)
							assert(pongTest->getPlayer(1)->getQuadParam().screen_location.y == COMPUTER_Y);
					}
				}
				break;
			case BALLSPEEDSLOW :
				assert(MBall::cBALL_SPEED_Y == 3);
				assert(pongTest->getBall()->getQuadParam().y_speed == 3);
				break;
			case BALLSPEEDMEZZO :
				assert(MBall::cBALL_SPEED_Y == 5);
				assert(pongTest->getBall()->getQuadParam().y_speed == 5);
				break;
			case BALLSPEEDFAST :
				assert(MBall::cBALL_SPEED_Y == 7);
				assert(pongTest->getBall()->getQuadParam().y_speed == 7);
				break;
			case COMPUTEREASY :
				if(pongTest->getComputer(0) != NULL)
					assert(pongTest->getComputer(0)->getComputerLevel() == 1);
				break;
			case COMPUTERMEDIUM :
				if(pongTest->getComputer(0) != NULL)
					assert(pongTest->getComputer(0)->getComputerLevel() == 2);
				break;
			case COMPUTERHARD :
				if(pongTest->getComputer(0) != NULL)
					assert(pongTest->getComputer(0)->getComputerLevel() == 3);
				break;	
			case COMPUTERHARDEST :
				if(pongTest->getComputer(0) != NULL)
					assert(pongTest->getComputer(0)->getComputerLevel() == 4);
				break;		
		}
	}
}

void Test_MPong::test_handleGameInput()
{
	printf("Test_MPong::test_handleGameInput : Start testing\n");
	MPong pongTest;
	pongTest.setInitGame(pongTest.initGame());
	pongTest.handleGameInput();
	assert(pongTest.getEndGame() == false);
	assert(pongTest.getInitGame() == true);

	SDL_Event eventTest;
	eventTest.type = SDL_KEYDOWN;
	pongTest.setEvent(eventTest);
	pongTest.handleGameInput();
	assert(pongTest.getEndGame() == false);
	assert(pongTest.getInitGame() == true);

	eventTest.type =  SDL_KEYUP;
	pongTest.setEvent(eventTest);
	pongTest.handleGameInput();
	assert(pongTest.getEndGame() == false);
	assert(pongTest.getInitGame() == true);

	//pongTest.setInitGame(pongTest.initGame());
	eventTest.type =  SDL_KEYUP;
	eventTest.key.keysym.sym = SDLK_ESCAPE;
	pongTest.setEvent(eventTest);
	pongTest.handleGameInput();
	assert(pongTest.getEndGame() == true);
	assert(pongTest.getInitGame() == true);	

	eventTest.type = SDL_KEYUP;
	pongTest.setEvent(eventTest);
	pongTest.handleGameInput();
	assert(pongTest.getEndGame() == false);
	assert(pongTest.getInitGame() == false);

	eventTest.type =  SDL_QUIT;
	pongTest.setEvent(eventTest);
	pongTest.handleGameInput();
	assert(pongTest.getEndGame() == false);
	assert(pongTest.getInitGame() == false);	

	printf("Test_MPong::test_handleGameInput : OK\n\n");	
}

void Test_MPong::test_bonusLauncher()
{
	printf("Test_MPong::test_bonusLauncher : Start testing\n");
	MPong pongTest;
	assert(pongTest.bonusLauncher() == false);
	pongTest.initGame();
	assert(pongTest.bonusLauncher() == true);
	MBall * ballTest = pongTest.getBall();
	pongTest.setBall(NULL);
	assert(pongTest.bonusLauncher() == false);
	printf("Test_MPong::test_bonusLauncher : OK\n\n");
}

void Test_MPong::test_bonusDisplay()
{
	printf("Test_MPong::test_bonusDisplay : Start testing\n");
	MPong pongTest;
	assert(pongTest.bonusDisplay() == false);
	pongTest.initGame();
	assert(pongTest.bonusDisplay() == true);
	MBall * ballTest = pongTest.getBall();
	pongTest.setBall(NULL);
	assert(pongTest.bonusDisplay() == false);
	pongTest.setBall(ballTest);
	pongTest.setScreenSurface(NULL);
	assert(pongTest.bonusDisplay() == false);
	printf("Test_MPong::test_bonusDisplay : OK\n\n");
}

void Test_MPong::test_displayText()
{
	printf("Test_MPong::test_displayText : Start testing\n");
	MPong pongTest;
	assert(pongTest.displayText("test", 0, 0, 10) == true);
	pongTest.initGame();
	assert(pongTest.displayText("test", 0, 0, 10) == true);
	pongTest.setScreenSurface(NULL);
	assert(pongTest.displayText("test", 0, 0, 10) == false);
	printf("Test_MPong::test_displayText : OK\n\n");
}

void Test_MPong::test_manageScore()
{
	printf("Test_MPong::test_manageScore : Start testing\n");
	MPong pongTest;
	assert(pongTest.manageScore() == true);
	pongTest.initGame();
	assert(pongTest.manageScore() == false);
	MBall * ballTest = pongTest.getBall();
	pongTest.setBall(NULL);
	assert(pongTest.manageScore() == true);
	pongTest.setBall(ballTest);
	SDL_Surface * screenSurfaceTest = pongTest.getScreenSurface();
	pongTest.setScreenSurface(NULL);
	assert(pongTest.manageScore() == true);
	pongTest.setScreenSurface(screenSurfaceTest);
	ballTest->setScore(0, 10);
	pongTest.setBall(ballTest);
	assert(pongTest.manageScore() == true);
	printf("Test_MPong::test_manageScore : OK\n\n");
}


void Test_MPong::test_game()
{
	printf("Test_MPong::test_game : Start testing\n");
	MPong pongTest;
	pongTest.game();
	assert(pongTest.getGameError() == false);

	SDL_Surface * bitmap = pongTest.getBitmap();
	pongTest.setBitmap(NULL);
	pongTest.game();
	assert(pongTest.getGameError() == true);
	pongTest.setBitmap(bitmap);
	pongTest.setGameError(false);

	SDL_Surface * screenSurface = pongTest.getScreenSurface();
	pongTest.setScreenSurface(NULL);
	pongTest.game();
	assert(pongTest.getGameError() == true);
	pongTest.setScreenSurface(screenSurface);
	pongTest.setGameError(false);

	SDL_Window * window = pongTest.getWindow();
	pongTest.setWindow(NULL);
	pongTest.game();
	assert(pongTest.getGameError() == true);
	pongTest.setWindow(window);
	pongTest.setGameError(false);

	MBall * ball = pongTest.getBall();
	pongTest.setBall(NULL);
	pongTest.game();
	assert(pongTest.getGameError() == false);

	pongTest.setBall(ball);
	pongTest.setGameError(false);

	printf("Test_MPong::test_game : OK\n\n");
}