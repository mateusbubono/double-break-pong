/*!
*	\file MPong.h
*	\brief MPong class header's file
*	\author Boris Merminod
*	\version 1.0
*/

#include "MMenu.h"
#include "Defines.h"
#include "MGameObject.h"

/*!
*	\class MPong
*
*	\brief MPong class manage all the game structure
*
*	MPong class manage all game structure : system
*	initialization and shut down, windows management
*	back end of the menu, game loop from initialization
*	to end of the game and some game's features
*	
*/
class MPong
{
	public :

		/*!
		*	\brief MPong constructor -> Initialize all
		*	MPong object attributes and run the init()
		*	method.
		*/
		MPong();

		/*!
		*	\brief MPong destructor -> Run the shutdown()
		*	method and free the memory used to manage the
		*	MPong object
		*/
		~MPong();

		/*!!
		*	\brief function init() -> Initialize all the
		*	system behind the game
		*
		*	Initialize all the system behind the game :
		*	- Initialize SDL
		*	- Create a window
		*	- Create a Surface from the window
		*	- Initialize a timer 
		*	- Load and set sprite
		*	- Initialize the main menu
		*	- Initialize TTF librairy
		*
		*	\return if the initialization goes well, the
		*	function will return true. In case of problem,
		*	it return false
		*/
		bool init();

		/*!
		*	\brief function shutdown() -> Free all 
		*	memory used to run the game 
		*
		*	Free all memory used to run the game :
		*	- Close TTF
		*	- Free all the surfaces and destroy the window
		*	- Destroy all game objects : players, computers
		*	and ball 
		*	- Clear the game object list
		*	- Destroy the main menu
		*/
		void shutdown();

		/*!
		*	\brief function menu -> Manage displaying and 
		*	events of the main menu
		*
		*	Manage displaying the main menu with a call to
		*	MMenu's displayText method and managing the
		*	events with eventManager method. The function
		*	will update the m_timer member at each call.
		*	In case of error the m_gameError member will
		*	be switched to true 
		*/
		static void menu();

		/*!
		*	\brief function eventManager() -> The function
		*	will call the menu's event handler or the 
		*	game's event handler in function of the 
		*	context
		*
		*	\param wantGame = false (bool) : if this 
		*	parameter is false the function will call
		*	the Menu Event Handler, otherwise it will call
		*	 the Game Event Handler.
		*/
		static void eventManager(bool wantGame = false);

		/*!
		*	\brief function handleMenuInput() -> Manage
		*	events from the main menu
		*
		*	Manage events from the main menu : if the user
		*	close the window and if it using the keyboard
		*
		*	\return the function returned true. If there's a
		*	problem it return false
		*/
		static bool handleMenuInput();

		/*!
		*	\brief function handleGameInput() -> Manage
		*	events during a Double Break Pong's game
		*
		*	Manage events during a Double Break Pong's game
		*	if the user close the window and if it press or 
		*	release a Key from the Keyboard
		*
		*	If there's an error, the function will switch
		*	the m_gameError member to true
		*/
		static void handleGameInput();

		/*!
		*	\brief function initGame() -> Run the game's
		*	initialization before it begin
		*
		*	Run the game's initialization before it begin :
		*	- Initialize players and computers location and
		*	color
		*	- Initialize the game according the game mode 
		*	chosen by the player(s)
		*	- Initialize the ball, the players and the
		*	computers objects
		*	- All the players, computers and bricks are
		*	stores in a MGameObject List
		*
		*	\return if the function running well, it return 
		*	true. In case of error it return false
		*/
		static bool initGame();

		/*!
		*	\brief function endGame() -> Destroy all the 
		*	game objects and clear the game object list
		*/
		static void endGame();

		/*!
		*	\brief function game() -> Manage the game's
		*	displays, updates and events (game loop)
		*
		*	Manage the game's displays, updates and events :
		*	- At the first call the function will call 
		*	initGame to initialize the game
		*	- The events are handle by the eventManager's
		*	function
		*	- Updates are done by the function itself :
		*		- Call the brickSpawner function in Brick
		*		Mode
		*		- Call the bonusLaucher function in Double
		*		Mode
		*		- Call MBall::handleBall's method to update
		*		the ball's location
		*		- Call MComputer::computerHandler's method
		*		to update the AI's pad location
		*	- Displays are done by severals functions :
		*		- The Display is clear by clearScreen
		*		function
		*		- Bonuses and maluses are displayed by 
		*		bonusDisplay() function
		*		- Game Objects can shows feedbacks reaction
		*		with a spriteFeedback method
		*		- The Game objects are then Blitted by
		*		SDL_BlitScaled and SDL_BlitSurface
		*	- The function end its work with an update 
		*	of window's surface and of the timer
		*
		*	In case of error the function will turn the
		*	m_gameError member to true
		*/
		static void game();

		static void colorFeedback(MGameObject * gameObject);

		/*!
		*	\brief function clearScreen() -> Fill the 
		*	screen surface with a rectangle filled by
		*	a defined color
		*
		*	\return if the function is running well, it
		*	return true, otherwise it return false
		*/
		static bool clearScreen();

		/*!
		*	\brief function displayText() -> Blit a string
		*	on the screen as a texture
		*
		*	\param text (std::string) : The text to display
		*	\param x (int) : the text's location in x
		*	coordinate
		*	\param y (int) : the text's location in y
		*	coordinate
		*	\param size (int) : the text's size
		*
		*	\return the function return true. In case of
		*	error it returned false
		*/
		static bool displayText(std::string text, int x, int y, int size);

		/*!
		*	\brief function bonusDisplay() -> Display the
		*	pad's parameters
		*
		*	Display on a rendered text the pad's 
		*	parameters : the speed, the size. 
		*	Then the player can see how he's affected by a
		*	bonus or a malus
		*
		*	\return the function return true. In case of
		*	error it returned false
		*/
		static bool bonusDisplay();

		/*!
		*	\brief function manageScore() -> Display the
		*	Players's scores and run the end of the game
		*	when the score reach 10
		*
		*	The function used displayText to show each
		*	team's score. If a score reach 10 the function
		*	indicate it by returning true. Otherwise it 
		*	returned false
		*
		*	\return the function return false. If a player's
		*	score reach 10 then it return true
		*/
		static bool manageScore()
		;

		/*!
		*	\brief function brickSpawner() -> the function
		*	will spawn a brick sometimes during a brick
		*	mode game.
		*
		*	The function will spawn a brick sometimes during
		*	the brick mode game : 
		*	- It randomly decide to spawn a brick
		*	- When the brick spawn is running, the spawn's 
		*	location is chosen randomly as well as the color
		*	- The Brick instance is then created and pushed
		*	to the m_gameObjectList
		*
		*	\return If it run well, the function will return
		*	true. In case of error it returned false
		*/
		static bool brickSpawner();

		/*!
		*	\brief function bonusLauncher() -> The function
		*	triggers a random bonus on the double game mode
		*
		*	The function triggers a random bonus on the 
		*	double game mode :
		*	- It randomly choose to run a bonus/malus
		*	- Then a player is chosen randomly and a 
		*	bonus/malus will affect it randomly too
		*
		*	\retun If the function run well, it returned
		*	true. In case of problem it returned false
		*/
		static bool bonusLauncher(); 

		/*!
		*	\brief function getScreenSurface() -> Accessor
		*	of the m_screenSurface member
		*
		*	\return m_screenSurface (SDL_Surface *) :
		*	Pointer on SDL_Surface structure that represent
		*	the screen
		*/
		SDL_Surface * getScreenSurface();

		/*!
		*	\brief function setScreenSurface() -> Mutator
		*	of the m_screenSurface member
		*
		*	\param screenSurface (SDL_Surface *) : Pointer
		*	on a SDL_Surface structure that will replace
		*	the last Pointer on the m_screenSurface
		*/
		void setScreenSurface(SDL_Surface * screenSurface);

		/*!
		*	\brief function getWindow() -> Accessor of the
		*	m_window member
		*
		*	\return m_window (SDL_Window *) : Return a
		*	pointer on SDL_Window structure of the current
		*	m_window member
		*/
		SDL_Window * getWindow();

		/*!
		*	\brief function setWindow() -> Mutator of the
		*	m_window member
		*
		*	\param window (SDL_Window *) : Pointer on the
		*	SDL_Window structure that will replace the
		*	last pointer on the m_window member
		*/
		void setWindow(SDL_Window * window);

		/*!
		*	\brief function getMenu() -> Accessor of the
		*	m_menu member
		*
		*	\return m_menu (MMenu *) : Pointer on a MMenu
		*	object
		*/
		MMenu * getMenu();

		/*!
		*	\brief function setMenu() -> Mutator of the
		*	m_menu member
		*
		*	\param menu (MMenu *) : Pointer on a MMenu 
		*	object that will replace these on m_menu
		*	member
		*/
		void setMenu(MMenu * menu);

		/*!
		*	\brief function getTimer() -> Accessor of the
		*	m_timer member
		*
		*	\return m_timer (int) : Value of the actual
		*	in-game time
		*/
		int getTimer();

		/*!
		*	\brief function setTimer() -> Mutator of the
		*	m_timer member
		*
		*	\param timer (int) : New timer value to replace
		*	the in-game time contained by m_timer
		*/
		void setTimer(int timer);

		/*!
		*	\brief function getEvent() -> Accessor of the
		*	m_event member
		*
		*	\return m_event (SDL_Event) : return a SDL_Event
		*	structure from the m_event member
		*/
		SDL_Event getEvent();

		/*!
		*	\brief function setEvent() -> Mutator of the 
		*	m_event member
		*
		*	\param event (SDL_Event) : New value of m_event
		*	by a SDL_Event structure
		*/
		void setEvent(SDL_Event event);

		/*!
		*	\brief function getGameError() -> Accessor of
		*	the m_gameError member
		*
		*	\return m_gameError (bool) : return the value
		*	of m_gameError, that is a flag for an error
		*/
		bool getGameError();

		/*!
		*	\brief function setGameError() -> Mutator of the
		*	m_gameError member
		*
		*	\param gameError (bool) : New value of the
		*	m_gameError member
		*/
		void setGameError(bool gameError);

		/*!
		*	\brief function getBitmap() -> Accessor of the
		*	m_bitmap member
		*
		*	\return m_bitmap (SDL_Surface *) : Pointer on
		*	SDL_Surface structure contained in the
		*	m_bitmap member
		*/
		SDL_Surface * getBitmap();

		/*!
		*	\brief function setBitmap() -> Mutator of the
		*	m_bitmap member
		*
		*	\param bitmap (SDL_Surface *) : Pointer on a
		*	SDL_Surface structure that will be associated to
		*	m_bitmap member
		*/
		void setBitmap(SDL_Surface * bitmap);

		/*!
		*	\brief function getBall() -> Accessor of the 
		*	m_ball member
		*
		*	\return m_ball (MBall*) : Pointer on a MBall
		*	object that represent m_ball member
		*/
		MBall * getBall();

		/*!
		*	\brief function setBall() -> Mutator of the 
		*	m_ball member
		*
		*	\param	ball (MBall *) : Pointer on MBall
		*	object that will replace the m_ball as a 
		*	new value	 
		*/
		void setBall(MBall * ball);

		/*!
		*	\brief function getPlayer() -> Accessor of the
		*	m_player array member
		*
		*	\param index (int) : index number of the player
		*	we need to get
		*
		*	\return m_player[index] (MPlayer*) : Pointer on
		*	a MPlayer object
		*/
		MPlayer * getPlayer(int index);

		/*!
		*	\brief function setPlayer() -> Mutator of the
		*	m_player array member
		*
		*	\param player (MPlayer*) : Pointer on MPlayer
		*	object that we'll replace the current value of
		*	m_player[index]
		*	\param index (int) : index of the player we need
		*	to set
		*/
		void setPlayer(MPlayer * player, int index);

		/*!
		*	\brief function getComputer() -> Accessor of the
		*	m_computer array member
		*
		*	\param index (int) : index of the computer
		*	member that we need to get
		*
		*	\return m_computer[index] (MComputer*) : Pointer
		*	on MComputer object
		*/
		MComputer * getComputer(int index);

		/*!
		*	\brief function setComputer() -> Mutator of the
		*	m_computer array member
		*
		*	\param computer (MComputer *) : Pointer on 
		*	MComputer object that we'll replace the current
		*	value of m_computer[index]
		*	\param index (int) : index of the computer we
		*	need to set
		*/
		void setComputer(MComputer * computer, int index);

		/*!
		*	\brief function getInitGame() -> Accessor of the
		*	m_initGame member
		*
		*	\return m_initGame (bool) : contain true if the
		*	function initGame was sucessfully runned, false
		*	if it does not yet
		*/
		bool getInitGame();

		/*!
		*	\brief function setInitGame() -> Mutator of the
		*	m_initGame member
		*
		*	\param initGame (bool) : The new value that will
		*	replace the current value of m_initGame
		*/
		void setInitGame(bool initGame);

		/*!
		*	\brief function getBrickode() -> Accessor of
		*	m_brickMode member
		*
		*	\return m_brickMode (bool) : get true if the
		*	game mode is set on brick mode, and false if
		*	it is set to double mode
		*/
		bool getBrickMode();

		/*!
		*	\brief function setBrickMode() -> Mutator of
		*	m_brickMode member
		*
		*	\param brickMode (bool) : New value that will 
		*	replace the m_brickMode's current value
		*/
		void setBrickMode(bool brickMode);

		/*!
		*	\brief function getEndGame() -> Accessor of the 
		*	m_endGame member
		*
		*	\return m_endGame (bool) : a flag that indicate
		*	the end of the game
		*/
		bool getEndGame();

		/*!
		*	\brief function setEndGame() -> Mutator of the
		*	m_endGame member
		*
		*	\param endGame (bool) : New value of the
		*	that will replace the current m_endGame's value
		*/
		void setEndGame(bool endGame);

		static std::stack<StateStruct> m_stateStack; /*<! The state stack contains some function to run in
		the game loop. The game is stopped if the stack is
		empty*/

	private : 

		/*!
		*	\brief function initTransformAndColor() ->
		*	The function initialize a default location
		*	and colors according the location that will
		*	be used to initialise players and computers
		*	location
		*
		*	\param tabEntity (Entity []) : Array with all
		*	the possible location in the game
		*	\param tabColorString (std::string []) : Array
		*	that will contain all the possible player's
		*	color in the game
		*	\param sizeTab (int) : The size of tabEntity
		*	and tabColorString
		*
		*	\return the function returned true. In case 
		*	of problem it returned false
		*/
		static bool initTransformAndColor(Entity tabEntity[], std::string tabColorString[], const int sizeTab);

		/*!
		*	\brief function initGameMode() -> Initialization
		*	of all the game's flag to determine the Game
		*	Mode
		*
		*	\param gameModeList (std::vector<PickUp>) : A
		*	list with all the game modes selected by the
		*	player(s)
		*	\param twoPlayerFlag (bool*) : Pointer on a bool
		*	that flag indicate if the game is one player 
		*	(false) or two players (true)
		*	\param doubleModeFlag (bool *) : Pointer on a
		*	bool. That flag indicate if the game is in
		*	brick mode (false) or in double mode (true)
		*	\param teamFlag (bool *) : Pointer on a bool.
		*	That flag indicate if in two player mode and
		*	in double mode the both players are working
		*	in the same team (true) or not (false)
		*	\param downFlag (bool *) : Pointer on a bool.
		*	That flag indicate if the player one is one
		*	 the bottom of the screen (true) or on the top
		*	of the screen (false)
		*	\param frontFlag (bool *) : Pointer on bool.
		*	That flag indicate if in double mode the 
		*	players are at the back position (false) 
		*	or the front position (true)
		*	\param computerLevel (int *) : Pointer on int.
		*	Indicate the computer difficulty's level
		*
		*	\return the function returned true. In case of
		*	error it returned false 
		*/
		static bool initGameMode(std::vector<PickUp> gameModeList, bool * twoPlayersFlag, bool * doubleModeFlag, bool * teamFlag, bool * downFlag, bool * frontFlag, int * computerLevel);

		/*!
		*	\brief function initAllPlayers() -> The function
		*	initialize players and computer according the
		*	game modes selected by the player in the main
		*	menu
		*
		*	\param twoPlayersFlag (bool) : Indicate if 
		*	there is one player (false) or two players
		*	(false)
		*	\param doubleModeFlag (bool) : Indicate if the
		*	game mode is brick (false) or double (true)
		*	\param teamFlag (bool) : Indicate if in two
		*	players and double game mode the players are
		*	on the same teams (true) or not (false)
		*	\param downFlag (bool) : Indicate if the 
		*	player one is on the bottom of the screen
		*	(true) or on the top of the screen (false)
		*	\param frontFlag (bool) : Indicate if in double
		*	game mode if the player is on the front or on
		*	the back
		*	\param computerLevel (int) : The computer
		*	difficulty's level
		*	\param tabEntity (Entity []) : Pointer on a 
		*	Entity Array that will be used to initialize
		*	the players locations
		*	\param tabColorString (std::string []) :
		*	Pointer on a std::string array to initialize
		*	the players's color
		*
		*	\return the function return true. In case of
		*	error the function will return false 
		*/
		static bool initAllPlayers(bool twoPlayersFlag, bool doubleModeFlag, bool teamFlag, bool downFlag, bool frontFlag, int computerLevel, Entity tabEntity[], std::string tabColorString[]);

		static SDL_Event m_event; /*!< Contain the inputs events generated by the players*/
		static SDL_Surface * m_bitmap; /*!<The image .bmp that contain all sprites used in the game*/
		static SDL_Window * m_window; /*!<SDL_Window structure that represent the main window of the game*/
		static SDL_Surface * m_screenSurface; /*!< SDL_Surface Structure that represent the screen's surface*/

		static SDL_PixelFormat* m_mappingFormat;

		static int m_timer; /*!< The actual time's state is store in m_timer*/
		//static int m_score[NB_PLAYERS];
		static MBall * m_ball; /*!< The ball used in the game*/
		static MPlayer * m_player[NB_PLAYERS]; /*!< Array of two that represent the number of human players, that playing the game*/
		static MComputer * m_computer[NB_COMPUTERS]; /*Array of three that represent the number of computer players that playing the game*/
		static std::vector<MGameObject*>m_gameObjectList; /*A list of all the in-game object except the ball : Players, Computers and Bricks*/
		static MMenu * m_menu; /*!< Contain the main menu*/
		//static PickUp m_lastHit; 
		static bool m_initGame; /*!<Flag that indicate if the initGame function was runned (true) or not (false*/
		static bool m_brickMode; /*!<Flag that indicate if the game mode is Brick (true) or double (false)*/
		static bool m_endGame; /*!<Flag that indicate if the game need to continue (false) or to stop (true)*/
		static bool m_gameError; /*!<Flag that indicate when a error was raised (true)*/
		
};

class Test_MPong
{
	public :
		void test_init_shutdown();
		void test_menu();
		void test_clearScreen();
		void test_handleMenuInput();
		void test_game();
		void test_initGame();
		void initGame_gameModeTest(std::vector<PickUp>gameModeList, MPong * pongTest);
		void initGame_goThroughGameMode(std::vector<PickUp>gameModeList, MPong * pongTest);
		void test_handleGameInput();
		void test_bonusLauncher();
		void test_bonusDisplay();
		void test_displayText();
		void test_manageScore();
};
