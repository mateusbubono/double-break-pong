#include "MMenu.h"

void Test_MMenu::test_displayText()
{

	printf("Test_MMenu::test_displayText : Début du test\n");

	SDL_Window * window = SDL_CreateWindow(WINDOW_CAPTION, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);

	SDL_Surface * screenSurface = SDL_GetWindowSurface(window);

	std::vector<std::string>textList;
	textList.push_back("(1) Number of players : 1");
	textList.push_back("(2) Game Mode : Brick");
	textList.push_back("(3) Screen position : Down");
	textList.push_back("(4) Double mode position : Back");
	textList.push_back("(Return) Start the Game");
	textList.push_back("(Escape) Quit the Game");

	std::vector<PickUp>gameModeList;
	gameModeList.push_back(ONEPLAYER);
	gameModeList.push_back(BRICKMODE);
	gameModeList.push_back(DOWNTEAM);
	gameModeList.push_back(BACK);
	gameModeList.push_back(NOMODE);
	gameModeList.push_back(NOMODE);

	std::vector<SDL_Keycode>keyList;
	keyList.push_back(SDLK_1);
	keyList.push_back(SDLK_2);
	keyList.push_back(SDLK_3);
	keyList.push_back(SDLK_4);

	int x = WINDOW_WIDTH / 2;
	int y = WINDOW_HEIGHT / 6;
	int size = 20;
	int shift = 20;

	SDL_Color foreground = {255, 255, 255};
	SDL_Color background = {0, 0, 0};
	
	MMenu menuTest(textList, gameModeList, keyList, x, y, size, shift, foreground, background);

	bool success;

	success = menuTest.displayText(screenSurface);

	if((menuTest.getGameModeList().size() != menuTest.getTextList().size()) || success == false)
	{
		fprintf(stderr, "Test_MMenu::test_displayText : (1) Erreur taille différente non conforme de la liste des textes et de la liste des modes de jeu\n");
		exit(EXIT_FAILURE);
	}

	if((menuTest.getGameModeList(0) != ONEPLAYER) || (menuTest.getTextList(0) != "(1) Number of players : 1")|| success == false)
	{
		fprintf(stderr, "Test_MMenu::test_displayText : (2) Erreur le texte affiché ne correspond pas au mode de jeu\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(TWOPLAYERS, 0);
	success = menuTest.displayText(screenSurface);
	if((menuTest.getGameModeList(0) != TWOPLAYERS) || (menuTest.getTextList(0) != "(1) Number of players : 2")|| success == false)
	{
		fprintf(stderr, "Test_MMenu::test_displayText : (3) Erreur le texte affiché ne correspond pas au mode de jeu\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(BRICKMODE, 0);
	success = menuTest.displayText(screenSurface);
	if((menuTest.getGameModeList(0) != BRICKMODE) || (menuTest.getTextList(0) != "(2) Game Mode : Brick") || success == false)
	{
		fprintf(stderr, "Test_MMenu::test_displayText : (4) Erreur le texte affiché ne correspond pas au mode de jeu\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(DOUBLEMODE, 0);
	success = menuTest.displayText(screenSurface);
	if((menuTest.getGameModeList(0) != DOUBLEMODE) || (menuTest.getTextList(0) != "(2) Game Mode : Double") || success == false)
	{
		fprintf(stderr, "Test_MMenu::test_displayText : (5) Erreur le texte affiché ne correspond pas au mode de jeu\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(DOWNTEAM, 0);
	success = menuTest.displayText(screenSurface);
	if((menuTest.getGameModeList(0) != DOWNTEAM) || (menuTest.getTextList(0) != "(3) Screen position : Down") || success == false)
	{
		fprintf(stderr, "Test_MMenu::test_displayText : (6) Erreur le texte affiché ne correspond pas au mode de jeu\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(UPTEAM, 0);
	success = menuTest.displayText(screenSurface);
	if((menuTest.getGameModeList(0) != UPTEAM) || (menuTest.getTextList(0) != "(3) Screen position : Up") || success == false)
	{
		fprintf(stderr, "Test_MMenu::test_displayText : (7) Erreur le texte affiché ne correspond pas au mode de jeu\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(BACK, 0);
	success = menuTest.displayText(screenSurface);
	if((menuTest.getGameModeList(0) != BACK) || (menuTest.getTextList(0) != "(4) Double mode position : Back") || success == false)
	{
		fprintf(stderr, "Test_MMenu::test_displayText : (8) Erreur le texte affiché ne correspond pas au mode de jeu\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(FRONT, 0);
	success = menuTest.displayText(screenSurface);
	if((menuTest.getGameModeList(0) != FRONT) || (menuTest.getTextList(0) != "(4) Double mode position : Front")|| success == false)
	{
		fprintf(stderr, "Test_MMenu::test_displayText : (9) Erreur le texte affiché ne correspond pas au mode de jeu\n");
		exit(EXIT_FAILURE);
	}

	//Echec de la fonction
	success = menuTest.displayText(NULL);
	if(success == true)
	{
		fprintf(stderr, "Test_MMenu::test_displayText : (10) Erreur la fonction aurait dûe échouer\n");
		exit(EXIT_FAILURE);
	}

	gameModeList.push_back(NOMODE);
	menuTest.setGameModeList(gameModeList);
	success = menuTest.displayText(screenSurface);
	if(success == true)
	{
		fprintf(stderr, "Test_MMenu::test_displayText : (11) Erreur la fonction aurait dûe échouer\n");
		exit(EXIT_FAILURE);
	}

	printf("Test_MMenu::test_displayText : OK\n\n");

}

void Test()
{
	printf("Je suis une fonction de test\n");
}

void Test_MMenu::test_handleMenuInput()
{
	printf("Test_MMenu::test_handleMenuInput : Début du test\n");
	std::vector<std::string>textList;
	textList.push_back("(1) Number of players : 1");
	textList.push_back("(2) Game Mode : Brick");
	textList.push_back("(3) Screen position : Down");
	textList.push_back("(4) Double mode position : Back");
	textList.push_back("(Return) Start the Game");
	textList.push_back("(Escape) Quit the Game");

	std::vector<PickUp>gameModeList;
	gameModeList.push_back(ONEPLAYER);
	gameModeList.push_back(BRICKMODE);
	gameModeList.push_back(DOWNTEAM);
	gameModeList.push_back(BACK);
	gameModeList.push_back(NOMODE);
	gameModeList.push_back(NOMODE);

	std::vector<SDL_Keycode>keyList;
	keyList.push_back(SDLK_1);
	keyList.push_back(SDLK_2);
	keyList.push_back(SDLK_3);
	keyList.push_back(SDLK_4);
	keyList.push_back(SDLK_0);
	keyList.push_back(SDLK_0);

	int x = WINDOW_WIDTH / 2;
	int y = WINDOW_HEIGHT / 6;
	int size = 20;
	int shift = 20;

	SDL_Color foreground = {255, 255, 255};
	SDL_Color background = {0, 0, 0};

	SDL_Event e;
	std::stack<StateStruct> stateStack;
	
	MMenu menuTest(textList, gameModeList, keyList, x, y, size, shift, foreground, background);

	e.key.keysym.sym = SDLK_0;
	menuTest.handleMenuInput(e,stateStack, Test);

	if(gameModeList.size() != menuTest.getGameModeList().size())
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (1) Changement non prévu de la taille de la m_gameModeList\n");
		exit(EXIT_FAILURE);
	}

	e.key.keysym.sym = SDLK_1;
	menuTest.setGameModeList(ONEPLAYER, 0);
	menuTest.handleMenuInput(e,stateStack, Test);

	if(menuTest.getGameModeList(0) != TWOPLAYERS)
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (2) Réponse non adéquat de la fonction\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(TWOPLAYERS, 0);
	menuTest.handleMenuInput(e,stateStack, Test);

	if(menuTest.getGameModeList(0) != ONEPLAYER)
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (3) Réponse non adéquat de la fonction\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(BRICKMODE, 0);
	menuTest.handleMenuInput(e,stateStack, Test);

	if(menuTest.getGameModeList(0) != DOUBLEMODE)
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (4) Réponse non adéquat de la fonction\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(DOUBLEMODE, 0);
	menuTest.handleMenuInput(e,stateStack, Test);

	if(menuTest.getGameModeList(0) != BRICKMODE)
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (5) Réponse non adéquat de la fonction\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(UPTEAM, 0);
	menuTest.handleMenuInput(e,stateStack, Test);

	if(menuTest.getGameModeList(0) != DOWNTEAM)
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (6) Réponse non adéquat de la fonction\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(DOWNTEAM, 0);
	menuTest.handleMenuInput(e,stateStack, Test);

	if(menuTest.getGameModeList(0) != UPTEAM)
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (7) Réponse non adéquat de la fonction\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(FRONT, 0);
	menuTest.handleMenuInput(e,stateStack, Test);

	if(menuTest.getGameModeList(0) != BACK)
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (8) Réponse non adéquat de la fonction\n");
		exit(EXIT_FAILURE);
	}

	menuTest.setGameModeList(BACK, 0);
	menuTest.handleMenuInput(e,stateStack, Test);

	if(menuTest.getGameModeList(0) != FRONT)
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (9) Réponse non adéquat de la fonction\n");
		exit(EXIT_FAILURE);
	}

	std::stack<StateStruct>stackTest;
	e.key.keysym.sym = SDLK_RETURN;
	stackTest = menuTest.handleMenuInput(e,stateStack, Test);
	if(stackTest.empty() == true)
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (10) Erreur la pile devrait contenir quelque chose\n");
		exit(EXIT_FAILURE);
	}

	e.key.keysym.sym = SDLK_ESCAPE;
	stackTest = menuTest.handleMenuInput(e,stackTest, Test);
	if(stackTest.empty() == false)
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (11) Erreur la pile devrait être vide\n");
		exit(EXIT_FAILURE);
	}

	//Echecs de la fonction
	stackTest = menuTest.handleMenuInput(e,stackTest, NULL);
	if(stackTest.empty() == false)
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (12) Erreur la pile devrait être vide\n");
		exit(EXIT_FAILURE);
	}

	gameModeList.push_back(NOMODE);
	menuTest.setGameModeList(gameModeList);
	stackTest = menuTest.handleMenuInput(e,stackTest, Test);
	if(stackTest.empty() == false)
	{
		fprintf(stderr, "Test_MMenu::test_handleMenuInput : (13) Erreur la pile devrait être vide\n");
		exit(EXIT_FAILURE);
	}
	printf("Test_MMenu::test_handleMenuInput : OK\n\n");
}

void test_menu()
{
	Test_MMenu test;
	test.test_displayText();
	test.test_handleMenuInput();
}
