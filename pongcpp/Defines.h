/*!
*	\file Defines.h
*	\author Boris Merminod
*	\brief Header's file with includes, defines, structures
*	enumerations and test functions prototypes
*	\version 1.0
*
*	Defines.h contain :
*	- All including libraries
*	- test functions prototypes
*	- Definition of Entity structure
*	- Definition of StateStruct structure
*	- Defines of window properties
*	- Defines of game properties
*	- Definition of Direction enumeration
*	- Definition of PickUp enumeration
*/

#pragma once

// ~~~~~~~~~~ DOUBLE BREAK PONG INCLUDES ~~~~~~~~~~
#include <stack>
#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"
#include "math.h"
#include "time.h"
#include "Defines.h"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <cassert>

//Unitary tests functions prototypes
void test_gameObject();
void test_player();
void test_computer();
void test_ball();
void test_menu();
void test_pong();

// ~~~~~~~~~~ DOUBLE BREAK PONG STRUCTURES ~~~~~~~~~~

/*!
*	\struct Entity
*	\brief Contain all game object's
*	characteristics
*
*	Contain all game object 's 
*	characteristics :
*	- The screen location and dimensions
*	- The bitmap location ans dimensions
*	- the x axis's speed	
*	- the y axis's speed
*/
struct Entity
{
	SDL_Rect screen_location; /*<! game object's screen location and dimensions*/
	SDL_Rect bitmap_location;  /*<! game object's bitmap location and dimensions*/
	int x_speed; /*<! game object's x axis's speed*/
	int y_speed; /*<! game object's y axis's speed*/
};

/*!
*	\struct StateStruct
*	\brief contain a Pointer on a function
*	that will be used in a stack to run
*	main menu function and game function in
*	game loop
*/
struct StateStruct
{
	void (*StatePointer)();
};

//~~~~~~~~~~ DOUBLE BREAK PONG DEFINES ~~~~~~~~~~

//Window parameters
/*!
*	\def WINDOW_WIDTH
*	The window width
*/
#define WINDOW_WIDTH 800

/*!
*	\def WINDOW_HEIGHT
*	The window Height
*/
#define WINDOW_HEIGHT 600

/*!
*	\def WINDOW_CAPTION
*	The window caption
*/
#define WINDOW_CAPTION "Double Break Pong"

//Game parameters

/*!
*	\def FRAMES_PER_SECOND
*	The number of frames by seconds
*/
#define FRAMES_PER_SECOND 60

/*!
*	\def FRAME_RATE
*	The frame rate
*/
#define FRAME_RATE 1000/FRAMES_PER_SECOND

//Locations of game objects in bitmap file

/*!
*	\def PADDLE_BITMAP_X
*	Location of paddles in x axis
*/
#define PADDLE_BITMAP_X 0

/*!
*	\def PADDLE_BITMAP_RED_Y
*	Location of red paddle in y axis
*/
#define PADDLE_BITMAP_RED_Y 0

/*!
*	\def PADDLE_BITMAP_BLUE_Y
*	Location of blue paddle in y axis
*/
#define PADDLE_BITMAP_BLUE_Y 20

/*!
*	\def BALL_BITMAP_X
*	Location of the ball in x axis
*/
#define BALL_BITMAP_X 100

/*!
*	\def BALL_BITMAP_Y
*	Location of the ball in y axis
*/
#define BALL_BITMAP_Y 0

/*!
*	\def BRICK_WHITE_X
*	Location of the white brick in x axis
*/
#define BRICK_WHITE_X 200

/*!
*	\def BRICK_BLUE_X
*	Location of the blue brick in x axis
*/
#define BRICK_BLUE_X 120

/*!
*	\def BRICK_RED_X
*	Location of the red brick in x axis
*/
#define BRICK_RED_X 160

/*!
*	\def BRICK_Y
*	Location of all the bricks in y axis
*/
#define BRICK_Y 0

/*!
*	\def FEEDBACK_BITMAP_Y
*	Location feed back sprite in y axis
*/
#define FEEDBACK_BITMAP_Y -50

//Pads's Location in Game's screen
/*!
*	\def COMPUTER_Y
*	Up ans back y axis position of player in upteam
*/
#define COMPUTER_Y 30

/*!
*	\def COMPUTER_FRONT_Y
*	Up and front y axis position of player in upteam
*/
#define COMPUTER_FRONT_Y COMPUTER_Y+150

/*!
*	\def PLAYER_Y
*	Down and back y axis position of player in downteam
*/
#define PLAYER_Y 550

/*!
*	\def PLAYER_FRONT_Y
*	Down and front y axis position of player in downteam
*/
#define  PLAYER_FRONT_Y PLAYER_Y-150

//Pads's dimensions

/*!
*	\def PADDLE_WIDTH
*	The paddle's width
*/
#define PADDLE_WIDTH 100

/*!
*	\def PADDLE_HEIGHT
*	The paddle's height
*/
#define PADDLE_HEIGHT 20

//Ball's diameter
/*!
*	\def BALL_DIAMETER
*	The ball's diameter
*/
#define BALL_DIAMETER 20

//Bricks dimensions
/*!
*	\def BRICK_WIDTH
*	The brick's width
*/
#define BRICK_WIDTH 40

/*!
*	\def BRICK_HEIGHT
*	The brick's height
*/
#define BRICK_HEIGHT 40

//Pad's speed
/*!
*	\def PLAYER_SPEED
*	The Player's speed
*/
#define PLAYER_SPEED 10

/*!
*	\def COMPUTER_SPEED
*	The Computer's speed
*/
#define COMPUTER_SPEED 10

//Ball's speed
/*!
*	\def BALL_SPEED_MODIFIER
*	Ball speed modification in function of
*	where it was hitting on pad 
*/
#define BALL_SPEED_MODIFIER 5 

//Other defines

/*!
*	\def NB_PLAYERS
*	The numbers max of human players
*/
#define NB_PLAYERS 2

/*!
*	\def NB_COMPUTERS
*	The numbers max of computer players
*/
#define NB_COMPUTERS 3

/*!
*	\def BITMAP_PATH
*	Path of the bitmap file
*/
#define BITMAP_PATH "sprite/pong.bmp"

/*!
*	\def FONT_PATH
*	Path of the font file
*/
#define FONT_PATH "sprite/Ubuntu-M.ttf"

/*!
*	\def FEEDBACKFRAMES
*	The feedback's duration
*/
#define FEEDBACKFRAMES 30

// ~~~~~~~~~~ DOUBLE BREAK PONG ENUMERATIONS ~~~~~~~~~~


/*!
*	\enum Direction
*	Indicate the Side of a collision for 
*	the ball - side screen's collisions
*	and for the ball - brick's collisions
*/
enum Direction
{
	LEFT,
	RIGHT,
	UP,
	DOWN,
	CORNERTOP,
	CORNERBOTTOM,
	NONE
};

/*!
*	\enum PickUp
*	Contains all Game modes options
*/
enum PickUp
{
	NOMODE,
	ONEPLAYER,
	TWOPLAYERS,
	BRICKMODE,
	DOUBLEMODE,
	VERSUS,
	TEAM,
	DOWNTEAM,
	UPTEAM,
	BACK,
	FRONT,
	BALLSPEEDSLOW,
	BALLSPEEDMEZZO,
	BALLSPEEDFAST,
	COMPUTEREASY,
	COMPUTERMEDIUM,
	COMPUTERHARD,
	COMPUTERHARDEST
};
