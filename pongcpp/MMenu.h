/*!
*	\file MMenu.h
*	\brief MMenu class header's file
*	\author Boris Merminod
*	\version 1.0
*/

#ifndef MMENU_H
#define MMENU_H
#include "MGameObject.h"

/*!
*	\class MMenu
*	
*	\brief MMenu manage all the menu's feature.
*
*	MMenu manage all the menu's feature : menu's inputs and
*	menu's display
*/
class MMenu
{
	public :

	/*!
	*	\brief Constructor of MMenu class
	*
	*	\param textList (std::vector<std::string>) : A text
	*	list which will be displayed by the menu
	*	\param gameModeList (std::vector<PickUp>) : A game 
	*	mode list wich contain all the 
	*	parameter's game pick up by the player
	*	\param keyList (std::vector<SDL_Keycode>) : A key
	*	list of all the menu's inputs
	*	\param size (int) : The text's size
	*	\param shift (int) : The shift between two
	*	displayed text.
	*	\param foreground (SDL_Color) : The text's color
	*	\param background (SDL_Color) : The background's color
	*/
	MMenu(std::vector<std::string> textList, std::vector<PickUp>gameModeList, std::vector<SDL_Keycode> keyList, int x, int y, int size, int shift, SDL_Color foreground, SDL_Color background);

	/*!
	*	\brief displayText() -> manage the text to display on screen
	*
	*	The function will display all the text from m_texList
	*	on the screen. It use a SDL_Surface pointer to know
	*	where it can blit the text.
	*
	*	\param window (SDL_Surface *) : A pointer on the screen surface
	*	that'll be used to blit the text.
	*
	*	\return if everything goes well, the function return
	*	true. In case of error it return false.
	*/
	bool displayText(SDL_Surface * window);

	/*!
	*	\brief handleMenuInput() -> Manage the menu's inputs
	*
	*	The function manage the menu's inputs to adjust the game's features, run the game or
	*	quit the game
	*
	*	\param e (SDL_Event) : A SDL data struct that contain the Event we need to treat. 
	*	\param stateStack (std::stack<StateStruct>) : A stack to run or quit the game
	*	\param void (*pFunction)(void) : A pointer on the next function that'll be put into the 
	*	stack
	*
	*	\return stateStack (std::stack<StateStruct>) : the stateStack updated
 -	*/
	std::stack<StateStruct> handleMenuInput(SDL_Event e, std::stack<StateStruct> stateStack, void (*pFunction)(void));

	/*!
	*	\brief getGameModeList() -> Accessor of the game mode list member
	*
	*	\return m_gameModeList (PickUp) : return the game mode list member
	*/
	std::vector<PickUp> getGameModeList();

	/*!
	*	\brief getGameModeList(int index) -> Accessor of one member of the game mode list member
	*
	*	\param index (int) : The index of the member that'll be returned by the function
	*
	*	\return m_gameModeList[index] (PickUp) : Return one member of game mode list member
	*/
	PickUp getGameModeList(int index);

	/*!
	*	\brief setGameModeList(std::vector<PickUp> gameModeList) -> Mutator of the game mode list member
	*
	*	\param gameModeList (std::vector<PickUp>) : the new gameModeList updated
	*/
	void setGameModeList(std::vector<PickUp> gameModeList);

	/*!
	*	\brief setGameModeList(PickUp gameMode, int index) -> Mutator of one member of 
	*	the game mode list member
	*
	*	\param gameMode (PickUp) : the new member of m_gameModeList
	*	\param index (int) : the member's index to update in the m_gameModeList
	*/
	void setGameModeList(PickUp gameMode, int index);

	/*!
	*	\brief function getTextList() -> Accessor of text list member
	*
	*	\return m_textList (std::vector<std::string>) : the function return m_textList member
	*/
	std::vector<std::string> getTextList();

	/*!
	*	\brief function getTextList(int index) -> Accessor of one member of text list member
	*	
	*	\param index (int) : Member's index
	*
	*	\return textList[index] (std::string) : The function return one member of the text list member
	*/
	std::string getTextList(int index);

	/*!
	*	function setTextList(std::vector<std::string> textList) -> Mutator of the text list member
	*
	*	\param textList (std::vector<std::string) : The textList updated
	*/
	void setTextList(std::vector<std::string> textList);

	/*!
	*	function setTextList(std::string text, int index) -> Mutator of one member of the text list
	*	member
	*
	*	\param text (std::string) : the member's value updated
	*	\param index (int) : The member's index to update
	*/
	void setTextList(std::string text, int index);

	private :
		std::vector<std::string> m_textList; /*!< A menu text list which we're going to display*/
		std::vector<PickUp> m_gameModeList; /*!< A game mode list which can contains all the possible game mode in the game*/
		std::vector<SDL_Keycode> m_keyList; /*!< A key list which contain the SDL_keycode for the inputs*/
		int m_x; /*!< x position of the text displayed*/
		int m_y; /*!< y position of the first text displayed*/
		int m_size; /*!< text size*/
		int m_shift; /*!< all textlist's member will be displayed at the same x position and a y position that shifted depends of the text size, the shift value and text index*/
		SDL_Color m_foreground; /*< The text's color*/
		SDL_Color m_background; /*< The background's color*/
};


class Test_MMenu
{
	public :
		void test_displayText();
		void test_handleMenuInput();
};

#endif
